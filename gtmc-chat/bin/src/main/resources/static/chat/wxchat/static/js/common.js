//var rootUrl="https://carapp.gtmc.com.cn/chat/wx";
//var rootUrl="http://127.0.0.1:8001/chat/wx";
var rootUrl="https://carapptest.gtmc.com.cn/chat/wx";
//var rootUrl="http://yytest.imwork.net/chat/wx";
//var mess_reg=/^@5452caf627d8475fd0b8b1e7/;   //生产
var mess_reg=/^@e199d7b4ac6602d6cda31494/;
Date.prototype.format = function(format) {
       var date = {
              "M+": this.getMonth() + 1,
              "d+": this.getDate(),
              "h+": this.getHours(),
              "m+": this.getMinutes(),
              "s+": this.getSeconds(),
              "q+": Math.floor((this.getMonth() + 3) / 3),
              "S+": this.getMilliseconds()
       };
       if (/(y+)/i.test(format)) {
              format = format.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length));
       }
       for (var k in date) {
              if (new RegExp("(" + k + ")").test(format)) {
                     format = format.replace(RegExp.$1, RegExp.$1.length == 1
                            ? date[k] : ("00" + date[k]).substr(("" + date[k]).length));
              }
       }
       return format;
}


function checkNull(obj){
		var b=obj.replace(/(^\s*)|(\s*$)/g, "").length;
		if(b==0){
			return true;
		}else{
			return false;
		}
			
}

function checkImage(obj){
	var reg=mess_reg;
	var b=reg.test(obj);	
	return b;
}

function checkOs(code,id,wxCode){
	var u = navigator.userAgent;
	var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1; //android终端
	var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
	if(isAndroid){
		return location.href.split('#')[0];
	}else if(isiOS){
		return rootUrl+"/wx_chat?ownerCode="+code+"&userId="+id+"&wxCode="+wxCode+"&dealerCode="+dealerCode;
	}else{
		return location.href.split('#')[0];
	}
}

function isOs(){
	var u = navigator.userAgent;
	var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1; //android终端
	var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
	
	if(isAndroid){
		return 1;
	}else if(isiOS){
		return 2;
	}else{
		return 0;
	}
}


function isWx(){
	var u = navigator.userAgent;
	var isWx=u.indexOf('MicroMessenger') > -1;
	if(isWx){
		return 0;
	}else{
		return 1;
	}
}


function checkWkWebView(){
	var f=window.__wxjs_is_wkwebview;
	return f;
}


//1、禁止事件冒泡
function stopBubble(e) { 
//如果提供了事件对象，则这是一个非IE浏览器 
if ( e && e.stopPropagation ) 
    //因此它支持W3C的stopPropagation()方法 
    e.stopPropagation(); 
else
    //否则，我们需要使用IE的方式来取消事件冒泡 
    window.event.cancelBubble = true; 
}

//2、禁止默认事件
//阻止浏览器的默认行为 
function stopDefault( e ) { 
    //阻止默认浏览器动作(W3C) 
    if ( e && e.preventDefault ) 
        e.preventDefault(); 
    //IE中阻止函数器默认动作的方式 
    else
        window.event.returnValue = false; 
    return false; 
}

function bingEventLister(element,method,event){
	var x=document.getElementById(element);
	if (x.addEventListener) {                    //所有主流浏览器，除了 IE 8 及更早 IE版本
	    x.addEventListener(event,method);
	} else if (x.attachEvent) {                  // IE 8 及更早 IE 版本
	    x.attachEvent("on"+event,method);
	}
}
