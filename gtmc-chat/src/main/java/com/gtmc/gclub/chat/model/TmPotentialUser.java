package com.gtmc.gclub.chat.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "TM_POTENTIAL_USER") 
public class TmPotentialUser {
	
	@Id
    @Column(name = "POTENTIAL_USER_ID")
    private BigDecimal potentialUserId;

    @Column(name = "DEVICE_OPEN_ID")
    private String deviceOpenId;// 设备MAC地址或微信openid
    
    @Column(name = "SUBSCRIBE_DATE")
    private Date subscribeDate;// 关注时间
    
    @Column(name = "SOURCE_TYPE")
    private BigDecimal sourceType;// 来源类型10011001-微信,10011002-APP
    
    @Column(name = "DEALER_CODE")
    private String dealerCode;// 经销商代码
    
    @Column(name = "STATUS")
    private BigDecimal status;// 状态-10011001-已关注 10011002-取消关注

    @Column(name = "SEX")
    private Short sex;// 用户的性别，值为1时是男性，值为2时是女性，值为0时是未知

    @Column(name = "NICKNAME")
    private String nickname;

    @Column(name = "CREATE_DATE")
    private Date createDate;
    
    @Column(name = "CREATE_BY")
    private BigDecimal createBy;
    
    @Column(name = "UPDATE_DATE")
    private Date updateDate;
    
    @Column(name = "UPDATE_BY")
    private BigDecimal updateBy;
    
    @Column(name = "PROVINCE")
    private String province;// 省份
    
    @Column(name = "CITY")
    private String city;// 省份
    
    @Column(name = "COUNTRY")
    private String country;// 国家
    
    @Column(name = "HEAD_IMGURL")
    private String headImgurl;// 国家
    
    @Column(name = "APP_TYPE")
    private Integer appType;// 设备类型
    
    @Column(name = "REGISTER_PHONE")
    private String registerPhone;// 注册手机号
    
    @Column(name = "JMESSAGE_CODE")
    private String jmessageCode;// 极光账号

	public BigDecimal getPotentialUserId() {
		return potentialUserId;
	}

	public void setPotentialUserId(BigDecimal potentialUserId) {
		this.potentialUserId = potentialUserId;
	}

	public String getDeviceOpenId() {
		return deviceOpenId;
	}

	public void setDeviceOpenId(String deviceOpenId) {
		this.deviceOpenId = deviceOpenId;
	}

	public Date getSubscribeDate() {
		return subscribeDate;
	}

	public void setSubscribeDate(Date subscribeDate) {
		this.subscribeDate = subscribeDate;
	}

	public BigDecimal getSourceType() {
		return sourceType;
	}

	public void setSourceType(BigDecimal sourceType) {
		this.sourceType = sourceType;
	}

	public String getDealerCode() {
		return dealerCode;
	}

	public void setDealerCode(String dealerCode) {
		this.dealerCode = dealerCode;
	}

	public BigDecimal getStatus() {
		return status;
	}

	public void setStatus(BigDecimal status) {
		this.status = status;
	}

	public Short getSex() {
		return sex;
	}

	public void setSex(Short sex) {
		this.sex = sex;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public BigDecimal getCreateBy() {
		return createBy;
	}

	public void setCreateBy(BigDecimal createBy) {
		this.createBy = createBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public BigDecimal getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(BigDecimal updateBy) {
		this.updateBy = updateBy;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getHeadImgurl() {
		return headImgurl;
	}

	public void setHeadImgurl(String headImgurl) {
		this.headImgurl = headImgurl;
	}

	public Integer getAppType() {
		return appType;
	}

	public void setAppType(Integer appType) {
		this.appType = appType;
	}

	public String getRegisterPhone() {
		return registerPhone;
	}

	public void setRegisterPhone(String registerPhone) {
		this.registerPhone = registerPhone;
	}

	public String getJmessageCode() {
		return jmessageCode;
	}

	public void setJmessageCode(String jmessageCode) {
		this.jmessageCode = jmessageCode;
	}

	@Override
	public String toString() {
		return "TmPotentialUser [potentialUserId=" + potentialUserId + ", deviceOpenId=" + deviceOpenId
				+ ", subscribeDate=" + subscribeDate + ", sourceType=" + sourceType + ", dealerCode=" + dealerCode
				+ ", status=" + status + ", sex=" + sex + ", nickname=" + nickname + ", createDate=" + createDate
				+ ", createBy=" + createBy + ", updateDate=" + updateDate + ", updateBy=" + updateBy + ", province="
				+ province + ", city=" + city + ", country=" + country + ", headImgurl=" + headImgurl + ", appType="
				+ appType + ", registerPhone=" + registerPhone + ", jmessageCode=" + jmessageCode + "]";
	}

}