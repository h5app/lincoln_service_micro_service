package com.gtmc.gclub.chat.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gtmc.gclub.chat.dao.TtChatLogFileMapper;
import com.gtmc.gclub.chat.dao.TtChatLogMapper;
import com.gtmc.gclub.chat.dto.ChatParam;
import com.gtmc.gclub.chat.model.TtAdviser;
import com.gtmc.gclub.chat.model.WxUserOwner;
import com.gtmc.gclub.chat.util.EmojiUtil;

@Service
public class ChatLogService {
	
	@Autowired
	private TtChatLogMapper ttChatLogMapper;
	
	@Autowired
	private TtChatLogFileMapper ttChatLogFileMapper;

	/**
	 * 分页查询聊天的用户list
	 * @param param
	 * @return
	 * @throws Exception 
	 */
	public PageInfo<ChatParam> queryCustomerList(ChatParam param) throws Exception {
		Integer limit = param.getLimit()==null?10:param.getLimit();
		Integer offset = param.getOffset()==null?1:param.getOffset();
		Integer pageNo = offset/limit+1;
		Integer pageSize = limit;
		PageHelper.startPage(pageNo, pageSize);
		List<ChatParam> list = ttChatLogMapper.queryAllCustomerList(param);
		PageInfo<ChatParam> pageInfo = new PageInfo<ChatParam>(list);
		return pageInfo;
	}

	/**
	 * 获取和选定客户的所有聊天记录
	 * @param param
	 * @return
	 * @throws Exception 
	 */
	public PageInfo<ChatParam> queryChatCustomerList(ChatParam param) throws Exception {
		Integer limit = param.getLimit()==null?10:param.getLimit();
		Integer offset = param.getOffset()==null?1:param.getOffset();
		Integer pageNo = offset/limit+1;
		Integer pageSize = limit;
		PageHelper.startPage(pageNo, pageSize);
		List<ChatParam> list = ttChatLogMapper.queryChatListByCustomer(param);
		PageInfo<ChatParam> pageInfo = new PageInfo<ChatParam>(list);
		return pageInfo;
	}

	/**
	 * 获取所有以客户为维度的聊天记录文件
	 * @param param
	 * @return
	 */
	public List<Map<String,Object>> chatCustomerHistoryFile(ChatParam param) throws Exception {
		List<Map<String,Object>> res = ttChatLogFileMapper.queryLogFileByCustomer(param);
		return res;
	}
}
