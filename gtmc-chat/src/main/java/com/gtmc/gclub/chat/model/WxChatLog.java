package com.gtmc.gclub.chat.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.gtmc.gclub.common.WxMaterialTypeEnum;

@Table(name = "WX_CHAT_LOG")
public class WxChatLog {
	/**
	 * 编号
	 */
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;

	/**
	 * 客服编号
	 */
	@Column(name = "wx_user_id")
	private Integer wxUserId;

	/**
	 * app用户账号
	 */
	@Column(name = "owner_code")
	private String ownerCode;

	/**
	 * 聊天内容
	 */
	private String content;
	private String contentType;

	/**
	 * 聊天时间
	 */
	@Column(name = "crt_time")
	private Date crtTime;

	/**
	 * 聊天方向：0,微信客服发起 1,app用户发起
	 */
	private Byte direction;

	/**
	 * 会话编号
	 */
	@Column(name = "session_id")
	private Integer sessionId;

	/**
	 * 发送后返回的msgId
	 */
	@Column(name = "msg_id")
	private Long msgId;

	@Column(name = "date_show")
	private Integer dateShow;
	
	@Column(name = "title")
	private String title;
	
	@Column(name = "url")
	private String url;

	/**
	 * 获取编号
	 *
	 * @return id - 编号
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 设置编号
	 *
	 * @param id
	 *            编号
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 获取客服编号
	 *
	 * @return wx_user_id - 客服编号
	 */
	public Integer getWxUserId() {
		return wxUserId;
	}

	/**
	 * 设置客服编号
	 *
	 * @param wxUserId
	 *            客服编号
	 */
	public void setWxUserId(Integer wxUserId) {
		this.wxUserId = wxUserId;
	}

	/**
	 * 获取app用户账号
	 *
	 * @return owner_code - app用户账号
	 */
	public String getOwnerCode() {
		return ownerCode;
	}

	/**
	 * 设置app用户账号
	 *
	 * @param ownerCode
	 *            app用户账号
	 */
	public void setOwnerCode(String ownerCode) {
		this.ownerCode = ownerCode;
	}

	/**
	 * 获取聊天内容
	 *
	 * @return content - 聊天内容
	 */
	public String getContent() {
		return content;
	}

	/**
	 * 设置聊天内容
	 *
	 * @param content
	 *            聊天内容
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * 获取聊天时间
	 *
	 * @return crt_time - 聊天时间
	 */
	public Date getCrtTime() {
		return crtTime;
	}

	/**
	 * 设置聊天时间
	 *
	 * @param crtTime
	 *            聊天时间
	 */
	public void setCrtTime(Date crtTime) {
		this.crtTime = crtTime;
	}

	/**
	 * 获取聊天方向：0,微信客服发起 1,app用户发起
	 *
	 * @return direction - 聊天方向：0,微信客服发起 1,app用户发起
	 */
	public Byte getDirection() {
		return direction;
	}

	/**
	 * 设置聊天方向：0,微信客服发起 1,app用户发起
	 *
	 * @param direction
	 *            聊天方向：0,微信客服发起 1,app用户发起
	 */
	public void setDirection(Byte direction) {
		this.direction = direction;
	}

	/**
	 * 获取会话编号
	 *
	 * @return session_id - 会话编号
	 */
	public Integer getSessionId() {
		return sessionId;
	}

	/**
	 * 设置会话编号
	 *
	 * @param sessionId
	 *            会话编号
	 */
	public void setSessionId(Integer sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * 获取发送后返回的msgId
	 *
	 * @return msg_id - 发送后返回的msgId
	 */
	public Long getMsgId() {
		return msgId;
	}

	/**
	 * 设置发送后返回的msgId
	 *
	 * @param msgId
	 *            发送后返回的msgId
	 */
	public void setMsgId(Long msgId) {
		this.msgId = msgId;
	}

	public Integer getDateShow() {
		return dateShow;
	}

	public void setDateShow(Integer dateShow) {
		this.dateShow = dateShow;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public void setContentType(WxMaterialTypeEnum contentType) {
		this.contentType = contentType.name();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "WxChatLog [id=" + id + ", wxUserId=" + wxUserId + ", ownerCode=" + ownerCode + ", content=" + content
				+ ", contentType=" + contentType + ", crtTime=" + crtTime + ", direction=" + direction + ", sessionId="
				+ sessionId + ", msgId=" + msgId + ", dateShow=" + dateShow + ", title=" + title + ", url=" + url + "]";
	}

}