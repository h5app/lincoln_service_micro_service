package com.gtmc.gclub.chat.dao;

import java.util.List;
import java.util.Map;

import com.gtmc.gclub.chat.annotation.MySQLDb;
import com.gtmc.gclub.chat.dto.ChatParam;
import com.gtmc.gclub.chat.model.TtChatLogFile;

import tk.mybatis.mapper.common.Mapper;

//@OracleDb
@MySQLDb
public interface TtChatLogFileMapper extends Mapper<TtChatLogFile> {

	public List<Map<String,Object>> queryLogFileByCustomer(ChatParam param);
}