package com.gtmc.gclub.chat.jmessage;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import cn.jiguang.common.utils.Preconditions;
import cn.jmessage.api.common.model.MessageBody;
import cn.jmessage.api.common.model.MessagePayload;
import cn.jmessage.api.message.MessageType;
import cn.jmessage.api.utils.StringUtils;

public class WxMessagePayload extends MessagePayload {

	private static final String FROM_NAME = "from_name";

	private static Gson gson1 = new Gson();

	private String from_name;

	public WxMessagePayload(Integer version, String target_type, String target_id, String from_type, String from_id,
			MessageType msg_type, MessageBody msg_body, String from_name) {
		super(version, target_type, target_id, from_type, from_id, msg_type, msg_body);
		this.from_name = from_name;
	}

	public String toString() {
		JsonObject js = (JsonObject) super.toJSON();
		if (null != from_name) {
			js.addProperty(FROM_NAME, from_name);
		}
		return gson1.toJson(js);
	}

	public static Builder2 newBuilder2() {
		return new Builder2();
	}

	public static class Builder2 {
		private Integer version;
		private String target_type;
		private String target_id;
		private String from_type;
		private String from_id;
		private MessageType msg_type;
		private MessageBody msg_body;
		private String from_name;

		public Builder2 setVersion(Integer version) {
			this.version = version;
			return this;
		}

		public Builder2 setTargetType(String target_type) {
			this.target_type = target_type.trim();
			return this;
		}

		public Builder2 setTargetId(String target_id) {
			this.target_id = target_id.trim();
			return this;
		}

		public Builder2 setFromType(String from_type) {
			this.from_type = from_type.trim();
			return this;
		}

		public Builder2 setFromId(String from_id) {
			this.from_id = from_id.trim();
			return this;
		}

		public Builder2 setFromName(String from_name) {
			this.from_name = from_name.trim();
			return this;
		}

		public Builder2 setMessageType(MessageType msg_type) {
			this.msg_type = msg_type;
			return this;
		}

		public Builder2 setMessageBody(MessageBody msg_body) {
			this.msg_body = msg_body;
			return this;
		}

		public WxMessagePayload build() {
			Preconditions.checkArgument(null != version, "The version must not be empty!");
			Preconditions.checkArgument(StringUtils.isNotEmpty(target_type), "The target type must not be empty!");
			StringUtils.checkUsername(target_id);
			Preconditions.checkArgument(StringUtils.isNotEmpty(from_type), "The from type must not be empty!");
			StringUtils.checkUsername(from_id);
			// StringUtils.checkUsername(from_name);
			Preconditions.checkArgument(msg_type != null, "The message type must not be empty!");
			Preconditions.checkArgument(null != msg_body, "The message body must not be empty!");

			return new WxMessagePayload(version, target_type, target_id, from_type, from_id, msg_type, msg_body,
					from_name);
		}
	}

}
