package com.gtmc.gclub.chat.vo;

import java.util.List;

public class WxMaterialBaseVo {
	private String total_count;
	private String item_count;
	private List<Object> item;
	public String getTotal_count() {
		return total_count;
	}
	public void setTotal_count(String total_count) {
		this.total_count = total_count;
	}
	public String getItem_count() {
		return item_count;
	}
	public void setItem_count(String item_count) {
		this.item_count = item_count;
	}
	public List<Object> getItem() {
		return item;
	}
	public void setItem(List<Object> item) {
		this.item = item;
	}
	@Override
	public String toString() {
		return "WxMaterialBaseVo [total_count=" + total_count + ", item_count=" + item_count + ", item=" + item + "]";
	}
}