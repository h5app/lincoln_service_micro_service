package com.gtmc.gclub.chat.policy;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.gtmc.gclub.chat.constants.ChatConstant;
import com.gtmc.gclub.chat.model.TrUserEnterprise;

@Component
public class ChatPolicy {
	/**
	 * 获取对应的销售 0销售1售后
	 * 选择策略目前无！
	 * TODO
	 * @param users
	 * @return
	 */
	public static List<TrUserEnterprise> chooseChatUser(List<TrUserEnterprise> users){
		List<TrUserEnterprise> rtn = new ArrayList<TrUserEnterprise>();
		// TODO 
		TrUserEnterprise saleUser = null;
		TrUserEnterprise  saleSupporUser = null;
		
		if(users != null && users.size() > 0){
			for(TrUserEnterprise user : users){
				if(user.getDealerCdType().byteValue() == ChatConstant.SHOU_HOU){
					saleSupporUser = user;
					break;
				};
			}
			for(TrUserEnterprise user : users){
				if(user.getDealerCdType().byteValue() == ChatConstant.SHOU_QIAN){
					saleUser = user;
					break;
				};
			}
		}
		
		if(saleUser != null){
			rtn.add(saleUser);
		}
		if(saleSupporUser != null){
			rtn.add(saleSupporUser);
		}
		return rtn;
	}
	
	public static void main(String[] args) {
		System.out.println(System.currentTimeMillis());
	
		System.out.println(new Date(1451952000).toLocaleString());
		
		try  
		{  
		    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd ");  
		    Date date = sdf.parse("2017-01-04 17:55:36"); 
		    System.out.println(date.getTime());
		}  
		catch (Exception e)  
		{  
		    System.out.println(e.getMessage());  
		}  
	}
}
