package com.gtmc.gclub.chat.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;
/**
 * 后台用户信息表
 * */
@Table(name = "TM_USERS")
public class TmUsers {

	@Id
    @Column(name = "USER_ID")
    private BigDecimal userId;
    
    @Column(name = "DEALER_CODE")
    private String dealerCode;
    
    @Column(name = "USER_CODE")
    private String userCode;
    
    @Column(name = "LOGIN_NAME")
    private String loginName;
    
    @Column(name = "USER_NAME")
    private String userName;
    // 0：男1：女
    @Column(name = "SEX")
    private BigDecimal sex;
    
    @Column(name = "BIRTHDAY")
    private Date birthday;
    
    @Column(name = "PHONE")
    private String phone;
    
    @Column(name = "IDENTITY_CARD")
    private String identityCard;
    
    @Column(name = "QUARTERS_ID")
    private String quartersId;
    
    @Column(name = "QUARTERS_SIDE_IDS")
    private String quartersSideIds;
    
    @Column(name = "PASSWORD")
    private String password;
    // 10021001  GTMC 10021002  DLR
    @Column(name = "USER_TYPE")
    private String userType;
    
    @Column(name = "CREATE_DATE")
    private Date createDate;
    
    @Column(name = "CREATE_BY")
    private BigDecimal createBy;
    
    @Column(name = "UPDATE_DATE")
    private Date updateDate;
    
    @Column(name = "UPDATE_BY")
    private BigDecimal updateBy;
    // 1：启用 2:禁用
    @Column(name = "STATUS")
    private BigDecimal status;
    
    @Column(name = "EMIAL")
    private String emial;
    
    @Column(name = "ICONS_PHOTO")
    private String iconsPhoto;
    // 验证码
    @Column(name = "VCODE")
    private String vcode;
    // 登录失败次数
    @Column(name = "FAILURE_NUM")
    private BigDecimal failureNum;
    // 虚拟中心CODE
    @Column(name = "VIRTUAL_CENTER")
    private String virtualCenter;
    // 所属经理CODE
    @Column(name = "MANAGER")
    private String manager;
    
	public BigDecimal getUserId() {
		return userId;
	}
	public void setUserId(BigDecimal userId) {
		this.userId = userId;
	}
	public String getDealerCode() {
		return dealerCode;
	}
	public void setDealerCode(String dealerCode) {
		this.dealerCode = dealerCode;
	}
	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public BigDecimal getSex() {
		return sex;
	}
	public void setSex(BigDecimal sex) {
		this.sex = sex;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getIdentityCard() {
		return identityCard;
	}
	public void setIdentityCard(String identityCard) {
		this.identityCard = identityCard;
	}
	public String getQuartersId() {
		return quartersId;
	}
	public void setQuartersId(String quartersId) {
		this.quartersId = quartersId;
	}
	public String getQuartersSideIds() {
		return quartersSideIds;
	}
	public void setQuartersSideIds(String quartersSideIds) {
		this.quartersSideIds = quartersSideIds;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public BigDecimal getCreateBy() {
		return createBy;
	}
	public void setCreateBy(BigDecimal createBy) {
		this.createBy = createBy;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public BigDecimal getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(BigDecimal updateBy) {
		this.updateBy = updateBy;
	}
	public BigDecimal getStatus() {
		return status;
	}
	public void setStatus(BigDecimal status) {
		this.status = status;
	}
	public String getEmial() {
		return emial;
	}
	public void setEmial(String emial) {
		this.emial = emial;
	}
	public String getIconsPhoto() {
		return iconsPhoto;
	}
	public void setIconsPhoto(String iconsPhoto) {
		this.iconsPhoto = iconsPhoto;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public BigDecimal getFailureNum() {
		return failureNum;
	}
	public void setFailureNum(BigDecimal failureNum) {
		this.failureNum = failureNum;
	}
	public String getVirtualCenter() {
		return virtualCenter;
	}
	public void setVirtualCenter(String virtualCenter) {
		this.virtualCenter = virtualCenter;
	}
	public String getManager() {
		return manager;
	}
	public void setManager(String manager) {
		this.manager = manager;
	}
	@Override
	public String toString() {
		return "TmUsers [userId=" + userId + ", dealerCode=" + dealerCode + ", userCode=" + userCode + ", loginName="
				+ loginName + ", userName=" + userName + ", sex=" + sex + ", birthday=" + birthday + ", phone=" + phone
				+ ", identityCard=" + identityCard + ", quartersId=" + quartersId + ", quartersSideIds="
				+ quartersSideIds + ", password=" + password + ", userType=" + userType + ", createDate=" + createDate
				+ ", createBy=" + createBy + ", updateDate=" + updateDate + ", updateBy=" + updateBy + ", status="
				+ status + ", emial=" + emial + ", iconsPhoto=" + iconsPhoto + ", vcode=" + vcode + ", failureNum="
				+ failureNum + ", virtualCenter=" + virtualCenter + ", manager=" + manager + "]";
	}
    
}