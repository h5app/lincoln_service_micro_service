package com.gtmc.gclub.chat.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gtmc.gclub.chat.dao.TmUsersMapper;
import com.gtmc.gclub.chat.dao.TsDataCollectLogMapper;
import com.gtmc.gclub.chat.model.TmUsers;
import com.gtmc.gclub.chat.model.TsDataCollectLog;
import com.gtmc.gclub.chat.util.SpringUtil;

@Service
@Transactional
public class DataCollectService {

	private Logger logger = LoggerFactory.getLogger(DataCollectService.class);

	@Autowired
	private TsDataCollectLogMapper tsDataCollectLogMapper;

	@Autowired
	private TmUsersMapper tmUserMapper;

	public List<TsDataCollectLog> getAll() {
		return tsDataCollectLogMapper.selectAll();
	}

	public int save(TsDataCollectLog log) {
		int i = tsDataCollectLogMapper.insert(log);

		return i;
	}

	public int saveAll(List<TsDataCollectLog> l) {
		int i = tsDataCollectLogMapper.insertLogList(l);
		return i;
	}

	public List<TsDataCollectLog> getList(TsDataCollectLog log) {
		return tsDataCollectLogMapper.select(log);
	}

	public int updateData(TsDataCollectLog log) {
		int i = tsDataCollectLogMapper.updateByPrimaryKeySelective(log);
		return i;
	}

	public int updateDataNullUpdate(TsDataCollectLog log) {
		int i = tsDataCollectLogMapper.updateByPrimaryKey(log);
		return i;
	}

	public int saveTransaction(TsDataCollectLog log, TmUsers user) {

		int y = 0;
		try {
			int i = tsDataCollectLogMapper.insert(log);

			DataSourceTransactionManager transactionManager = (DataSourceTransactionManager) SpringUtil
					.getBean("transactionManager");
			DefaultTransactionDefinition def = new DefaultTransactionDefinition();
			def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW); // 事物隔离级别，开启新事务，与A类和B类不使用同一个事务。
			TransactionStatus status1 = transactionManager.getTransaction(def); // 获得事务状态
			y = tmUserMapper.insert(user);
			transactionManager.commit(status1);

			if (10 / 0 > 1)
				;
		} catch (Exception e) {
			logger.error("捕获到异常", e);
			// 一般trycatch后就不会回滚
			// 通过这行可以来rollback
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		return y;
	}

	public PageInfo<TsDataCollectLog> pageQuery(int pageCurrent, int pageSize, String premise) {
		Page<TsDataCollectLog> page = PageHelper.startPage(pageCurrent, pageSize);
		List<TsDataCollectLog> logList = tsDataCollectLogMapper.selectAll();
		PageInfo<TsDataCollectLog> pageInfo = new PageInfo<TsDataCollectLog>(logList);
		return pageInfo;

	}

}
