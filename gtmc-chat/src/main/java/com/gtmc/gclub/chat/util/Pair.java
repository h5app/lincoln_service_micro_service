/**
 * Pair.java
 * Created at 2015年7月8日
 * Created by Renyifei
 * Copyright (C) 2015 SHANGHAI VOLKSWAGEN, All rights reserved.
 */
package com.gtmc.gclub.chat.util;

/**
 * <p>ClassName: Pair</p>
 * <p>Description: TODO</p>
 * <p>Author: Renyifei</p>
 * <p>Date: 2015年7月8日</p>
 */
public class Pair {
    private String key;
    private String value;
    
    /**
     * <p>Description: TODO</p>
     * @param key
     * @param value
     * @param type
     */
    public Pair(String key, String value) {
        this.key = key;
        this.value = value;
    }
    /**
     * <p>Description: TODO</p>
     * @return the key
     */
    public String getKey() {
        return key;
    }
    /**
     * <p>Description: TODO</p>
     * @param key the key to set
     */
    public void setKey(String key) {
        this.key = key;
    }
    /**
     * <p>Description: TODO</p>
     * @return the value
     */
    public String getValue() {
        return value;
    }
    /**
     * <p>Description: TODO</p>
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }
}
