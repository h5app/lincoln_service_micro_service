package com.gtmc.gclub.chat.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.gtmc.gclub.chat.model.TtAdviser;
import com.gtmc.gclub.chat.service.CarOwnerService;
import com.gtmc.gclub.chat.service.DataCollectService;

@Controller
@RequestMapping(value="/admin/activity")
public class ActivityController extends BaseController{
	
	@Autowired
	private DataCollectService dataService;
	
	@Autowired
	private CarOwnerService carOwnerService;
	
	@RequestMapping(value = LIST, method = RequestMethod.GET)
	public String list() {
		return "/admin/view/activity/list.html";
	}
	
	@RequestMapping(value = ADD, method = RequestMethod.GET)
	public String add() {
		return "/admin/view/activity/add.html";
	}
	
	@ResponseBody
	@RequestMapping(value = PAGE)
	public PageInfo<TtAdviser> queryForList(@RequestParam(value = "page", defaultValue = "1") int page, @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
		PageInfo<TtAdviser> result = carOwnerService.pageQuery(page, pageSize);
		return result;
	}
}
