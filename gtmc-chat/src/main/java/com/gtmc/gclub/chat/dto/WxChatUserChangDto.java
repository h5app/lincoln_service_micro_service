package com.gtmc.gclub.chat.dto;

import com.gtmc.gclub.common.WxMaterialTypeEnum;

public class WxChatUserChangDto {
	private String srcChatId = null;
	private String tarChatId = null ;
	private String ownerCode = null ;
	private String message = null ;
	private String messageType = null ;
	public String getSrcChatId() {
		return srcChatId;
	}
	public void setSrcChatId(String srcChatId) {
		this.srcChatId = srcChatId;
	}
	public String getTarChatId() {
		return tarChatId;
	}
	public void setTarChatId(String tarChatId) {
		this.tarChatId = tarChatId;
	}
	public String getOwnerCode() {
		return ownerCode;
	}
	public void setOwnerCode(String ownerCode) {
		this.ownerCode = ownerCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public void setMessageType(WxMaterialTypeEnum messageType) {
		this.messageType = messageType.name();
	}
	@Override
	public String toString() {
		return "WxChatUserChangDto [srcChatId=" + srcChatId + ", tarChatId=" + tarChatId + ", ownerCode=" + ownerCode
				+ ", message=" + message + ", messageType=" + messageType + "]";
	}
	
}
