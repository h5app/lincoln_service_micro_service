package com.gtmc.gclub.chat.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.Workbook;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.enmus.ExcelType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.gtmc.gclub.chat.bean.BizException;
import com.gtmc.gclub.chat.bean.Result;
import com.gtmc.gclub.chat.dto.ChatClassifyContentDto;
import com.gtmc.gclub.chat.dto.ChatClassifyQueryDto;
import com.gtmc.gclub.chat.model.TmClassifyContent;
import com.gtmc.gclub.chat.model.TmClassifyTag;
import com.gtmc.gclub.chat.service.AppApiService;
import com.gtmc.gclub.chat.util.DateUtil;
import com.gtmc.gclub.chat.vo.ClassifyContentPageVo;
import com.gtmc.gclub.chat.vo.ClassifyTagFirstVo;
import com.gtmc.gclub.chat.vo.ClassifyTagSecondVo;
import com.gtmc.gclub.chat.vo.TmClassifyContentVo;
import com.infoservice.filestore.FileStore;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/chat/app/content")
public class AppContentController {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private AppApiService appApiService;

	@ApiOperation(value = "添加常见标签-单条", produces = "application/json; charset=utf-8")
	@RequestMapping(value = "/addAppChatTag", method = RequestMethod.POST)
	@ResponseBody
	public Result<String> addAppChatTag(@RequestParam(value = "dealerCode", required = true) String dealerCode,
			@RequestBody ChatClassifyContentDto dto) {
		logger.info("addAppChatTag==>");
		TmClassifyTag tm = new TmClassifyTag();
		tm.setClassifyCode(dto.getClassifyCode());
		tm.setClassifyName(dto.getClassifyName());
		tm.setPid(dto.getPid());
		tm.setCreateDate(new Date());
		tm.setDealerCode(dealerCode);
		Integer i = appApiService.addAppChatTag(tm);
		return new Result<>(1, null);
	}

	@ApiOperation(value = "添加常见内容-单条", produces = "application/json; charset=utf-8")
	@RequestMapping(value = "/addAppChatContent", method = RequestMethod.POST)
	@ResponseBody
	public Result<String> addAppChatContent(@RequestBody ClassifyContentPageVo dto) {
		logger.info("addAppChatContent==>");
		TmClassifyContent tm = new TmClassifyContent();
		tm.setContent(dto.getContent());
		tm.setPid(dto.getId2());
		tm.setCreateDate(new Date());
		Integer i = appApiService.addAppChatContent(tm);
		return new Result<>(1, null);
	}

	@ApiOperation(value = "添加常见内容-多条", produces = "application/json;charset=utf-8")
	@RequestMapping(value = "/addBatchAppChatContent", method = RequestMethod.POST)
	@ResponseBody
	public Result<String> addBatchAppChatContent(@RequestParam(value = "dealerCode", required = true) String dealerCode,
			@RequestBody List<ChatClassifyContentDto> dto) {
		logger.info("addBatchAppChatContent==>");
		int i = appApiService.addBatchAppChatContent(dto, dealerCode);
		return new Result<>(1, null);
	}

	// ---------------->
	@ApiOperation(value = "分级内容更新", produces = "application/json; charset=utf-8")
	@RequestMapping(value = "/updateAppChatContent", method = RequestMethod.POST)
	@ResponseBody
	public Result<String> updateAppChatContent(@RequestBody ClassifyContentPageVo dto) {
		logger.info("updateAppChatDialogue==>");
		int i = appApiService.updateAppChatContent(dto);
		return new Result<>(1, null);
	}

	@ApiOperation(value = "分级标签更新", produces = "application/json; charset=utf-8")
	@RequestMapping(value = "/updateAppChatTag", method = RequestMethod.POST)
	@ResponseBody
	public Result<String> updateAppChatTag(@RequestBody ClassifyContentPageVo dto) {
		logger.info("updateAppChatTag==>");
		int i = appApiService.updateAppChatTag(dto);
		return new Result<>(1, null);
	}

	@ApiOperation(value = "分级内容删除", produces = "application/json; charset=utf-8")
	@RequestMapping(value = "/deleteAppChatContent", method = RequestMethod.POST)
	@ResponseBody
	public Result<String> deleteAppChatContent(@RequestBody ClassifyContentPageVo dto) {
		logger.info("deleteAppChatContent==>");
		if (dto.getId() == null) {
			throw new BizException("id is null");
		}
		int i = appApiService.deleteAppChatContent(dto);
		return new Result<>(1, null);
	}

	@ApiOperation(value = "分级标签删除", produces = "application/json; charset=utf-8")
	@RequestMapping(value = "/deleteAppChatTag", method = RequestMethod.POST)
	@ResponseBody
	public Result<String> deleteAppChatTag(@RequestBody ClassifyContentPageVo dto) {
		logger.info("deleteAppChatTag==>");
		int i = appApiService.deleteAppChatTag(dto);
		return new Result<>(1, null);
	}

	// --------------->
	@ApiOperation(value = "获取常用内容标签对象", produces = "application/json; charset=utf-8")
	@RequestMapping(value = "/getAppChatTag", method = RequestMethod.GET)
	@ResponseBody
	public Result<List<TmClassifyContentVo>> getAppChatTag(
			@RequestParam(value = "dealerCode", required = true) String dealerCode) {
		logger.info("getAppChatTag==>");
		List<TmClassifyContentVo> vo = appApiService.getAppChatTag(dealerCode);
		return new Result<>(1, vo, null);
	}

	@ApiOperation(value = "分页-->获取常用内容", produces = "application/json; charset=utf-8")
	@RequestMapping(value = "/getAppChatContent", method = RequestMethod.POST)
	@ResponseBody
	public Result<PageInfo<ClassifyContentPageVo>> getAppChatContent(@RequestBody ChatClassifyQueryDto dto) {
		logger.info("getAppChatContent==>");
		PageInfo<ClassifyContentPageVo> page = appApiService.getAppChatContent(dto);
		return new Result<PageInfo<ClassifyContentPageVo>>(1, page, null);
	}

	@ApiOperation(value = "分页-->获取常用一级标签或者二级", produces = "application/json; charset=utf-8")
	@RequestMapping(value = "/getAppChatClassify", method = RequestMethod.POST)
	@ResponseBody
	public Result<PageInfo<ClassifyContentPageVo>> getAppChatClassify(
			@RequestParam(value = "classify", required = true) String classify, @RequestBody ChatClassifyQueryDto dto) {
		logger.info("getAppChatClassify==>");
		PageInfo<ClassifyContentPageVo> page = appApiService.getAppChatClassify(dto, classify);
		return new Result<PageInfo<ClassifyContentPageVo>>(1, page, null);
	}

	@ApiOperation(value = "导出-->获取常用内容", produces = "application/json; charset=utf-8")
	@RequestMapping(value = "/getAppChatContentExport", method = RequestMethod.POST)
	@ResponseBody
	public Result<String> getAppChatContentExport(@RequestBody ChatClassifyQueryDto dto) {
		logger.info("getAppChatContentExport==>");
		List<ClassifyContentPageVo> page = appApiService.getAppChatContentExport(dto);
		ExportParams params = new ExportParams(null, "常见内容");
		// ExcelHandler h = new ExcelHandler();
		// h.setNeedHandlerFields(new String[] { "发放平台", "优惠劵类型", "有效期类型", "状态",
		// "是否允许多张一起使用" });
		// params.setDataHanlder(h);
		params.setType(ExcelType.HSSF);
		Workbook wk = ExcelExportUtil.exportExcel(params, ClassifyContentPageVo.class, page);
		ByteArrayOutputStream os = null;
		String fileUrl = null;
		try {
			String str = "常见内容" + "-" + DateUtil.format(new Date(), "yyyyMMddHHmmssSSS") + ".xls";
			os = new ByteArrayOutputStream();
			wk.write(os);
			fileUrl = FileStore.getInstance().write("nd02", "fs01", str, os.toByteArray());
		} catch (Exception e) {
			logger.error("error1", e);
			if (os != null) {
				try {
					os.close();
				} catch (IOException e1) {
					logger.error("error2", e1);
				}
			}
			return new Result<String>(0, fileUrl, null);
		}
		return new Result<String>(1, fileUrl, null);
	}

	@ApiOperation(value = "导出-->一级标签或者二级标签", produces = "application/json; charset=utf-8")
	@RequestMapping(value = "/getAppChatClassifyExport", method = RequestMethod.POST)
	@ResponseBody
	public Result<String> getAppChatClassifyExport(@RequestParam(value = "classify", required = true) String classify,
			@RequestBody ChatClassifyQueryDto dto) {
		logger.info("getAppChatClassifyExport==>");
		String fileUrl = null;
		if ("1".equals(classify)) {
			List<ClassifyTagFirstVo> page = appApiService.getClassifyFirstExport(dto, classify);
			ExportParams params = new ExportParams(null, "常见内容");
			params.setType(ExcelType.HSSF);
			Workbook wk = ExcelExportUtil.exportExcel(params, ClassifyTagFirstVo.class, page);
			ByteArrayOutputStream os = null;
			try {
				String str = "分类标签" + "-" + DateUtil.format(new Date(), "yyyyMMddHHmmssSSS") + ".xls";
				os = new ByteArrayOutputStream();
				wk.write(os);
				fileUrl = FileStore.getInstance().write("nd02", "fs01", str, os.toByteArray());
			} catch (Exception e) {
				logger.error("error1", e);
				if (os != null) {
					try {
						os.close();
					} catch (IOException e1) {
						logger.error("error2", e1);
					}
				}
				return new Result<String>(0, fileUrl, null);
			}
		} else {
			List<ClassifyTagSecondVo> page = appApiService.getClassifySecondExport(dto, classify);
			ExportParams params = new ExportParams(null, "常见内容");
			params.setType(ExcelType.HSSF);
			Workbook wk = ExcelExportUtil.exportExcel(params, ClassifyTagSecondVo.class, page);
			ByteArrayOutputStream os = null;

			try {
				String str = "分类标签" + "-" + DateUtil.format(new Date(), "yyyyMMddHHmmssSSS") + ".xls";
				os = new ByteArrayOutputStream();
				wk.write(os);
				fileUrl = FileStore.getInstance().write("nd02", "fs01", str, os.toByteArray());
			} catch (Exception e) {
				logger.error("error1", e);
				if (os != null) {
					try {
						os.close();
					} catch (IOException e1) {
						logger.error("error2", e1);
					}
				}
				return new Result<String>(0, fileUrl, null);
			}

		}

		return new Result<String>(1, fileUrl, null);
	}
}
