package com.gtmc.gclub.chat.dto;

import com.gtmc.gclub.common.WxMaterialTypeEnum;

public class ChatMessageDto {
	private String content;
	private WxMaterialTypeEnum type;
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public WxMaterialTypeEnum getType() {
		return type;
	}
	public void setType(WxMaterialTypeEnum type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "ChatMessageDto [content=" + content + ", type=" + type + "]";
	}
}
