package com.gtmc.gclub.chat.dao;

import com.gtmc.gclub.chat.annotation.MySQLDb;
import com.gtmc.gclub.chat.annotation.OracleDb;
import com.gtmc.gclub.chat.model.TmDealer;
import tk.mybatis.mapper.common.Mapper;
//@OracleDb
@MySQLDb
public interface TmDealerMapper extends Mapper<TmDealer> {
}