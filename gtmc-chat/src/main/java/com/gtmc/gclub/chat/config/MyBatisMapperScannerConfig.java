package com.gtmc.gclub.chat.config;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.gtmc.gclub.chat.annotation.MySQLDb;
import com.gtmc.gclub.chat.annotation.OracleDb;

import tk.mybatis.spring.mapper.MapperScannerConfigurer;

import java.util.Properties;

@Configuration
//注意，由于MapperScannerConfigurer执行的比较早，所以必须有下面的注解
@AutoConfigureAfter(DatasourceConfig.class)
public class MyBatisMapperScannerConfig {
	 	@Bean
	 	@Primary
	    public MapperScannerConfigurer mapperScannerConfigurer() {
	 		 MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
		        mapperScannerConfigurer.setSqlSessionFactoryBeanName("sqlSessionFactoryMySql");
		        mapperScannerConfigurer.setBasePackage("com.gtmc.gclub.chat.dao");
		        mapperScannerConfigurer.setAnnotationClass(MySQLDb.class);
		        Properties properties = new Properties();
		        properties.setProperty("mappers", "com.gtmc.gclub.chat.util.MyMapper");
		        properties.setProperty("notEmpty", "false");
		        properties.setProperty("IDENTITY", "MYSQL");
		        mapperScannerConfigurer.setProperties(properties);
		        return mapperScannerConfigurer;
	    }
	 	
//	 	@Bean
//	    public MapperScannerConfigurer mapperScannerConfigurerOracle() {
//	 		 MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
//		        mapperScannerConfigurer.setSqlSessionFactoryBeanName("sqlSessionFactoryOracle");
//		        mapperScannerConfigurer.setBasePackage("com.gtmc.gclub.chat.dao");
//		        mapperScannerConfigurer.setAnnotationClass(OracleDb.class);
//		        Properties properties = new Properties();
//		        properties.setProperty("mappers", "com.gtmc.gclub.chat.util.MyMapper");
//		        properties.setProperty("notEmpty", "false");
//		        properties.setProperty("IDENTITY", "ORACLE");
//		        mapperScannerConfigurer.setProperties(properties);
//		        return mapperScannerConfigurer;
//	    }
}