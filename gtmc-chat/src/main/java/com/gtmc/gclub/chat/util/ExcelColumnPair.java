/**
 * ExcelColumnPair.java
 * Created at 2015年8月14日
 * Created by Renyifei
 * Copyright (C) 2015 SHANGHAI VOLKSWAGEN, All rights reserved.
 */
package com.gtmc.gclub.chat.util;

import java.util.Map;

/**
 * <p>ClassName: ExcelColumnPair</p>
 * <p>Description: TODO</p>
 * <p>Author: Renyifei</p>
 * <p>Date: 2015年8月14日</p>
 */
public class ExcelColumnPair extends Pair{
    private Formatter formatter;

    /**
     * <p>Description: TODO</p>
     * @param key
     * @param value
     */
    public ExcelColumnPair(String key, String value) {
        super(key, value);
    }
    
    public ExcelColumnPair(String key, String value,Formatter formatter){
        super(key, value);
        this.formatter = formatter;
    }
    
    /**
     * <p>Description: TODO</p>
     * @return the formatter
     */
    public Formatter getFormatter() {
        return formatter;
    }

    /**
     * <p>Description: TODO</p>
     * @param formatter the formatter to set
     */
    public void setFormatter(Formatter formatter) {
        this.formatter = formatter;
    }



    /**
     * 
     * <p>ClassName: Formatter</p>
     * <p>Description: 格式化回调接口</p>
     * <p>Author: Renyifei</p>
     * <p>Date: 2015年8月14日</p>
     */
    public interface Formatter{
        Object format(Object value,Map<String,Object> rec);
    }

}
