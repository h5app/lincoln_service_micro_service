/**
 * ExcelExport.java
 * Created at 2015年8月13日
 * Created by Renyifei
 * Copyright (C) 2015 SHANGHAI VOLKSWAGEN, All rights reserved.
 */
package com.gtmc.gclub.chat.util;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.gtmc.gclub.chat.util.ExcelColumnPair.Formatter;
import com.infoservice.filestore.FileStore;


public class SimpleExcelExport {
    final static Logger logger = Logger.getLogger(SimpleExcelExport.class);
    /**
     * 
     * <p>Description: 下载Excel文件</p>
     * @param columns 表头
     * @param data 数据
     * @throws Exception
     */
    public static String getFileStoreDownloadUrl(List<Pair> columns,List<Map<String,Object>> data,String name){
        try{
            HSSFWorkbook hssfWorkbook = new HSSFWorkbook();
            HSSFSheet createSheet = hssfWorkbook.createSheet();
            HSSFRow row = createSheet.createRow(0);
            writeHeader(row,columns);
            if(data != null){
            	//写入数据
                for (int di = 0,ri = 1; di < data.size(); di++,ri++) {
                    row = createSheet.createRow(ri);
                    writeRow(row,columns,data.get(di));
                }
            }
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            hssfWorkbook.write(out);
            byte[] b = out.toByteArray();
    		String fileUrl = FileStore.getInstance().write("nd02", "fs01", name, b);
    		return fileUrl;
        }catch(Exception e){
            logger.error("Excel下载失败",e);
            e.printStackTrace();
        }
        return null;
    }
    
    /**
     * <p>Description: Excel写入本地文件</p>
     * @param columns 表头
     * @param data 数据
     * @param path
     * @throws Exception
     */
    public static void writeToFs(List<Pair> columns,List<Map<String,Object>> data,String path){
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(path);
            HSSFWorkbook hssfWorkbook = new HSSFWorkbook();
            HSSFSheet createSheet = hssfWorkbook.createSheet();
            HSSFRow row = createSheet.createRow(0);
            writeHeader(row,columns);
            //写入数据
            for (int di = 0,ri = 1; di < data.size(); di++,ri++) {
                row = createSheet.createRow(ri);
                writeRow(row,columns,data.get(di));
            }
            
          //  fileOutputStream.write(null);
            hssfWorkbook.write(fileOutputStream);
        }catch(Exception e){
            logger.error("Excel写入FS失败",e);
        }finally{
            if(fileOutputStream != null){
                try {
                    fileOutputStream.close();
                } catch (IOException e) {}
            }
        }
    }
    
    private static void writeRow(HSSFRow row,List<Pair> columns,Map<String,Object> data){
        for (int i = 0; i < columns.size(); i++) {
            Pair pair = columns.get(i);
            Object value = data.get(pair.getKey());
            HSSFCell cell = row.createCell(i);
            if(pair instanceof ExcelColumnPair){
                Formatter formatter = ((ExcelColumnPair) pair).getFormatter();
                if(formatter != null && value!=null){
                    value = formatter.format(value, data);
                }
            }
            if(value instanceof Integer){
                cell.setCellValue((Integer)value);
            } else if(value instanceof Double){
                cell.setCellValue((Double)value);
            } else if(value instanceof Boolean){
                cell.setCellValue((Boolean)value);
            } else if(value instanceof Date){
                cell.setCellValue((Date)value);
            } else if(value instanceof BigDecimal){
                cell.setCellValue(((BigDecimal)value).doubleValue());
            } else if(value instanceof Long){
                cell.setCellValue((Long)value);
            } else {
                if(value!=null){
                    cell.setCellValue(value.toString());
                } else {
                    cell.setCellValue("");
                }
            }
        }
    }
    
    private static void writeHeader(HSSFRow row,List<Pair> columns){
      //写入表头
        for (int i = 0; i < columns.size(); i++) {
            Pair pair = columns.get(i);
            HSSFCell cell = row.createCell(i);
            cell.setCellValue(pair.getValue());
        }
    }
}