package com.gtmc.gclub.chat.bean;

public class BizException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	private String code;
	private String message;

	public BizException() {
		super();
	}

	public BizException(String code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	public BizException(String message) {
		super();
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
