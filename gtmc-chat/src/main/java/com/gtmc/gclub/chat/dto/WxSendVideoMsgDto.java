package com.gtmc.gclub.chat.dto;

/**
 * @author kaihu
 * 发送微信视频消息时使用的参数，用于规范视频消息发送接口的参数
 * 当前阶段未完成，后续优化使用
 */
public class WxSendVideoMsgDto extends WxSendMsgBaseDto{
	private String thumb_media_id;
	private String title;
	private String description;
	public String getThumb_media_id() {
		return thumb_media_id;
	}
	public void setThumb_media_id(String thumb_media_id) {
		this.thumb_media_id = thumb_media_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Override
	public String toString() {
		return "WxSendVideoMsgDto [thumb_media_id=" + thumb_media_id + ", title=" + title + ", description="
				+ description + "]";
	}
}
