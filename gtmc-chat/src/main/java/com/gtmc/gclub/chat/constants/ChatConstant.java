package com.gtmc.gclub.chat.constants;

public class ChatConstant {

	public static final byte CHAT_DIRECTION_WX = 0; // 微信客服发起的聊天
	public static final byte CHAT_DIRECTION_APP = 1; // app用户发起的聊天

	public static final int RESPONSE_CODE_CREATE_SUC = 201;

	public static final byte SHOU_QIAN = 0;// 售前
	public static final byte SHOU_HOU = 1;// 售后

	public static final String WX_SMALL_IMAGE = "0";

	public static final int RESULT_RETURN_FLAG_CONNECTION = 10001;// 连接错误
	public static final int RESULT_RETURN_FLAG_DB = 10002;// db错误
	public static final int RESULT_RETURN_FLAG_DATA = 10003;// 数据错误

	public static final String JMESSAGE_DEFAULT_PWD = "123456";// 默认注册jmessage帐号密码

	// 返回的错误代码
	public static final int SERVER_ERRORCODE_USER_EXIST = 899001;

	public static final String IMESSAGE_PREFIX = "wx_";

	public static final int LOG_EXPIRE_DAYS = 60; 
	
//	public static final String ENDSTR1 = "您好，请问您是否还在线？还有其他可以帮到您吗？";
	public static final String ENDSTR = "您好。由于您长时间没有应答，会话将被关闭。感谢您对林肯中国的支持。如有其他咨询欢迎再次联系人工线上客服，或致电林肯客服 中心电话 ：400-988-6789，祝您生活愉快，再见！";

}
