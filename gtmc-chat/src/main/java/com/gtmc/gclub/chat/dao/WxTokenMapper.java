package com.gtmc.gclub.chat.dao;

import com.gtmc.gclub.chat.annotation.MySQLDb;
import com.gtmc.gclub.chat.model.WxToken;

import tk.mybatis.mapper.common.Mapper;

@MySQLDb
public interface WxTokenMapper extends Mapper<WxToken> {
}