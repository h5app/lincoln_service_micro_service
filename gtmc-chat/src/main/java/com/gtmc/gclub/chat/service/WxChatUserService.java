package com.gtmc.gclub.chat.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gtmc.gclub.chat.bean.Result;
import com.gtmc.gclub.chat.dao.TrUserEnterpriseMapper;
import com.gtmc.gclub.chat.dao.WxUserOwnerMapper;
import com.gtmc.gclub.chat.jmessage.UserOperator;
import com.gtmc.gclub.chat.model.TrUserEnterprise;
import com.gtmc.gclub.chat.util.DateUtil;

@Service
@Transactional
public class WxChatUserService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private TrUserEnterpriseMapper wxChatUserMapper;
	
	@Autowired
	private WxUserOwnerMapper wxUserOwnerMapper;
	
	@Autowired
	private TrUserEnterpriseMapper trUserEnterpriseMapper;
	
	@Autowired
	private UserOperator userOperator;
	
	// 查询销售店客服列表
	public List<TrUserEnterprise> getList(TrUserEnterprise user) {
		//WxChatUser rtn = wxChatUserMapper.se.s;
		return wxChatUserMapper.select(user);
	}
	
	// 获得某位客服
	public TrUserEnterprise getOne(TrUserEnterprise user) {
		return wxChatUserMapper.selectOne(user);
	}
	
	/**
	 * 根据林肯系统userId获取客服信息 
	 * */
	public TrUserEnterprise getChatUserByLKUser(String userId) {
		return wxChatUserMapper.getChatUserByLKUser(userId);
	}
	
	/**
	 * 根据林肯系统userId获取客服信息 
	 * */
	public Integer getChatUserIdByLKUserId(Integer userId) {
		TrUserEnterprise chatUserByLKUser = wxChatUserMapper.getChatUserByLKUser(userId.toString());
		if(chatUserByLKUser==null) {
			logger.error("无法查询到林肯用户");
			return null;
		}
		return chatUserByLKUser.getId().intValue();
	}
	
	/**
	 * 策略获取客服：
	 * 若当前用户在指定范围的时间内(目前为当天)已经存在对应的客服，且在线，则返回此客服；若不在线，则按照固定时间内不存在客服的逻辑筛选。
	 * 若当前用户在指定范围的时间内(目前为当天)不存在已经对应的客服，则查找指定范围的时间内对应人数最少的，且在线的客服。
	 * @param param
	 * @return
	 */
	public TrUserEnterprise getLeisureService(List<String> userIdList) {
		// 获取客服列表，按照从少到多的顺序排序
		// 如果确定时间内(暂为当天)已经存在客服，则返回此客服
		List<TrUserEnterprise> leisureService = trUserEnterpriseMapper.getChatUsers(userIdList);
		// 逐一判断，找出在线的，对应人数最少的客服
		TrUserEnterprise res = null ;
		for(TrUserEnterprise tempUser : leisureService) {
			Boolean onlineFlag = false;
			// 判断是否在线.未完成，这里可以优化，可以不用每次都去调极光获取状态，可以维护这个状态，然后去数据库里获取这个 状态
			Result<Map> onlineResult = userOperator.getUserState(tempUser.getJmessageCode());
			if (onlineResult.getReturnFlag() == 1 && onlineResult.getData() != null) {
				onlineFlag = (Boolean) onlineResult.getData().get("online");
			}
			if(onlineFlag) {
				res = tempUser;
				break;
			}
		}
		return res;
	}
	
	public Integer updateChatInfo(TrUserEnterprise entity) {
		return trUserEnterpriseMapper.updateByPrimaryKeySelective(entity);
	}
	
	public void loginOut(Integer chatIds) {
		if(chatIds==null) {
			logger.error("需要下线的客服为空！"+chatIds);return;
		}
		trUserEnterpriseMapper.loginOut(chatIds);
	}
	
}
