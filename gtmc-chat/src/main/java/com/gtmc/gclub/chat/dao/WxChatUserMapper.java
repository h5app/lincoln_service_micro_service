//package com.gtmc.gclub.chat.dao;
//
//import java.util.List;
//import java.util.Map;
//
//import com.gtmc.gclub.chat.annotation.MySQLDb;
//import com.gtmc.gclub.chat.model.WxChatUser;
//
//import tk.mybatis.mapper.common.Mapper;
//
//@MySQLDb
//public interface WxChatUserMapper extends Mapper<WxChatUser> {
//
//	int insertUser(WxChatUser wxUser);
//
//	void deleteAll();
//
//	void insertByBatchId(List<WxChatUser> yList);
//
//	void insertByBatch(List<WxChatUser> xList);
//	
//	List<WxChatUser> getWxUserList(Map<String, Object> map);
//	
//	/**
//	 * 获取客服列表，按照目前聊天对应人数从少到多排序
//	 * */
//	public List<WxChatUser> getLeisureService(Map<String,Object> map);
//	
//	/**
//	 * 根据林肯系统userId获取客服信息 
//	 * */
//	public WxChatUser getChatUserByLKUser(String userId);
//}