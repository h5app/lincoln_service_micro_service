package com.gtmc.gclub.chat.constants;

public class MessageConstant {

    public static final String GCLUB_ACTIVITY_QUEUE="GCLUB.ACTIVITY";
    
    public static final String GCLUB_USER_QUEUE="GCLUB.USER";
    
    public static final String ICLUB_ACTIVITY_QUEUE="ICLUB.ACTIVITY";
    
    public static final String ICLUB_USER_QUEUE="ICLUB.USER";
    
    public static final String W_ACTIVITY_QUEUE="WCLUB.ACTIVITY";
    
    public static final String W_USER_QUEUE="WCLUB.USER";
    
    public static final String GTMC_EXCHANGES_TOPIC_GCLUB="gclub.topic";

    /**
     * 消息的类别
     * ACTIVITY 代表活动
     * USER	代表用户信息
     * 
     * @author BENJAMIN
     */
    public enum MESSAGE_TYPE {
        ACTIVITY("ACTIVITY"),USER("USER");

        private final String type;

        MESSAGE_TYPE(String type) {
            this.type = type;
        }

        public String toString() {
            return type.toString();
        }
    }



  
}
