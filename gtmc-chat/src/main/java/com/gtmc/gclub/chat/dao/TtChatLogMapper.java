package com.gtmc.gclub.chat.dao;

import java.util.List;

import com.gtmc.gclub.chat.annotation.MySQLDb;
import com.gtmc.gclub.chat.dto.ChatParam;
import com.gtmc.gclub.chat.model.TtChatLog;

import tk.mybatis.mapper.common.Mapper;

//@OracleDb
@MySQLDb
public interface TtChatLogMapper extends Mapper<TtChatLog> {

	public List<ChatParam> queryAllCustomerList(ChatParam param) throws Exception;

	public List<ChatParam> queryChatListByCustomer(ChatParam param);
}