package com.gtmc.gclub.chat.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.gtmc.gclub.chat.annotation.MySQLDb;
import com.gtmc.gclub.chat.annotation.OracleDb;

import com.gtmc.gclub.chat.model.TmPotentialUser;

import tk.mybatis.mapper.common.Mapper;



//@OracleDb
@MySQLDb
public interface TmPotentialUserMapper extends Mapper<TmPotentialUser> {
	
	public Integer updateJmessageCode(Map<String,String> param) ;

	public List<TmPotentialUser> queryOwnerByIds(@Param("resIds") List<String> resIds);
}