package com.gtmc.gclub.chat.dto;
/**
 * 微信公众号消息参数实体
 * **/
public class WxGzMessageDto {
	private String toUserName;// 	开发者微信号
	private String fromUserName;// 	发送方帐号（一个OpenID）
	private String createTime;//	消息创建时间 （整型） 
	private String msgType;// 		消息类型
	private String content;// 	文本消息内容
	private String msgId;// 	文本/图片消息id，64位整型
	private String msgID;// 	语音消息id，64位整型
	private String mediaId;// 	图片消息媒体id，可以调用多媒体文件下载接口拉取数据。/语音消息媒体id，可以调用多媒体文件下载接口拉取数据。
	private String picUrl;// 	图片链接（由系统生成）
	private String format;//	语音格式：amr
	private String recognition;//  语音识别结果，UTF8编码
	private String thumbMediaId;// 视频消息缩略图的媒体id，可以调用多媒体文件下载接口拉取数据。
	
	private String location_X;//   地理位置维度
	private String location_Y;//   地理位置经度
	private String scale;// 		地图缩放大小
	private String label;//   		地理位置信息
	
	private String title;//   		消息标题
	private String url;//   		消息链接
	private String description;//   		消息描述
	
	private String userId; // 	发送信息的用户ID
	private String phone; // 	发送信息的用户手机号
	
	private String dateShow;// 是否显示
	private String jmessageCode;// 发送的jmessageCode
	public String getToUserName() {
		return toUserName;
	}
	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}
	public String getFromUserName() {
		return fromUserName;
	}
	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getMsgType() {
		return msgType;
	}
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getMsgId() {
		return msgId;
	}
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
	public String getMsgID() {
		return msgID;
	}
	public void setMsgID(String msgID) {
		this.msgID = msgID;
	}
	public String getMediaId() {
		return mediaId;
	}
	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}
	public String getPicUrl() {
		return picUrl;
	}
	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public String getRecognition() {
		return recognition;
	}
	public void setRecognition(String recognition) {
		this.recognition = recognition;
	}
	public String getThumbMediaId() {
		return thumbMediaId;
	}
	public void setThumbMediaId(String thumbMediaId) {
		this.thumbMediaId = thumbMediaId;
	}
	public String getLocation_X() {
		return location_X;
	}
	public void setLocation_X(String location_X) {
		this.location_X = location_X;
	}
	public String getLocation_Y() {
		return location_Y;
	}
	public void setLocation_Y(String location_Y) {
		this.location_Y = location_Y;
	}
	public String getScale() {
		return scale;
	}
	public void setScale(String scale) {
		this.scale = scale;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getJmessageCode() {
		return jmessageCode;
	}
	public void setJmessageCode(String jmessageCode) {
		this.jmessageCode = jmessageCode;
	}
	public String getDateShow() {
		return dateShow;
	}
	public void setDateShow(String dateShow) {
		this.dateShow = dateShow;
	}
	@Override
	public String toString() {
		return "WxGzMessageDto [toUserName=" + toUserName + ", fromUserName=" + fromUserName + ", createTime="
				+ createTime + ", msgType=" + msgType + ", content=" + content + ", msgId=" + msgId + ", msgID=" + msgID
				+ ", mediaId=" + mediaId + ", picUrl=" + picUrl + ", format=" + format + ", recognition=" + recognition
				+ ", thumbMediaId=" + thumbMediaId + ", location_X=" + location_X + ", location_Y=" + location_Y
				+ ", scale=" + scale + ", label=" + label + ", title=" + title + ", url=" + url + ", description="
				+ description + ", userId=" + userId + ", phone=" + phone + ", dateShow=" + dateShow + ", jmessageCode="
				+ jmessageCode + "]";
	}
}
