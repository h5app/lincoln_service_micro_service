package com.gtmc.gclub.chat.dto;

/**
 * @author kaihu
 * 客服选择策略中，客服排队信息实体
 *
 */
public class ChatQueueDetailDto {
	/**
	 * 客服ID
	 */
	private String chatId;
	/**
	 * 用户ID
	 */
	private String ownerCode;
	/**
	 * 队列深度
	 */
	private Integer queueNo;
	/**
	 * 消息内容
	 */
	private String content;
	public String getChatId() {
		return chatId;
	}
	public void setChatId(String chatId) {
		this.chatId = chatId;
	}
	public String getOwnerCode() {
		return ownerCode;
	}
	public void setOwnerCode(String ownerCode) {
		this.ownerCode = ownerCode;
	}
	public Integer getQueueNo() {
		return queueNo;
	}
	public void setQueueNo(Integer queueNo) {
		this.queueNo = queueNo;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	@Override
	public String toString() {
		return "ChatQueueDetailDto [chatId=" + chatId + ", ownerCode=" + ownerCode + ", queueNo=" + queueNo
				+ ", content=" + content + "]";
	}
}
