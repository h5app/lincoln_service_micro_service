package com.gtmc.gclub.chat.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author kaihua
 *
 */
@Table(name = "TT_CHAT_LOG_FILE")
public class TtChatLogFile {

	@Id
	@Column(name="ID")
	private Integer id;
	
	/**
	 * 潜客ID
	 */
	@Column(name="POTENTIAL_USER_ID")
	private String potentialUserId;
	/**
	 * 文件名
	 */
	@Column(name="FILE_NAME")
	private String fileName;
	/**
	 * 文件地址
	 */
	@Column(name="FILE_URL")
	private String fileUrl;
	/**
	 * 客服ID（林肯系统后台ID）
	 */
	@Column(name="SERVICE_ID")
	private String serviceId;
	/**
	 * 聊天记录时间
	 */
	@Column(name = "CREATE_DATE")
	private Date createDate;
	/**
	 * 创建人ID
	 */
	@Column(name = "CREATE_BY")
	private Integer createBy;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPotentialUserId() {
		return potentialUserId;
	}
	public void setPotentialUserId(String potentialUserId) {
		this.potentialUserId = potentialUserId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileUrl() {
		return fileUrl;
	}
	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Integer getCreateBy() {
		return createBy;
	}
	public void setCreateBy(Integer createBy) {
		this.createBy = createBy;
	}
	@Override
	public String toString() {
		return "TtChatLogFile [id=" + id + ", potentialUserId=" + potentialUserId + ", fileName=" + fileName
				+ ", fileUrl=" + fileUrl + ", serviceId=" + serviceId + ", createDate=" + createDate + ", createBy="
				+ createBy + "]";
	}
}
