package com.gtmc.gclub.chat.dto;

import java.util.Date;
import java.util.List;

import com.gtmc.gclub.common.WxMaterialTypeEnum;

public class ChatMsgDetailDto{
	private String formeUser;
	private WxMaterialTypeEnum msgType;
	/**
	 * 消息内容<br/>
	 * 由于在等待的过程中，用户可能发送多条消息过来，此时修改消息内容，并且要保证等待队列不动<br/>
	 * 此时就需要用数组来存储用户发送的所有消息
	 */
	private List<ChatMessageDto> allContent;
	private String content;
	private Date createTime;
	public String getFormeUser() {
		return formeUser;
	}
	public void setFormeUser(String formeUser) {
		this.formeUser = formeUser;
	}
	public WxMaterialTypeEnum getMsgType() {
		return msgType;
	}
	public void setMsgType(WxMaterialTypeEnum msgType) {
		this.msgType = msgType;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public List<ChatMessageDto> getAllContent() {
		return allContent;
	}
	public void setAllContent(List<ChatMessageDto> allContent) {
		this.allContent = allContent;
	}
	@Override
	public String toString() {
		return "ChatMsgDetailDto [formeUser=" + formeUser + ", msgType=" + msgType + ", allContent=" + allContent
				+ ", content=" + content + ", createTime=" + createTime + "]";
	}
}