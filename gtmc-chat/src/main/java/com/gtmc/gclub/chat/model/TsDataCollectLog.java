package com.gtmc.gclub.chat.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TS_DATA_COLLECT_LOG")
public class TsDataCollectLog {
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    @Column(name = "MESSAGE_ID")
    private Integer messageId;

    @Column(name = "ACTION_CLASS_NAME")
    private String actionClassName;

    @Column(name = "PROCESS_START_DATE")
    private Date processStartDate;

    @Column(name = "PROCESS_END_DATE")
    private Date processEndDate;

    @Column(name = "RETURN_FLAG")
    private Integer returnFlag;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return MESSAGE_ID
     */
    public Integer getMessageId() {
        return messageId;
    }

    /**
     * @param messageId
     */
    public void setMessageId(Integer messageId) {
        this.messageId = messageId;
    }

    /**
     * @return ACTION_CLASS_NAME
     */
    public String getActionClassName() {
        return actionClassName;
    }

    /**
     * @param actionClassName
     */
    public void setActionClassName(String actionClassName) {
        this.actionClassName = actionClassName;
    }

    /**
     * @return PROCESS_START_DATE
     */
    public Date getProcessStartDate() {
        return processStartDate;
    }

    /**
     * @param processStartDate
     */
    public void setProcessStartDate(Date processStartDate) {
        this.processStartDate = processStartDate;
    }

    /**
     * @return PROCESS_END_DATE
     */
    public Date getProcessEndDate() {
        return processEndDate;
    }

    /**
     * @param processEndDate
     */
    public void setProcessEndDate(Date processEndDate) {
        this.processEndDate = processEndDate;
    }

    /**
     * @return RETURN_FLAG
     */
    public Integer getReturnFlag() {
        return returnFlag;
    }

    /**
     * @param returnFlag
     */
    public void setReturnFlag(Integer returnFlag) {
        this.returnFlag = returnFlag;
    }

	@Override
	public String toString() {
		return "TsDataCollectLog [id=" + id + ", messageId=" + messageId + ", actionClassName=" + actionClassName
				+ ", processStartDate=" + processStartDate + ", processEndDate=" + processEndDate + ", returnFlag="
				+ returnFlag + "]";
	}
}