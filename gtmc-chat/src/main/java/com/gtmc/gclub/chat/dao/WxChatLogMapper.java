package com.gtmc.gclub.chat.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.gtmc.gclub.chat.annotation.MySQLDb;
import com.gtmc.gclub.chat.dto.WxChatLogExportDto;
import com.gtmc.gclub.chat.model.WxChatLog;
import com.gtmc.gclub.chat.vo.WxChatLogExportVo;

import tk.mybatis.mapper.common.Mapper;

@MySQLDb
public interface WxChatLogMapper extends Mapper<WxChatLog> {

	int upDateShow(Long msgId);

	List<Map<String, Object>> getWxChatTalk(Map<String, Object> map);

	int getWxChatTalkCount(Map<String, Object> map);

	Long getWxChatCount(Map<String, Object> map);

	List<Map<String, Object>> getWxChatLog(Map<String, Object> map);

	int deleteExpire(@Param("days") Integer days);

	int deleteLog(String cleanDate);

	int insertBase(WxChatLog log);

	/*
	 * int countByExample(WxChatLogExample example);
	 * 
	 * int deleteByExample(WxChatLogExample example);
	 * 
	 * List<WxChatLog> selectByExample(WxChatLogExample example);
	 * 
	 * int updateByExampleSelective(@Param("record") WxChatLog
	 * record, @Param("example") WxChatLogExample example);
	 * 
	 * int updateByExample(@Param("record") WxChatLog record, @Param("example")
	 * WxChatLogExample example);
	 */
	/**
	 * 导出聊天记录
	 * @param searchDto
	 * @return
	 */
	List<WxChatLogExportVo> exportWxChatTalk(WxChatLogExportDto searchDto);
}