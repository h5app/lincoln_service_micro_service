package com.gtmc.gclub.chat.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TT_ADVISER")
public class TtAdviser {
    @Id
    @Column(name = "SA_CODE")
    private String saCode;

    @Column(name = "MSG_ID")
    private String msgId;

    @Column(name = "DEALER_CODE")
    private String dealerCode;

    @Column(name = "EMPLOY_TYPE")
    private Short employType;

    @Column(name = "SA_NAME")
    private String saName;

    @Column(name = "SA_PHONE")
    private String saPhone;

    @Column(name = "DELETE_FLAG")
    private Short deleteFlag;

    @Column(name = "CREATE_BY")
    private Integer createBy;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "UPDATE_BY")
    private Integer updateBy;

    @Column(name = "UPDATE_DATE")
    private Date updateDate;

    /**
     * @return SA_CODE
     */
    public String getSaCode() {
        return saCode;
    }

    /**
     * @param saCode
     */
    public void setSaCode(String saCode) {
        this.saCode = saCode;
    }

    /**
     * @return MSG_ID
     */
    public String getMsgId() {
        return msgId;
    }

    /**
     * @param msgId
     */
    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    /**
     * @return DEALER_CODE
     */
    public String getDealerCode() {
        return dealerCode;
    }

    /**
     * @param dealerCode
     */
    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    /**
     * @return EMPLOY_TYPE
     */
    public Short getEmployType() {
        return employType;
    }

    /**
     * @param employType
     */
    public void setEmployType(Short employType) {
        this.employType = employType;
    }

    /**
     * @return SA_NAME
     */
    public String getSaName() {
        return saName;
    }

    /**
     * @param saName
     */
    public void setSaName(String saName) {
        this.saName = saName;
    }

    /**
     * @return SA_PHONE
     */
    public String getSaPhone() {
        return saPhone;
    }

    /**
     * @param saPhone
     */
    public void setSaPhone(String saPhone) {
        this.saPhone = saPhone;
    }

    /**
     * @return DELETE_FLAG
     */
    public Short getDeleteFlag() {
        return deleteFlag;
    }

    /**
     * @param deleteFlag
     */
    public void setDeleteFlag(Short deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    /**
     * @return CREATE_BY
     */
    public Integer getCreateBy() {
        return createBy;
    }

    /**
     * @param createBy
     */
    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }

    /**
     * @return CREATE_DATE
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return UPDATE_BY
     */
    public Integer getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy
     */
    public void setUpdateBy(Integer updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * @return UPDATE_DATE
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}