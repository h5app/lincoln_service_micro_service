package com.gtmc.gclub.chat.vo;

import java.util.HashMap;
import java.util.Map;

import com.gtmc.gclub.common.WxMaterialTypeEnum;

/**
 * @author kaihua
 *	保存微信永久素材，作为常见内容中的媒体内容出现
 */
public class WxMaterialDetailH5 {
	private String tagName;
	private String media_id;
	private String title;
	private String description;
	private String down_url;
	private String type;
	private String content;
	private String thumb_media_id;
	private String show_cover_pic;
	private String author;
	private String digest;
	private String url;
	private String content_source_url;
	
//	entityDetail.put("title", wxMaterialNewsDetail.getTitle());
//	entityDetail.put("thumb_media_id", wxMaterialNewsDetail.getThumb_media_id());
//	entityDetail.put("show_cover_pic", wxMaterialNewsDetail.getShow_cover_pic()==null ? null : wxMaterialNewsDetail.getShow_cover_pic().toString());
//	entityDetail.put("author", wxMaterialNewsDetail.getAuthor());
//	entityDetail.put("digest", wxMaterialNewsDetail.getDigest());
//	entityDetail.put("content", wxMaterialNewsDetail.getContent());
//	entityDetail.put("url", wxMaterialNewsDetail.getUrl());
//	entityDetail.put("content_source_url", wxMaterialNewsDetail.getContent_source_url());
//	entityDetail.put("type", WxMaterialTypeEnum.news.name());
	
//	Map<String,Object> root = new HashMap<String,Object>();
//	root.put("tagName", entity.getName());
//	root.put("media_id", entity.getMedia_id());
//	Map<String,String> entityDetail = new HashMap<String,String>();
//	entityDetail.put("title", entity.getTitle());
//	entityDetail.put("description", entity.getDescription());
//	entityDetail.put("down_url", entity.getUrl());
//	entityDetail.put("type", type.name());// 这里从永久素材列表是拿不到素材类型的,只能从调用出获取
//	root.put("content", entityDetail);
//	return root;
	public String getTagName() {
		return tagName;
	}
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
	public String getMedia_id() {
		return media_id;
	}
	public void setMedia_id(String media_id) {
		this.media_id = media_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDown_url() {
		return down_url;
	}
	public void setDown_url(String down_url) {
		this.down_url = down_url;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getThumb_media_id() {
		return thumb_media_id;
	}
	public void setThumb_media_id(String thumb_media_id) {
		this.thumb_media_id = thumb_media_id;
	}
	public String getShow_cover_pic() {
		return show_cover_pic;
	}
	public void setShow_cover_pic(String show_cover_pic) {
		this.show_cover_pic = show_cover_pic;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getDigest() {
		return digest;
	}
	public void setDigest(String digest) {
		this.digest = digest;
	}
	/**
	 * @return 微信url原地址
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param 微信url原地址
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	public String getContent_source_url() {
		return content_source_url;
	}
	public void setContent_source_url(String content_source_url) {
		this.content_source_url = content_source_url;
	}
	public void setType(WxMaterialTypeEnum type) {
		this.type = type.name();
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getType() {
		return type;
	}
	public Map<String,Object> getWxMaterialMediaForH5() {
		Map<String,Object> root = new HashMap<String,Object>();
		root.put("tagName", getTitle());
		root.put("media_id", getMedia_id());
		Map<String,String> entityDetail = new HashMap<String,String>();
		entityDetail.put("title", getTitle());
		entityDetail.put("description", getDescription());
		entityDetail.put("down_url", getDown_url());
		entityDetail.put("type", getType());// 这里从永久素材列表是拿不到素材类型的,只能从调用出获取
		root.put("content", entityDetail);
		return root;
	}
	public Map<String,Object> getWxMaterialNewsForH5() {
		Map<String,Object> root = new HashMap<String,Object>();
		root.put("tagName", getTitle());
		root.put("media_id", getMedia_id());
		Map<String,String> entityDetail = new HashMap<String,String>();
		entityDetail.put("title", getTitle());
		entityDetail.put("thumb_media_id", getThumb_media_id());
		entityDetail.put("show_cover_pic", getShow_cover_pic()==null ? null : getShow_cover_pic().toString());
		entityDetail.put("author", getAuthor());
		entityDetail.put("digest", getDigest());
		entityDetail.put("content", getContent());
		entityDetail.put("url", getUrl());
		entityDetail.put("content_source_url", getContent_source_url());
		entityDetail.put("type", getType());
		root.put("content", entityDetail);
		return root;
	}
	@Override
	public String toString() {
		return "WxMaterialDetailH5 [tagName=" + tagName + ", media_id=" + media_id + ", title=" + title
				+ ", description=" + description + ", down_url=" + down_url + ", type=" + type + ", content=" + (content==null ? "null" : content.length() )
				+ ", thumb_media_id=" + thumb_media_id + ", show_cover_pic=" + show_cover_pic + ", author=" + author
				+ ", digest=" + digest + ", url=" + url + ", content_source_url=" + content_source_url + "]";
	}
}