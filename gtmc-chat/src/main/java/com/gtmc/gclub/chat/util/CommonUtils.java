/*
* Copyright 2016 Yonyou Auto Information Technology（Shanghai） Co., Ltd. All Rights Reserved.
*
* This software is published under the terms of the YONYOU Software
* License version 1.0, a copy of which has been included with this
* distribution in the LICENSE.txt file.
*
* @Project Name : marketing-service-newcar-showroom
*
* @File name : CommonUtils.java
*
* @Author : LiuJun
*
* @Date : 2016年11月1日
*
----------------------------------------------------------------------------------
*     Date       Who       Version     Comments
* 1. 2016年11月1日    LiuJun    1.0
*
*
*
*
----------------------------------------------------------------------------------
*/
	
package com.gtmc.gclub.chat.util;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collection;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

/**
*
* @author LiuJun
* 公共工具类
* @date 2016年11月1日
*/
public class CommonUtils {
    
    public final static String EMPTY_STRING = "";

    
    /**
     * check if null or empty string
     */
    public static boolean isNullOrEmpty(String str) {
        return (null == str || EMPTY_STRING.equals(str));
    }

    /**
     * check if the list is null or empty
     */
    public static boolean isNullOrEmpty(Collection<?> list) {
        return list == null || list.isEmpty();
    }

    /**
     * check if the map is null or empty
     */
    public static boolean isNullOrEmpty(Map<?, ?> map) {
        return map == null || map.isEmpty();
    }
    
    /**
     * check if the integer is null or 0
     */
    public static boolean isNullOrO(Integer integer) {
        return integer == null || integer.equals(0);
    }

    /**
     * check if the long is null or 0
     */
    public static boolean isNullOrOLong(Long l) {
        return l == null || l.equals(0L);
    }
    
    /**
     * check if the bigDecimal is null or 0
     */
    public static boolean isNullOrOBigDecimal(BigDecimal bigd) {
        if(bigd == null){
            return true;
        }
        int num= bigd.compareTo(BigDecimal.ZERO);
        return num==0;
    }
    
    /**
     * 输出获得对象中所有成员变量的值
     * 
     * @author WangHuicheng
     * @date 2016年12月23日
     * @param obj
     * @return
     */
    public static String toString(Object obj) {
        StringBuffer result = new StringBuffer();
        result.append(obj.getClass().getName() + "的成员变量值如下：");
        try {
            Field[] fields = obj.getClass().getDeclaredFields();

            for (int i = 0; i < fields.length; i++) {
                fields[i].setAccessible(true);
                result.append(fields[i].getName() + ":" + fields[i].get(obj) + "\r\n");
                // System.out.print("成员变量" + i + "类型 : " + fields[i].getType().getName());
                // System.out.print("\t成员变量" + i + "变量名: " + fields[i].getName() + "\t");
                // System.out.println("成员变量" + i + "值: " + fields[i].get(aInstance));
            }
        } catch (Exception e) {
            e.printStackTrace();
            result.append(obj.getClass().getName() + "的成员变量值读取失败：" + e.getMessage());
        }
        return result.toString();
    }
    
    /**
     * 根据路径获取内容
     * 
     * @author QianKun
     * @date 2017年3月10日
     * @param naiId
     * @return
     * @throws Exception 
     */
    public static String getUrlToContent(String url) throws Exception  {

     // url = url.replaceAll("(?<=\\//)(.*?)(?=\\/f)", inforHost);
        if (StringUtils.isEmpty(url)) {
            return EMPTY_STRING;
        }
        InputStream inputStream = null;
        ByteArrayOutputStream output = null;
        try {
            URL urlTemp = new URL(url);
            HttpURLConnection httpConn = (HttpURLConnection) urlTemp.openConnection();

            httpConn.connect();
            inputStream = httpConn.getInputStream();
            // filestore

            output = new ByteArrayOutputStream();
            byte[] buffer = new byte[4096];
            int n = 0;
            while (-1 != (n = inputStream.read(buffer))) {
                output.write(buffer, 0, n);
            }
            String resultContent = output.toString("utf-8");
            return resultContent;
        } catch (Exception e) {
            throw new Exception(e.getMessage(), e);
        } finally {
            try {
                if (inputStream != null) inputStream.close();
            } catch (Exception e) {
            }
            try {
                if (output != null) output.close();
            } catch (Exception e) {
            }

        }
    }

}
