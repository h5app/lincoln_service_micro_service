package com.gtmc.gclub.chat.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.gtmc.gclub.chat.annotation.MySQLDb;
import com.gtmc.gclub.chat.model.TsDataCollectLog;

import tk.mybatis.mapper.common.Mapper;


@MySQLDb
public interface TsDataCollectLogMapper extends Mapper<TsDataCollectLog> {
	
	public int insertLogList(@Param("logs") List<TsDataCollectLog> logList);
	
}