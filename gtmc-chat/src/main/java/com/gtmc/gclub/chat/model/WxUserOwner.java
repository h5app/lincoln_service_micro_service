package com.gtmc.gclub.chat.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.gtmc.gclub.common.WxMaterialTypeEnum;

/**
 * 聊天关系表-会话列表
 * @author kaihua
 *
 */
@Table(name = "WX_USER_OWNER")
public class WxUserOwner {
	
	public static final Integer STATUS_USEFUL = 1 ;
	public static final Integer STATUS_NOUSE = 0 ;
	/**
	 * 编号
	 */
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;

	/**
	 * 客服编号
	 */
	@Column(name = "wx_user_id")
	private String wxUserId;

	/**
	 * app用户账号
	 */
	@Column(name = "owner_code")
	private String ownerCode;

	/**
	 * app用户的头像地址
	 */
	@Column(name = "owner_icon")
	private String ownerIcon;

	/**
	 * app用户昵称
	 */
	@Column(name = "owner_name")
	private String ownerName;

	/**
	 * app用户的imessage账号
	 */
	@Column(name = "owner_imessage_code")
	private String ownerImessageCode;

	/**
	 * 最后聊天内容
	 */
	@Column(name = "last_content")
	private String lastContent;
	
	/**
	 * 最后聊天内容消息类型
	 */
	@Column(name = "last_content_type")
	private String lastContentType;
	/**
	 * 若最后聊天内容是图文，最后聊天内容标题
	 */
	@Column(name = "last_content_title")
	private String lastContentTitle;
	/**
	 * 若最后聊天内容是媒体，最后聊天内容链接
	 */
	@Column(name = "last_content_url")
	private String lastContentUrl;

	/**
	 * 最后聊天时间
	 */
	@Column(name = "last_time")
	private Date lastTime;
	
	/**
	 * 最后聊天时间
	 */
	@Column(name = "first_time")
	private Date firstTime;

	/**
	 * 聊天方向：0,微信客服发起 1,app用户发起
	 */
	private Byte direction;

	/**
	 * 会话编号
	 */
	@Column(name = "session_id")
	private Integer sessionId;

	/**
	 * 最近登录时间
	 */
	@Column(name = "wx_view_time")
	private Date wxViewTime;

	/**
	 * 连发数目
	 */
	@Column(name = "wx_active_num")
	private Integer wxActiveNum;
	/**
	 * 链接状态
	 */
	@Column(name = "status")
	private Integer status;

	public Date getWxViewTime() {
		return wxViewTime;
	}

	public void setWxViewTime(Date wxViewTime) {
		this.wxViewTime = wxViewTime;
	}

	public Integer getWxActiveNum() {
		return wxActiveNum;
	}

	public void setWxActiveNum(Integer wxActiveNum) {
		this.wxActiveNum = wxActiveNum;
	}

	/**
	 * 获取编号
	 *
	 * @return id - 编号
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 设置编号
	 *
	 * @param id
	 *            编号
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 获取客服编号
	 *
	 * @return wx_user_id - 客服编号
	 */
	public String getWxUserId() {
		return wxUserId;
	}

	/**
	 * 设置客服编号
	 *
	 * @param wxUserId
	 *            客服编号
	 */
	public void setWxUserId(String wxUserId) {
		this.wxUserId = wxUserId;
	}

	/**
	 * 获取app用户账号
	 *
	 * @return owner_code - app用户账号
	 */
	public String getOwnerCode() {
		return ownerCode;
	}

	/**
	 * 设置app用户账号
	 *
	 * @param ownerCode
	 *            app用户账号
	 */
	public void setOwnerCode(String ownerCode) {
		this.ownerCode = ownerCode;
	}

	/**
	 * 获取app用户的头像地址
	 *
	 * @return owner_icon - app用户的头像地址
	 */
	public String getOwnerIcon() {
		return ownerIcon;
	}

	/**
	 * 设置app用户的头像地址
	 *
	 * @param ownerIcon
	 *            app用户的头像地址
	 */
	public void setOwnerIcon(String ownerIcon) {
		this.ownerIcon = ownerIcon;
	}

	/**
	 * 获取app用户昵称
	 *
	 * @return owner_name - app用户昵称
	 */
	public String getOwnerName() {
		return ownerName;
	}

	/**
	 * 设置app用户昵称
	 *
	 * @param ownerName
	 *            app用户昵称
	 */
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	/**
	 * 获取app用户的imessage账号
	 *
	 * @return owner_imessage_code - app用户的imessage账号
	 */
	public String getOwnerImessageCode() {
		return ownerImessageCode;
	}

	/**
	 * 设置app用户的imessage账号
	 *
	 * @param ownerImessageCode
	 *            app用户的imessage账号
	 */
	public void setOwnerImessageCode(String ownerImessageCode) {
		this.ownerImessageCode = ownerImessageCode;
	}

	/**
	 * 获取最后聊天内容
	 *
	 * @return last_content - 最后聊天内容
	 */
	public String getLastContent() {
		return lastContent;
	}

	/**
	 * 设置最后聊天内容
	 *
	 * @param lastContent
	 *            最后聊天内容
	 */
	public void setLastContent(String lastContent) {
		this.lastContent = lastContent;
	}

	public String getLastContentTitle() {
		return lastContentTitle;
	}

	public void setLastContentTitle(String lastContentTitle) {
		this.lastContentTitle = lastContentTitle;
	}

	public String getLastContentUrl() {
		return lastContentUrl;
	}

	public void setLastContentUrl(String lastContentUrl) {
		this.lastContentUrl = lastContentUrl;
	}

	/**
	 * 获取最后聊天时间
	 *
	 * @return last_time - 最后聊天时间
	 */
	public Date getLastTime() {
		return lastTime;
	}

	/**
	 * 设置最后聊天时间
	 *
	 * @param lastTime
	 *            最后聊天时间
	 */
	public void setLastTime(Date lastTime) {
		this.lastTime = lastTime;
	}
	/**
	 * 获取建立连接第一次聊天时间
	 *
	 * @return last_time - 建立连接第一次聊天时间
	 */
	public Date getFirstTime() {
		return firstTime;
	}
	/**
	 * 设置建立连接第一次聊天时间
	 *
	 * @param lastTime
	 *            建立连接第一次聊天时间
	 */
	public void setFirstTime(Date firstTime) {
		this.firstTime = firstTime;
	}

	/**
	 * 获取聊天方向：0,微信客服发起 1,app用户发起
	 *
	 * @return direction - 聊天方向：0,微信客服发起 1,app用户发起
	 */
	public Byte getDirection() {
		return direction;
	}

	/**
	 * 设置聊天方向：0,微信客服发起 1,app用户发起
	 *
	 * @param direction
	 *            聊天方向：0,微信客服发起 1,app用户发起
	 */
	public void setDirection(Byte direction) {
		this.direction = direction;
	}

	/**
	 * 获取会话编号
	 *
	 * @return session_id - 会话编号
	 */
	public Integer getSessionId() {
		return sessionId;
	}

	/**
	 * 设置会话编号
	 *
	 * @param sessionId
	 *            会话编号
	 */
	public void setSessionId(Integer sessionId) {
		this.sessionId = sessionId;
	}
	/**
	 * 获取连接状态
	 *
	 * @return status - 连接状态
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置连接状态
	 *
	 * @param status
	 *            连接状态
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getLastContentType() {
		return lastContentType;
	}

	public void setLastContentType(String lastContentType) {
		this.lastContentType = lastContentType;
	}
	public void setLastContentType(WxMaterialTypeEnum lastContentType) {
		this.lastContentType = lastContentType.name();
	}

	public static Integer getStatusUseful() {
		return STATUS_USEFUL;
	}

	public static Integer getStatusNouse() {
		return STATUS_NOUSE;
	}

	@Override
	public String toString() {
		return "WxUserOwner [id=" + id + ", wxUserId=" + wxUserId + ", ownerCode=" + ownerCode + ", ownerIcon="
				+ ownerIcon + ", ownerName=" + ownerName + ", ownerImessageCode=" + ownerImessageCode + ", lastContent="
				+ lastContent + ", lastContentType=" + lastContentType + ", lastContentTitle=" + lastContentTitle
				+ ", lastContentUrl=" + lastContentUrl + ", lastTime=" + lastTime + ", firstTime=" + firstTime
				+ ", direction=" + direction + ", sessionId=" + sessionId + ", wxViewTime=" + wxViewTime
				+ ", wxActiveNum=" + wxActiveNum + ", status=" + status + "]";
	}
}