package com.gtmc.gclub.chat.vo;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

/**
 * 微信客服和林肯后台用户对应表
 * */
public class TrUserEnterpriseVo {
    private BigDecimal id;
	
	private String userId;

    private String phone;

    private String wxUserId;

    private String iconsPhoto;

//    private Date createDate;
//    
//    private BigDecimal createBy;
//    
//    private Date updateDate;
//    
//    private BigDecimal updateBy;

    private String jmessageCode;
    
    private Integer jmessageStatus;
    
    private String wxJmessageCode;
    
    private String weixinName;
    
    private String dealerCode;
    
    private String dealerName;
    
    private BigDecimal dealerCdType;

	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getWxUserId() {
		return wxUserId;
	}

	public void setWxUserId(String wxUserId) {
		this.wxUserId = wxUserId;
	}

	public String getIconsPhoto() {
		return iconsPhoto;
	}

	public void setIconsPhoto(String iconsPhoto) {
		this.iconsPhoto = iconsPhoto;
	}

//	public Date getCreateDate() {
//		return createDate;
//	}
//
//	public void setCreateDate(Date createDate) {
//		this.createDate = createDate;
//	}
//
//	public BigDecimal getCreateBy() {
//		return createBy;
//	}
//
//	public void setCreateBy(BigDecimal createBy) {
//		this.createBy = createBy;
//	}
//
//	public Date getUpdateDate() {
//		return updateDate;
//	}
//
//	public void setUpdateDate(Date updateDate) {
//		this.updateDate = updateDate;
//	}
//
//	public BigDecimal getUpdateBy() {
//		return updateBy;
//	}
//
//	public void setUpdateBy(BigDecimal updateBy) {
//		this.updateBy = updateBy;
//	}

	public String getJmessageCode() {
		return jmessageCode;
	}

	public void setJmessageCode(String jmessageCode) {
		this.jmessageCode = jmessageCode;
	}

	public String getWeixinName() {
		return weixinName;
	}

	public void setWeixinName(String weixinName) {
		this.weixinName = weixinName;
	}

	public BigDecimal getDealerCdType() {
		return dealerCdType;
	}

	public void setDealerCdType(BigDecimal dealerCdType) {
		this.dealerCdType = dealerCdType;
	}

	public String getDealerCode() {
		return dealerCode;
	}

	public void setDealerCode(String dealerCode) {
		this.dealerCode = dealerCode;
	}

	public String getDealerName() {
		return dealerName;
	}

	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}

	public String getWxJmessageCode() {
		return wxJmessageCode;
	}

	public void setWxJmessageCode(String wxJmessageCode) {
		this.wxJmessageCode = wxJmessageCode;
	}

	public Integer getJmessageStatus() {
		return jmessageStatus;
	}

	public void setJmessageStatus(Integer jmessageStatus) {
		this.jmessageStatus = jmessageStatus;
	}

	@Override
	public String toString() {
		return "TrUserEnterpriseVo [id=" + id + ", userId=" + userId + ", phone=" + phone + ", wxUserId=" + wxUserId
				+ ", iconsPhoto=" + iconsPhoto + ", jmessageCode=" + jmessageCode + ", jmessageStatus=" + jmessageStatus
				+ ", wxJmessageCode=" + wxJmessageCode + ", weixinName=" + weixinName + ", dealerCode=" + dealerCode
				+ ", dealerName=" + dealerName + ", dealerCdType=" + dealerCdType + "]";
	}

}