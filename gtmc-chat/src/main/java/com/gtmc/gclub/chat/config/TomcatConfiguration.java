package com.gtmc.gclub.chat.config;

import org.apache.catalina.connector.Connector;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;



@Configuration
public class TomcatConfiguration {
	
	
	@Value("${jvmroute}")
	private String jvmRoute;
	
	@Value("${ajpport}")
	private int ajpport;
	

	@Bean
	public EmbeddedServletContainerFactory servletContainer() throws Exception{

		TomcatEmbeddedServletContainerFactory tomcat = new TomcatEmbeddedServletContainerFactory();
		Connector ajpConnector = new Connector("AJP/1.3");
		ajpConnector.setProtocol("AJP/1.3");
		ajpConnector.setPort(ajpport);
		ajpConnector.setProperty("jvmRoute", jvmRoute);
//		ajpConnector.setSecure(isAjpSecure());
//		ajpConnector.setAllowTrace(isAjpAllowTrace());
//		ajpConnector.setScheme(getAjpScheme());
		tomcat.addAdditionalTomcatConnectors(ajpConnector);

		return tomcat;
	}
	// ... Get/Set
}
