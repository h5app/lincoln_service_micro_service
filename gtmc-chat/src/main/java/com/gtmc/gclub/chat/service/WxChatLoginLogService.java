package com.gtmc.gclub.chat.service;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gtmc.gclub.chat.config.ChatConfigParam;
import com.gtmc.gclub.chat.dao.WxChatLoginLogMapper;
import com.gtmc.gclub.chat.model.WxChatLoginLog;

@Service
@Transactional
public class WxChatLoginLogService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private WxChatLoginLogMapper wxChatLoginLogMapper;
	
	public void insert(WxChatLoginLog entity) {
		// 把未下线的T下线
		loginOut(entity.getWxUserId());
		entity.setLoginDate(new Date());
		entity.setUpdateDate(new Date());
		wxChatLoginLogMapper.insert(entity);
	}
	
	public Date getLastDate(Integer wxUserId,Integer appType) {
		WxChatLoginLog entity = wxChatLoginLogMapper.selectLast(wxUserId,appType);
		return entity.getUpdateDate();
	}
	
	public void updateTime(Integer wxUserId,Integer appType) {
		wxChatLoginLogMapper.updateTime(wxUserId,appType);
	}
	
	public void loginOut(Integer wxUserId) {
		wxChatLoginLogMapper.loginOut(wxUserId);
	}
	
	public List<WxChatLoginLog> getAllTimeOutChat(Integer timeout){
		return wxChatLoginLogMapper.getAllTimeOutChat(timeout);
	}
}
