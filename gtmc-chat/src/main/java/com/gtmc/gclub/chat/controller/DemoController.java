//package com.gtmc.gclub.chat.controller;
//
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import com.gtmc.gclub.chat.component.MessagerSender;
//import com.gtmc.gclub.chat.constants.MessageConstant.MESSAGE_TYPE;
//import com.gtmc.gclub.chat.message.MessageProtocol;
//import com.gtmc.gclub.chat.model.TmPageMark;
//import com.gtmc.gclub.chat.model.TmUser;
//import com.gtmc.gclub.chat.model.TsDataCollectLog;
//import com.gtmc.gclub.chat.service.CarOwnerService;
//import com.gtmc.gclub.chat.service.DataCollectService;
//import com.gtmc.gclub.chat.service.PageMarkService;
//import com.gtmc.gclub.chat.service.UserService;
//
//import io.swagger.annotations.ApiOperation;
//
//@Controller
//@RequestMapping(value = "/api")
//public class DemoController extends BaseController {
//
//	private Logger log = LoggerFactory.getLogger(this.getClass());
//
//	@Autowired
//	private MessagerSender messagerSender;
//
//	@Autowired
//	private UserService UserService;
//	
//	
//	@Autowired
//	private PageMarkService pageMarkService;
//
//	@Autowired
//	private CarOwnerService carOwnerService;
//
//	@Autowired
//	private DataCollectService dataCollectService;
//
//	@RequestMapping(value = "/amsg",method=RequestMethod.POST)
//	public void mqAct() {
//		MessageProtocol msg = new MessageProtocol();
//		msg.setContent("活动数据111");
//		msg.setMessageType(MESSAGE_TYPE.ACTIVITY);
//		msg.setMsgId(new Date().getTime());
//		msg.setTimestamp(new Date().getTime());
//		messagerSender.sendActivity2ExchangesGClub(msg);
//		// messagerSender.sendActivity(msg);
//	}
//
//	@RequestMapping(value = "/umsg",method=RequestMethod.POST)
//	public void mqUser() {
//		MessageProtocol msg = new MessageProtocol();
//		msg.setContent("用户数据111");
//		msg.setMessageType(MESSAGE_TYPE.ACTIVITY);
//		msg.setMsgId(new Date().getTime());
//		msg.setTimestamp(new Date().getTime());
//		messagerSender.sendTest(msg);
//		// messagerSender.sendActivity(msg);
//	}
//
//	@RequestMapping(value = "/add",method=RequestMethod.POST)
//	public void testAdd() {
//		TsDataCollectLog data = new TsDataCollectLog();
//		data.setActionClassName("ceshi");
//		dataCollectService.save(data);
//	}
//
//	@RequestMapping(value = "/addlist",method=RequestMethod.POST)
//	public void testAddList() {
//		TsDataCollectLog data = new TsDataCollectLog();
//		data.setActionClassName("ceshi");
//		TsDataCollectLog data2 = new TsDataCollectLog();
//		data2.setActionClassName("ceshi2");
//		TsDataCollectLog data3 = new TsDataCollectLog();
//		data3.setActionClassName("ceshi2");
//		List<TsDataCollectLog> l = new ArrayList<TsDataCollectLog>();
//		l.add(data3);
//		l.add(data2);
//		l.add(data);
//		log.info("新增了 " + dataCollectService.saveAll(l));
//	}
//
//	@ApiOperation(value = "查询", notes = "查询一个列表")
//	@RequestMapping(value = "/queryList",method=RequestMethod.GET)
//	@ResponseBody
//	public Map<String, Object> queryList() {
//		TsDataCollectLog data = new TsDataCollectLog();
//		data.setActionClassName("ceshi");
//		Map<String, Object> returnMap = new HashMap<String, Object>();
//		returnMap.put("result", 1);
//		returnMap.put("data", dataCollectService.getList(data));
//		return returnMap;
//
//	}
//
//	@RequestMapping(value = "/data/{id}", method = RequestMethod.PUT)
//	@ResponseBody
//	public Map<String, Object> updateData(@PathVariable(value = "id") String id, @RequestBody TsDataCollectLog log) {
//		log.setId(Integer.parseInt(id));
//		Map<String, Object> returnMap = new HashMap<String, Object>();
//		returnMap.put("result", 1);
//		returnMap.put("data", dataCollectService.updateDataNullUpdate(log));
//		return returnMap;
//
//	}
//
//	@RequestMapping(value = "/transationTest",method=RequestMethod.GET)
//	@ResponseBody
//	public Map<String, Object> insertTransationTest() {
//		TmUser user = new TmUser();
//		user.setUserName("benjamin");
//
//		TsDataCollectLog data = new TsDataCollectLog();
//		data.setActionClassName("benjamin123");
//		dataCollectService.saveTransaction(data, user);
//
//		Map<String, Object> returnMap = new HashMap<String, Object>();
//		returnMap.put("result", 1);
//		return returnMap;
//	}
//
//	@RequestMapping(value = "/queryOwners",method=RequestMethod.GET)
//	@ResponseBody
//	public Map<String, Object> queryOwners() {
//
//		Map<String, Object> returnMap = new HashMap<String, Object>();
//		returnMap.put("result", 1);
//		returnMap.put("data", carOwnerService.getOwnerList());
//		return returnMap;
//	}
//	
//	@RequestMapping(value="/querymark",method=RequestMethod.GET)
//	@ResponseBody
//	public List<TmPageMark> queryMarkList(){
//		return pageMarkService.getPageMarkList();
//	}
//
//	
//
//}
