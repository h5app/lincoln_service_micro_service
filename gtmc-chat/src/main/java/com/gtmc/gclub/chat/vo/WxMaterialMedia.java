package com.gtmc.gclub.chat.vo;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.gtmc.gclub.common.WxMaterialTypeEnum;

/**
 * @author kaihua
 *
 * 图片、语音、视频素材参数实体
 */
public class WxMaterialMedia{
	private String media_id;
	private String name;
//	private String tagName;// 视频专用
	private Date update_time;
	private String url;
//	private String down_url;// 视频专用
	private String title;
	private String description;
	private WxMaterialTypeEnum type;
	public String getMedia_id() {
		return media_id;
	}
	public void setMedia_id(String media_id) {
		this.media_id = media_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getUpdate_time() {
		return update_time;
	}
	public void setUpdate_time(Date update_time) {
		this.update_time = update_time;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public WxMaterialTypeEnum getType() {
		return type;
	}
	public void setType(WxMaterialTypeEnum type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "WxMaterialMedia [media_id=" + media_id + ", name=" + name + ", update_time=" + update_time + ", url="
				+ url + ", title=" + title + ", description=" + description + ", type=" + type + "]";
	}
	public static Map<String,Object> getH5JsonMap(WxMaterialMedia entity,WxMaterialTypeEnum type){
		Map<String,Object> root = new HashMap<String,Object>();
		root.put("tagName", entity.getName());
		root.put("media_id", entity.getMedia_id());
		Map<String,String> entityDetail = new HashMap<String,String>();
		entityDetail.put("title", entity.getTitle());
		entityDetail.put("description", entity.getDescription());
		entityDetail.put("down_url", entity.getUrl());
		entityDetail.put("type", type.name());// 这里从永久素材列表是拿不到素材类型的,只能从调用出获取
		root.put("content", entityDetail);
		return root;
	}
}