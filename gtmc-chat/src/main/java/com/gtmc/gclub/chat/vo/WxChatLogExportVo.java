package com.gtmc.gclub.chat.vo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.gtmc.gclub.common.WxMaterialTypeEnum;

public class WxChatLogExportVo {
	/**
	 * 客服编号
	 */
	private Integer wxUserId;
	private String wxUserName;
	
	private String dealerCode;
	private String dealerName;

	/**
	 * app用户账号
	 */
	private String ownerCode;
	private String ownerName;
	
	private String ownerModel;
	private String ownerModelName;

	/**
	 * 聊天内容
	 */
	private String content;
	private String contentType;

	/**
	 * 聊天时间
	 */
	private Date crtTime;

	/**
	 * 聊天方向：0,微信客服发起 1,app用户发起
	 */
	private String direction;

	/**
	 * 会话编号
	 */
	private Integer sessionId;

	private String title;
	
	private String url;

	/**
	 * 获取客服编号
	 *
	 * @return wx_user_id - 客服编号
	 */
	public Integer getWxUserId() {
		return wxUserId;
	}

	/**
	 * 设置客服编号
	 *
	 * @param wxUserId
	 *            客服编号
	 */
	public void setWxUserId(Integer wxUserId) {
		this.wxUserId = wxUserId;
	}

	/**
	 * 获取app用户账号
	 *
	 * @return owner_code - app用户账号
	 */
	public String getOwnerCode() {
		return ownerCode;
	}

	/**
	 * 设置app用户账号
	 *
	 * @param ownerCode
	 *            app用户账号
	 */
	public void setOwnerCode(String ownerCode) {
		this.ownerCode = ownerCode;
	}

	/**
	 * 获取聊天内容
	 *
	 * @return content - 聊天内容
	 */
	public String getContent() {
		return content;
	}

	/**
	 * 设置聊天内容
	 *
	 * @param content
	 *            聊天内容
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * 获取聊天时间
	 *
	 * @return crt_time - 聊天时间
	 */
	public Date getCrtTime() {
		return crtTime;
	}

	/**
	 * 设置聊天时间
	 *
	 * @param crtTime
	 *            聊天时间
	 */
	public void setCrtTime(Date crtTime) {
		this.crtTime = crtTime;
	}

	/**
	 * 获取聊天方向：0,微信客服发起 1,app用户发起
	 *
	 * @return direction - 聊天方向：0,微信客服发起 1,app用户发起
	 */
	public String getDirection() {
		return direction;
	}

	/**
	 * 设置聊天方向：0,微信客服发起 1,app用户发起
	 *
	 * @param direction
	 *            聊天方向：0,微信客服发起 1,app用户发起
	 */
	public void setDirection(String direction) {
		this.direction = direction;
	}

	/**
	 * 获取会话编号
	 *
	 * @return session_id - 会话编号
	 */
	public Integer getSessionId() {
		return sessionId;
	}

	/**
	 * 设置会话编号
	 *
	 * @param sessionId
	 *            会话编号
	 */
	public void setSessionId(Integer sessionId) {
		this.sessionId = sessionId;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public void setContentType(WxMaterialTypeEnum contentType) {
		this.contentType = contentType.name();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getWxUserName() {
		return wxUserName;
	}

	public void setWxUserName(String wxUserName) {
		this.wxUserName = wxUserName;
	}

	public String getDealerCode() {
		return dealerCode;
	}

	public void setDealerCode(String dealerCode) {
		this.dealerCode = dealerCode;
	}

	public String getDealerName() {
		return dealerName;
	}

	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getOwnerModel() {
		return ownerModel;
	}

	public void setOwnerModel(String ownerModel) {
		this.ownerModel = ownerModel;
	}

	public String getOwnerModelName() {
		return ownerModelName;
	}

	public void setOwnerModelName(String ownerModelName) {
		this.ownerModelName = ownerModelName;
	}

	@Override
	public String toString() {
		return "WxChatLogExportDto [wxUserId=" + wxUserId + ", wxUserName=" + wxUserName + ", dealerCode=" + dealerCode
				+ ", dealerName=" + dealerName + ", ownerCode=" + ownerCode + ", ownerName=" + ownerName
				+ ", ownerModel=" + ownerModel + ", ownerModelName=" + ownerModelName + ", content=" + content
				+ ", contentType=" + contentType + ", crtTime=" + crtTime + ", direction=" + direction + ", sessionId="
				+ sessionId + ", title=" + title + ", url=" + url + "]";
	}

}