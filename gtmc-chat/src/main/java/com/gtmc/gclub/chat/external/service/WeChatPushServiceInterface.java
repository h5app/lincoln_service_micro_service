/*
* Copyright 2017 Yonyou Auto Information Technology（Shanghai） Co., Ltd. All Rights Reserved.
*
* This software is published under the terms of the YONYOU Software
* License version 1.0, a copy of which has been included with this
* distribution in the LICENSE.txt file.
*
* @Project Name : marketing-service-wechatpush
*
* @File name : ChatServiceInterface.java
*
* @Author : Administrator
*
* @Date : 2017年12月25日
*
----------------------------------------------------------------------------------
*     Date       Who       Version     Comments
* 1. 2017年12月25日    Administrator    1.0
*
*
*
*
----------------------------------------------------------------------------------
*/
	
package com.gtmc.gclub.chat.external.service;

import java.util.Map;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.gtmc.gclub.chat.dto.WxGzMessageDto;

/**
*
* @author Administrator
* TODO description
* @date 2017年12月25日
*/
@FeignClient(name = "SERVICE-WECHAT-PUSH")
public interface WeChatPushServiceInterface {

    @RequestMapping(value = "/wechat-push/api/v1/replyUser", method = RequestMethod.POST,consumes = "application/json;UTF-8")
    public Map<String,Object> sendWxgzMsg(@RequestBody WxGzMessageDto wxgzMsg);
    
    @RequestMapping(value = "/wechat-push/api/v1/disconnect", method = RequestMethod.GET,consumes = "application/json;UTF-8")
    public Map<String,Object> disconnect(@RequestParam("openId") String openId);
    
    @RequestMapping(value = "/genToken", method = RequestMethod.GET,consumes = "application/json;UTF-8")
    public String getWxPublishToken();
}
