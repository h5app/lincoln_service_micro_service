package com.gtmc.gclub.chat.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.gtmc.gclub.chat.annotation.MySQLDb;
import com.gtmc.gclub.chat.annotation.OracleDb;
import com.gtmc.gclub.chat.dto.WxChatLogExportDto;
import com.gtmc.gclub.chat.model.TrUserEnterprise;
import com.gtmc.gclub.chat.vo.WxChatLogExportVo;

import tk.mybatis.mapper.common.Mapper;

//@OracleDb
@MySQLDb
public interface TrUserEnterpriseMapper extends Mapper<TrUserEnterprise> {

	int insertUser(TrUserEnterprise wxUser);

	void deleteAll();

	void insertByBatchId(List<TrUserEnterprise> yList);

	void insertByBatch(List<TrUserEnterprise> xList);
	
	List<TrUserEnterprise> getWxUserList(Map<String, Object> map);
	
	/**
	 * 根据林肯系统userId获取客服信息 
	 * */
	public TrUserEnterprise getChatUserByLKUser(@Param("userId") String userId);
	
	/**
	 * 根据林肯客服IDS获取客服列表
	 * @param userId
	 * @return
	 */
	public List<TrUserEnterprise> getChatUsers(@Param("userIds") List<String> userId);
	
	/**
	 * 根据客服系统IDS获取客服列表
	 * @param ids
	 * @return
	 */
	public List<TrUserEnterprise> getChatUsersByPrimaryKeys(@Param("ids") List<String> ids);

	/**
	 * @param chatId 客服下线
	 */
	public void loginOut(@Param("id")Integer chatId);

}