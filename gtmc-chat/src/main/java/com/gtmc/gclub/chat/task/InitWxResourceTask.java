package com.gtmc.gclub.chat.task;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.gtmc.gclub.chat.service.WxService;
import com.gtmc.gclub.chat.vo.WxMaterialBaseVo;
import com.gtmc.gclub.common.WxMaterialTypeEnum;

/**
 * @author kaihu
 * 定时拉取媒体资源，存入数据库
 */
public class InitWxResourceTask {
	
	@Autowired
	private WxService wxService;
	
	public void syncMeterialFromWx() throws Exception {
		for(WxMaterialTypeEnum type : WxMaterialTypeEnum.values()) {
			List<WxMaterialBaseVo> allMaterial = wxService.getAllMaterial(type);
		}
	}
}
