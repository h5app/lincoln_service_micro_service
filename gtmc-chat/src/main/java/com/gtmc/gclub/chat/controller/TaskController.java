package com.gtmc.gclub.chat.controller;

import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gtmc.gclub.chat.bean.Result;
import com.gtmc.gclub.chat.constants.CommonProperties;
import com.gtmc.gclub.chat.dao.TrUserEnterpriseMapper;
import com.gtmc.gclub.chat.dao.WxUserOwnerMapper;
import com.gtmc.gclub.chat.model.TrUserEnterprise;
import com.gtmc.gclub.chat.model.WxChatLoginLog;
import com.gtmc.gclub.chat.model.WxMaterialFile;
import com.gtmc.gclub.chat.model.WxUserOwner;
import com.gtmc.gclub.chat.service.WxChatLoginLogService;
import com.gtmc.gclub.chat.service.WxMaterialFileService;
import com.gtmc.gclub.chat.service.WxService;
import com.gtmc.gclub.chat.task.GetAllMaterialFromWxGZTask;
import com.gtmc.gclub.common.WxMaterialTypeEnum;
import com.infoservice.filestore.FileStoreException;

@Controller
@RequestMapping("/chat/task")
public class TaskController extends BaseController {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private WxService wxService;
	
	@Autowired
	private WxMaterialFileService wxMaterialFileService;
	
	@Autowired
	private WxUserOwnerMapper wxUserOwnerMapper;
	
	@Autowired
	private WxController wxController ;
	@Autowired
	private WxChatLoginLogService wxChatLoginLogService ;
	@Autowired
	private TrUserEnterpriseMapper trUserEnterpriseMapper ;
	
	@Autowired
	private CommonProperties commonProperties;
	
	@RequestMapping(value = "/wx_user_task", method = RequestMethod.GET)
	@ResponseBody
	public Result<String> wxUserTask() {
		logger.info("定时任务：wxUserTask");
		// 1.获取部门列表 parentId=23
		// 2.获取部门成员信息
		// 3.获取标签列表
		// 4.获取数据库信息
		// 5.遍历部门列表，完善数据
		// 6.清空表，插入数据
		// DepartmentList delist=
		wxService.updateChatUser(wxService.getDepartmentList());
		return new Result<String>(1, null, "操作成功");
	}
	
	@RequestMapping(value = "/chat_connect_Task", method = RequestMethod.GET)
	@ResponseBody
	public Result<String> chatConnectTask() throws Exception {
		logger.info("定时任务：chatConnectTask启动");
//		wxUserOwnerMapper.updateStatusByTimeOut(ChatConfigParam.CHAT_CONNECT_TIMEOUT);
		// 首先查询出所有超时的聊天链接
		List<WxUserOwner> selectChatUserByTimeOut = wxUserOwnerMapper.selectChatUserByTimeOut(commonProperties.chatConnectTimeOut);
		logger.info("查询出超时聊天链接："+selectChatUserByTimeOut);
		for(WxUserOwner discon : selectChatUserByTimeOut) {
			TrUserEnterprise query = new TrUserEnterprise();
			query.setId(new BigDecimal(discon.getWxUserId()));
			TrUserEnterprise chatUser = trUserEnterpriseMapper.selectOne(query);
			wxController.deleteChatOwner(Integer.valueOf(chatUser.getUserId()), discon.getOwnerCode());
		}
		// 然后找出登录超时的客服
		List<WxChatLoginLog> allTimeOutChat = wxChatLoginLogService.getAllTimeOutChat(commonProperties.chatLoginTimeOut);
		logger.info("查询出超时客服："+allTimeOutChat);
		for(WxChatLoginLog timeOutChat : allTimeOutChat) {
			wxController.chatLoginOut(timeOutChat.getWxUserId());
		}
		logger.info("定时任务：chatConnectTask操作完成");
		return new Result<String>(1, null, "操作成功");
	}
	
	@RequestMapping(value = "/sync_wx_permanent_material", method = RequestMethod.GET)
	@ResponseBody
	public Result<String> syncPermanentMeterialTask(@RequestParam(value = "type", required = false) String type) {
		logger.info("定时任务：同步微信永久素材syncPermanentMeterialTask");
		new GetAllMaterialFromWxGZTask(wxService,type==null ? null : WxMaterialTypeEnum.valueOf(type)).start();
		return new Result<String>(1, null, "操作成功");
	}
	@RequestMapping(value = "/delete_all_permanent_material", method = RequestMethod.GET)
	@ResponseBody
	public Result<String> deleteAllPermanentMeterialTask() throws FileStoreException {
		logger.info("测试删除syncPermanentMeterialTask");
		List<WxMaterialFile> all = wxMaterialFileService.getAll();
		for(WxMaterialFile wxMaterialFile : all) {
			wxMaterialFileService.deleteMaterial(wxMaterialFile);
		}
		return new Result<String>(1, null, "操作成功");
	}
}
