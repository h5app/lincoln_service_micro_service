package com.gtmc.gclub.chat.vo;

public class ClassifyAllContentVo {
	private Integer id;
	private Integer pid;
	private String ccontent;
	private Integer fid;
	private String mediaId;
	private String type;
	private String wxUrl;
	private String localUrl;
	private String title;
	private String description;
	private String contentSourceUrl;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getPid() {
		return pid;
	}
	public void setPid(Integer pid) {
		this.pid = pid;
	}
	public String getCcontent() {
		return ccontent;
	}
	public void setCcontent(String ccontent) {
		this.ccontent = ccontent;
	}
	public Integer getFid() {
		return fid;
	}
	public void setFid(Integer fid) {
		this.fid = fid;
	}
	public String getMediaId() {
		return mediaId;
	}
	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getWxUrl() {
		return wxUrl;
	}
	public void setWxUrl(String wxUrl) {
		this.wxUrl = wxUrl;
	}
	public String getLocalUrl() {
		return localUrl;
	}
	public void setLocalUrl(String localUrl) {
		this.localUrl = localUrl;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getContentSourceUrl() {
		return contentSourceUrl;
	}
	public void setContentSourceUrl(String contentSourceUrl) {
		this.contentSourceUrl = contentSourceUrl;
	}
	@Override
	public String toString() {
		return "ClassifyAllContentVo [id=" + id + ", pid=" + pid + ", content=" + ccontent + ", fid=" + fid
				+ ", mediaId=" + mediaId + ", type=" + type + ", wxUrl=" + wxUrl + ", localUrl=" + localUrl + ", title="
				+ title + ", description=" + description + ", contentSourceUrl=" + contentSourceUrl + "]";
	}
	
}
