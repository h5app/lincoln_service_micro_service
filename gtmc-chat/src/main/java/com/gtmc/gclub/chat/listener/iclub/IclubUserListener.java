package com.gtmc.gclub.chat.listener.iclub;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import com.gtmc.gclub.chat.constants.MessageConstant;
import com.gtmc.gclub.chat.message.MessageProtocol;

/**
 * 用户消息的监听
 * 
 * @author BENJAMIN
 */
@Component
public class IclubUserListener {
	private static Logger logger = LoggerFactory.getLogger(IclubUserListener.class);
	
	//@RabbitListener(queues=MessageConstant.ICLUB_USER_QUEUE,containerFactory="rabbitListenerContainerFactory")
	public void receiveQueueGClub(@Payload MessageProtocol protocol){
		logger.info("start to listen");
        logger.info("@@@@ ICLUB 的 USER 队列 接收到 content is "+protocol.toString());
        logger.info("end to listen " );
	}
}
