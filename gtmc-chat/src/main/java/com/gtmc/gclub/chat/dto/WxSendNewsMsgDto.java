package com.gtmc.gclub.chat.dto;

/**
 * @author kaihu
 * 发送微信图文消息时使用的参数，用于规范图文消息发送接口的参数
 * 当前阶段未完成，后续优化使用
 */
public class WxSendNewsMsgDto extends WxSendMsgBaseDto {
	private String title;
	private String description;
	private String url;
	private String picurl;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getPicurl() {
		return picurl;
	}
	public void setPicurl(String picurl) {
		this.picurl = picurl;
	}
	@Override
	public String toString() {
		return "WxSendNewsMsgDto [title=" + title + ", description=" + description + ", url=" + url + ", picurl="
				+ picurl + "]";
	}
}
