package com.gtmc.gclub.chat.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.gtmc.gclub.chat.annotation.MySQLDb;
import com.gtmc.gclub.chat.dto.ChatClassifyQueryDto;
import com.gtmc.gclub.chat.model.TmClassifyTag;
import com.gtmc.gclub.chat.vo.ClassifyContentPageVo;
import com.gtmc.gclub.chat.vo.ClassifyTagFirstVo;
import com.gtmc.gclub.chat.vo.ClassifyTagSecondVo;
import com.gtmc.gclub.chat.vo.TmClassifyContentVo;

import tk.mybatis.mapper.common.Mapper;

@MySQLDb
public interface ExTmClassifyTagMapper extends Mapper<TmClassifyTag> {

	int insertBase(TmClassifyTag tm);

	int getTagCodeCount(TmClassifyTag t);

	List<Map<String, Object>> getTagCodeId(TmClassifyTag t);

	List<ClassifyTagFirstVo> getAppChatTagFirstExport(ChatClassifyQueryDto dealerCode);

	List<ClassifyTagSecondVo> getAppChatTagSecondExport(ChatClassifyQueryDto dealerCode);

	List<TmClassifyContentVo> getAppChatTag(String dealerCode);

	List<ClassifyContentPageVo> getAppChatTagFirst(com.gtmc.gclub.chat.dto.ChatClassifyQueryDto dealerCode);

	List<ClassifyContentPageVo> getAppChatTagSecond(com.gtmc.gclub.chat.dto.ChatClassifyQueryDto dealerCode);

	List<TmClassifyTag> getAllClassifyTagByDealerCode(@Param("dealerCode")String dealerCode);
}