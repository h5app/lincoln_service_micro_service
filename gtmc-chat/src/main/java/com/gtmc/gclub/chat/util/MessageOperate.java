package com.gtmc.gclub.chat.util;

public class MessageOperate {
	/**
	 * 把图文消息转换为标题加链接  
	 * 后续可能优化，所有参数丢给前台，所有样式效果交给前台处理。
	 * @param title
	 * @param url
	 * @return
	 */
	public static String getNewsContent(String title,String url) {
		return "<a href='"+url+"'  target='_blank'>"+title+"</a>";
	}
}
