package com.gtmc.gclub.chat.dao;

import java.util.List;

import com.gtmc.gclub.chat.annotation.MySQLDb;
import com.gtmc.gclub.chat.dto.ChatDialogueDto;
import com.gtmc.gclub.chat.model.TmChatDialogue;

import tk.mybatis.mapper.common.Mapper;

@MySQLDb
public interface ExTmChatDialogueMapper extends Mapper<TmChatDialogue> {
	int insertBaseAll(TmChatDialogue tm);

	int insertBase(TmChatDialogue tm);

	int insertBatchs(ChatDialogueDto dto);

	int updateDialogueTimes(Integer id);

	List<com.gtmc.gclub.chat.vo.TmChatDialoguePageVo> getAppChatDialogue(String dealerCode);

	List<com.gtmc.gclub.chat.vo.TmChatDialoguePageVo> getAppChatDialogueExport(String dealerCode);

}
