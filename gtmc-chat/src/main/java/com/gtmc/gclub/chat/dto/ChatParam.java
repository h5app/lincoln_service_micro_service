package com.gtmc.gclub.chat.dto;

import java.util.Date;

public class ChatParam {
	private Integer logId;// 会话ID
	private String dealerCode;
	private String dealerName;
	private String modelCode;
	private String modelName;
	private String chatId;// 这里的客服ID,统一使用林肯用户的ID
	private Integer ownerCode;// 用户ID,统一使用潜客的ID
	// 客户姓名
	private String userName;
	// 客户手机号
	private String phone;
	// 客服姓名
	private String chatName;
	// 客服手机号
	private String chatPhone;
	
	private String content;
	private String contentType;
	
	private Integer direction;
	private Date searchStartDate;
	private Date searchEndDate;
	private Date sendDate;
	
	private Integer status;// 会话状态
	private Integer limit;
	private Integer offset;
	public Integer getLogId() {
		return logId;
	}
	public void setLogId(Integer logId) {
		this.logId = logId;
	}
	public String getDealerCode() {
		return dealerCode;
	}
	public void setDealerCode(String dealerCode) {
		this.dealerCode = dealerCode;
	}
	public String getModelCode() {
		return modelCode;
	}
	public void setModelCode(String modelCode) {
		this.modelCode = modelCode;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Integer getLimit() {
		return limit;
	}
	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	public Integer getOffset() {
		return offset;
	}
	public void setOffset(Integer offset) {
		this.offset = offset;
	}
	public Date getSearchStartDate() {
		return searchStartDate;
	}
	public void setSearchStartDate(Date searchStartDate) {
		this.searchStartDate = searchStartDate;
	}
	public Date getSearchEndDate() {
		return searchEndDate;
	}
	public void setSearchEndDate(Date searchEndDate) {
		this.searchEndDate = searchEndDate;
	}
	public String getDealerName() {
		return dealerName;
	}
	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}
	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	public Integer getOwnerCode() {
		return ownerCode;
	}
	public void setOwnerCode(Integer ownerCode) {
		this.ownerCode = ownerCode;
	}
	public String getChatId() {
		return chatId;
	}
	public void setChatId(String chatId) {
		this.chatId = chatId;
	}
	public String getChatName() {
		return chatName;
	}
	public void setChatName(String chatName) {
		this.chatName = chatName;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public Integer getDirection() {
		return direction;
	}
	public void setDirection(Integer direction) {
		this.direction = direction;
	}
	public String getChatPhone() {
		return chatPhone;
	}
	public void setChatPhone(String chatPhone) {
		this.chatPhone = chatPhone;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Date getSendDate() {
		return sendDate;
	}
	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}
	@Override
	public String toString() {
		return "ChatParam [logId=" + logId + ", dealerCode=" + dealerCode + ", dealerName=" + dealerName
				+ ", modelCode=" + modelCode + ", modelName=" + modelName + ", chatId=" + chatId + ", ownerCode="
				+ ownerCode + ", userName=" + userName + ", phone=" + phone + ", chatName=" + chatName + ", chatPhone="
				+ chatPhone + ", content=" + content + ", contentType=" + contentType + ", direction=" + direction
				+ ", searchStartDate=" + searchStartDate + ", searchEndDate=" + searchEndDate + ", sendDate=" + sendDate
				+ ", status=" + status + ", limit=" + limit + ", offset=" + offset + "]";
	}
	
}
