package com.gtmc.gclub.chat.config;

public final class ChatConfigParam {
	/**
	 * 单个客服同时对应的最大人数
	 */
	public static final Integer CHAT_MAX_CNUM = 5; 
	/**
	 * 客服等待队列最大深度
	 */
	public static final Integer CHAT_WAIT_QUEUE_MAX = 2;
	/**
	 * 无聊天情况下，聊天超时自动断开链接时间,5分钟,单位秒
	 */
	public static final Integer CHAT_CONNECT_TIMEOUT = 50*60;
	
	/**
	 * 无操作情况下，客服超时自动断开链接时间,15分钟,单位秒
	 */
	public static final Integer CHAT_LOGIN_TIMEOUT = 150*60;
	
}
