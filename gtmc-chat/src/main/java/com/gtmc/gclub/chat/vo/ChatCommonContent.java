package com.gtmc.gclub.chat.vo;

import java.util.List;

/**
 * @author kaihua
 *
 * 递归常见内容树用
 */
public class ChatCommonContent {
	private String id;
	private ClassifyAllContentVo content;
	private List<ChatCommonContent> childs;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<ChatCommonContent> getChilds() {
		return childs;
	}
	public void setChilds(List<ChatCommonContent> childs) {
		this.childs = childs;
	}
	public ClassifyAllContentVo getContent() {
		return content;
	}
	public void setContent(ClassifyAllContentVo content) {
		this.content = content;
	}
	@Override
	public String toString() {
		return "ChatCommonContent [id=" + id + ", content=" + content + ", childs=" + childs + "]";
	}
}
