package com.gtmc.gclub.chat.modelutil;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gtmc.gclub.chat.model.WxMaterialFile;
import com.gtmc.gclub.chat.service.WxService;
import com.gtmc.gclub.chat.vo.WxMaterialDetailH5;
import com.gtmc.gclub.common.WxMaterialTypeEnum;

public class WxMaterialModelToH5 {
	private static Logger logger = LoggerFactory.getLogger(WxMaterialModelToH5.class);
	// 这个方法有问题，待修正后，在投入使用
	public static WxMaterialDetailH5 modelToH5(WxMaterialFile model) throws IllegalAccessException, InvocationTargetException {
		WxMaterialDetailH5 h5 = new WxMaterialDetailH5();
		logger.debug("拷贝开始model:");
		BeanUtils.copyProperties(h5, model);
		logger.debug("拷贝完成：");
		h5.setTagName(model.getTitle());
		h5.setDown_url(model.getLocalUrl());
		if(!(StringUtils.equals(model.getType(), WxMaterialTypeEnum.news.name()) 
				|| StringUtils.equals(model.getType(), WxMaterialTypeEnum.mpnews.name())) ) {
			h5.setUrl(model.getWxUrl());
		}
		logger.debug("属性整合完成："+h5);
		return h5;
	}
	public static Map<String,Object> getWxMaterialMediaForH5(WxMaterialFile model) {
		Map<String,Object> root = new HashMap<String,Object>();
		root.put("tagName", model.getTitle()==null ? "" : model.getTitle());
		root.put("media_id", model.getMediaId());
		Map<String,String> entityDetail = new HashMap<String,String>();
		entityDetail.put("title", model.getTitle());
		entityDetail.put("description", model.getDescription());
		entityDetail.put("down_url", model.getLocalUrl());
		entityDetail.put("type", model.getType());// 这里从永久素材列表是拿不到素材类型的,只能从调用出获取
		root.put("content", entityDetail);
		return root;
	}
	public static Map<String,Object> getWxMaterialNewsForH5(WxMaterialFile model) {
		Map<String,Object> root = new HashMap<String,Object>();
		root.put("tagName", model.getTitle());
		root.put("media_id", model.getMediaId());
		Map<String,String> entityDetail = new HashMap<String,String>();
		entityDetail.put("title", model.getTitle());
		entityDetail.put("thumb_media_id", model.getThumb_media_id());
		entityDetail.put("show_cover_pic", String.valueOf(model.getShow_cover_pic()==null ? null : model.getShow_cover_pic()));
		entityDetail.put("author", model.getAuthor());
		entityDetail.put("digest", model.getDigest());
		entityDetail.put("content", model.getContent());
		entityDetail.put("url", model.getWxUrl());
		entityDetail.put("content_source_url", model.getContent_source_url());
		entityDetail.put("type", model.getType());
		root.put("content", entityDetail);
		return root;
	}
}
