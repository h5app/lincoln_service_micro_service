package com.gtmc.gclub.chat.util;

public class JmessageUserUtil {
	public static final String JMESSAGE_DEFAULT_PASS = "123456" ;// 通过极光发送给客服的极光账号
	
	/**
	 * 获取PC端极光账号
	 * @param dealerCode
	 * @param userId
	 * @return
	 */
	public static String getPcJmessageCode(String userId) {
		return "JMChat_"+userId+"_PC";
	}
	/**
	 * 获取企业号端极光账号
	 * @param dealerCode
	 * @param userId
	 * @return
	 */
	public static String getQyJmessageCode(String userId) {
		return "JMChat_"+userId+"_QYH";
	}
}
