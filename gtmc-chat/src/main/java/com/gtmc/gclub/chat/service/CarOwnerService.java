package com.gtmc.gclub.chat.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gtmc.gclub.chat.dao.TtAdviserMapper;
import com.gtmc.gclub.chat.model.TtAdviser;

@Service
public class CarOwnerService {

	@Autowired
	private TtAdviserMapper TtAdviserMapper;

	public List<TtAdviser> getOwnerList() {
		return TtAdviserMapper.selectAll();
	}

	public PageInfo<TtAdviser> pageQuery(int pageCurrent, int pageSize) {
		PageHelper.startPage(pageCurrent, pageSize);
		List<TtAdviser> logList = TtAdviserMapper.selectAll();
		PageInfo<TtAdviser> pageInfo = new PageInfo<TtAdviser>(logList);
		return pageInfo;

	}
}
