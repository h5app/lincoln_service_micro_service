package com.gtmc.gclub.chat.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gtmc.gclub.chat.dao.WxMaterialFileMapper;
import com.gtmc.gclub.chat.model.WxMaterialFile;
import com.infoservice.filestore.FileStoreException;

@Service
@Transactional
public class WxMaterialFileService {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private FileService fileService;
	
	@Autowired
	private WxMaterialFileMapper wxMaterialFileMapper;
	
	public List<WxMaterialFile> getAll(WxMaterialFile query){
		return wxMaterialFileMapper.select(query);
	}
	public List<WxMaterialFile> getAll(){
		return wxMaterialFileMapper.selectAll();
	}
	public List<WxMaterialFile> getGzUseFulAll(){
		WxMaterialFile query = new WxMaterialFile();
		query.setAppid(WxMaterialFile.APPID_GZ);
		query.setStatus(WxMaterialFile.STATUS_USEFUL);
		return getAll(query);
	}
	
	public void insert(WxMaterialFile record) {
		wxMaterialFileMapper.insert(record);
	}
	
	public void deleteMaterial(WxMaterialFile entity) throws FileStoreException {
		logger.debug("deleteMaterial删除素材开始，更新数据库素材状态");
		entity.setStatus(WxMaterialFile.STATUS_NOUSE);
		wxMaterialFileMapper.updateByPrimaryKeySelective(entity);
		logger.debug("deleteMaterial删除素材开始，更新数据库素材状态完成");
//		wxMaterialFileMapper.delete(entity);
		logger.debug("删除服务器中的素材文件");
		fileService.deleteFileForLocal(entity.getLocalUrl());
		logger.debug("删除完成");
	}

}
