package com.gtmc.gclub.chat.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "WX_TOKEN")
public class WxToken {
	/**
	 * 主键
	 */
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;

	/**
	 * access_token凭证
	 */
	@Column(name = "access_token")
	private String accessToken;

	/**
	 * 凭证有效时间，单位：秒
	 */
	@Column(name = "expires_in")
	private Integer expiresIn;

	/**
	 * 创建时间距离当前时间毫秒数
	 */
	@Column(name = "start_time")
	private Long startTime;

	/**
	 * 创建时间
	 */
	@Column(name = "create_date")
	private Date createDate;

	/**
	 * 获取主键
	 *
	 * @return id - 主键
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 设置主键
	 *
	 * @param id
	 *            主键
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 获取access_token凭证
	 *
	 * @return access_token - access_token凭证
	 */
	public String getAccessToken() {
		return accessToken;
	}

	/**
	 * 设置access_token凭证
	 *
	 * @param accessToken
	 *            access_token凭证
	 */
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	/**
	 * 获取凭证有效时间，单位：秒
	 *
	 * @return expires_in - 凭证有效时间，单位：秒
	 */
	public Integer getExpiresIn() {
		return expiresIn;
	}

	/**
	 * 设置凭证有效时间，单位：秒
	 *
	 * @param expiresIn
	 *            凭证有效时间，单位：秒
	 */
	public void setExpiresIn(Integer expiresIn) {
		this.expiresIn = expiresIn;
	}

	/**
	 * 获取创建时间距离当前时间毫秒数
	 *
	 * @return start_time - 创建时间距离当前时间毫秒数
	 */
	public Long getStartTime() {
		return startTime;
	}

	/**
	 * 设置创建时间距离当前时间毫秒数
	 *
	 * @param startTime
	 *            创建时间距离当前时间毫秒数
	 */
	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	/**
	 * 获取创建时间
	 *
	 * @return create_date - 创建时间
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * 设置创建时间
	 *
	 * @param createDate
	 *            创建时间
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Override
	public String toString() {
		return "WxToken [id=" + id + ", accessToken=" + accessToken + ", expiresIn=" + expiresIn + ", startTime="
				+ startTime + ", createDate=" + createDate + "]";
	}

}