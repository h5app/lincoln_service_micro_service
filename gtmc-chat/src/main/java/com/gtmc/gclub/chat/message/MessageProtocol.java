package com.gtmc.gclub.chat.message;


import javax.validation.constraints.NotNull;

import com.gtmc.gclub.chat.constants.MessageConstant;
import java.util.UUID;


/**
 * 消息协议
 */
public class MessageProtocol {
	
    @Override
	public String toString() {
		return "MessageProtocol [messageType=" + messageType + ", content=" + content + ", timestamp=" + timestamp
				+ ", msgId=" + msgId + "]";
	}
    public MessageProtocol(){
    	this.id=UUID.randomUUID().toString();
    }
    
	/**
     * 消息唯一标识
     */
    private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	/**
     * 消息的业务类别
     */
    @NotNull
    private MessageConstant.MESSAGE_TYPE messageType;

    /**
     * 消息的内容
     */
    @NotNull
    private String content;
    
    /**
     * 消息投递的时间
     */
    @NotNull
    private Long timestamp;
    
    public MessageConstant.MESSAGE_TYPE getMessageType() {
		return messageType;
	}

	public void setMessageType(MessageConstant.MESSAGE_TYPE messageType) {
		this.messageType = messageType;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public Long getMsgId() {
		return msgId;
	}

	public void setMsgId(Long msgId) {
		this.msgId = msgId;
	}

	/**
     * 消息的ID
     */
    @NotNull
    private Long msgId;
    

   
}
