package com.gtmc.gclub.chat.service;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.gtmc.gclub.chat.util.DateUtil;
import com.gtmc.gclub.chat.util.RandomNum;
import com.gtmc.gclub.common.WxMaterialTypeEnum;
import com.infoservice.filestore.FileStore;
import com.infoservice.filestore.FileStoreException;

@Service
public class FileService {
	public static final String BASE_TEMP_FILE_PATH = File.separator+"home"+File.separator+"wechatuser"+File.separator+"appadmin"+File.separator+"service"+File.separator+"lkwechat"+File.separator+"lkFileTemp"+File.separator ;
//	public static final String BASE_TEMP_FILE_PATH = "D:\\filestore\\data\\dn01\\" ;
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	public String uploadFile(MultipartFile file,String fileType) {
		try {
			logger.info("上传文件到FileStore:uploadFile:");
			logger.debug("size:" + file.getSize());
			String extName = null ;
			switch (fileType) {
			case "image":
				extName = ".jpg";
				break;

			default:
				return "不支持文件类型"+fileType;
			}
			String name = DateUtil.format(new Date(), "HHmmssSSS") + RandomNum.createRandomString(6) + extName;
			String fileId = FileStore.getInstance().write("nd02", "fs01", name, file.getBytes());
			logger.debug("fileId = " + fileId);
			return fileId;
		} catch (FileStoreException e) {
			logger.error("FileStore is failure.", e);
			return "filestore failure";
		} catch (IOException e) {
			logger.error("FileStore have io error!.", e);
			return "io failure";
		} catch (Exception e) {
			logger.error("error:", e);
			return "io failure";
		}
	}
	public String uploadFileForLocal(MultipartFile file,WxMaterialTypeEnum fileType) throws IOException {
			logger.info("上传文件到本地:uploadFileForLocal:");
			logger.debug("size:" + file.getSize());
			String extName = null ;
			switch (fileType) {
			case image:
				extName = ".jpg";
				break;
				
			default:
				return "不支持文件类型"+fileType;
			}
			String name = DateUtil.format(new Date(), "HHmmssSSS") + RandomNum.createRandomString(6) + extName;
//			String fileId = FileStore.getInstance().write("nd02", "fs01", name, file.getBytes());
			String fileId = BASE_TEMP_FILE_PATH+name;
			File newFile = new File(fileId);
			InputStream in = new ByteArrayInputStream(file.getBytes());
			OutputStream out = new BufferedOutputStream(new FileOutputStream(newFile));
			byte[] tempByte = new byte[32768];
			int len = 0 ;
			while((len = in.read(tempByte)) >= 0) {
				out.write(tempByte,0,len);
			}
			out.close();
			out = null ;
			logger.debug("fileId = " + fileId);
			return fileId;
	}
	public void deleteFileForLocal(String url) throws FileStoreException {
		String fileName = url.substring(url.lastIndexOf("/"), url.length());
		FileStore.getInstance().delete(fileName);
	}
}
