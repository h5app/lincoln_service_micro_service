package com.gtmc.gclub.chat.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TM_CHAT_DIALOGUE")
public class TmChatDialogue {
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 经销商code
     */
    @Column(name = "dealer_code")
    private String dealerCode;

    /**
     * 内容
     */
    @Column(name = "content")
    private String content;

    /**
     * 使用次数
     */
    @Column(name = "use_num")
    private Integer useNum;

    /**
     * 创建时间
     */
    @Column(name = "create_date")
    private Date createDate;

    /**
     * 更新时间
     */
    @Column(name = "update_date")
    private Date updateDate;
    
    /**
     * 消息类型
     */
    @Column(name = "content_type")
    private String contentType;
    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取经销商code
     *
     * @return dealer_code - 经销商code
     */
    public String getDealerCode() {
        return dealerCode;
    }

    /**
     * 设置经销商code
     *
     * @param dealerCode 经销商code
     */
    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    /**
     * 获取内容
     *
     * @return content - 内容
     */
    public String getContent() {
        return content;
    }

    /**
     * 设置内容
     *
     * @param content 内容
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * 获取使用次数
     *
     * @return use_num - 使用次数
     */
    public Integer getUseNum() {
        return useNum;
    }

    /**
     * 设置使用次数
     *
     * @param useNum 使用次数
     */
    public void setUseNum(Integer useNum) {
        this.useNum = useNum;
    }

    /**
     * 获取创建时间
     *
     * @return create_date - 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间
     *
     * @param createDate 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取更新时间
     *
     * @return update_date - 更新时间
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * 设置更新时间
     *
     * @param updateDate 更新时间
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	@Override
	public String toString() {
		return "TmChatDialogue [id=" + id + ", dealerCode=" + dealerCode + ", content=" + content + ", useNum=" + useNum
				+ ", createDate=" + createDate + ", updateDate=" + updateDate + ", contentType=" + contentType + "]";
	}

}