package com.gtmc.gclub.chat.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gtmc.gclub.chat.dao.TmUsersMapper;
import com.gtmc.gclub.chat.model.TmUsers;

@Service
@Transactional
public class UserService {
	
	@Autowired
	private TmUsersMapper tmUsersMapper;
	
	public int insertUser(TmUsers user){
		return tmUsersMapper.insert(user);
	}
	
	public List<TmUsers> queryUsers(TmUsers user){
		return tmUsersMapper.select(user);
	}

}
