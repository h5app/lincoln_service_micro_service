package com.gtmc.gclub.chat.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "TM_CLASSIFY_CONTENT")
public class TmClassifyContent {
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 内容
     */
    private String content;

    /**
     * 上级节点id
     */
    private Integer pid;

    /**
     * 创建时间
     */
    @Column(name = "create_date")
    private Date createDate;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;
    /**
     * 消息类型
     */
    @Column(name = "content_type")
    private String contentType;
    /**
     * 消息ID
     */
    @Column(name = "media_id")
    private String mediaId;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取内容
     *
     * @return content - 内容
     */
    public String getContent() {
        return content;
    }

    /**
     * 设置内容
     *
     * @param content 内容
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * 获取上级节点id
     *
     * @return pid - 上级节点id
     */
    public Integer getPid() {
        return pid;
    }

    /**
     * 设置上级节点id
     *
     * @param pid 上级节点id
     */
    public void setPid(Integer pid) {
        this.pid = pid;
    }

    /**
     * 获取创建时间
     *
     * @return create_date - 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间
     *
     * @param createDate 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取更新时间
     *
     * @return update_time - 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     *
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }


	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	@Override
	public String toString() {
		return "TmClassifyContent [id=" + id + ", content=" + content + ", pid=" + pid + ", createDate=" + createDate
				+ ", updateTime=" + updateTime + ", contentType=" + contentType + "]";
	}

}