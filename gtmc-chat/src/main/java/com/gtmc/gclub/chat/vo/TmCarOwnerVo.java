package com.gtmc.gclub.chat.vo;

import com.gtmc.gclub.common.WxMaterialTypeEnum;

/**
 * 用户基础信息
 * */
public class TmCarOwnerVo {
	private String userId;// 后台管理系统用户ID
	private String name;// 昵称
	private String picUrl;// 头像
	private String phone;// 手机号
	private String jmessage;// jmessage账号，用于极光推送消息用
	private String jpassword;// jmessage密码，用于极光推送消息登录用
	private String wxCode;// 微信账号
	private WxMaterialTypeEnum lastContentType;// 最后聊天内容的消息类型
	private H5MessageVo lastContent;// 最后聊天内容
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPicUrl() {
		return picUrl;
	}
	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getJmessage() {
		return jmessage;
	}
	public void setJmessage(String jmessage) {
		this.jmessage = jmessage;
	}
	public String getJpassword() {
		return jpassword;
	}
	public void setJpassword(String jpassword) {
		this.jpassword = jpassword;
	}
	public String getWxCode() {
		return wxCode;
	}
	public void setWxCode(String wxCode) {
		this.wxCode = wxCode;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public WxMaterialTypeEnum getLastContentType() {
		return lastContentType;
	}
	public void setLastContentType(WxMaterialTypeEnum lastContentType) {
		this.lastContentType = lastContentType;
	}
	public H5MessageVo getLastContent() {
		return lastContent;
	}
	public void setLastContent(H5MessageVo lastContent) {
		this.lastContent = lastContent;
	}
	@Override
	public String toString() {
		return "TmCarOwnerVo [userId=" + userId + ", name=" + name + ", picUrl=" + picUrl + ", phone=" + phone
				+ ", jmessage=" + jmessage + ", jpassword=" + jpassword + ", wxCode=" + wxCode + ", lastContentType="
				+ lastContentType + ", lastContent=" + lastContent + "]";
	}
	
}
