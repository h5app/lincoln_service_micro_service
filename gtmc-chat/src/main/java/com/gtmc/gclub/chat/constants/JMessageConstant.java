package com.gtmc.gclub.chat.constants;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 
 * @author Administrator
 * jmessage的相关参数
 */
//@ConfigurationProperties(prefix = "com.yonyou")
public class JMessageConstant {
	
	public String appkey;

	public String masterSecret;

	public String getAppkey() {
		return appkey;
	}

	public void setAppkey(String appkey) {
		this.appkey = appkey;
	}

	public String getMasterSecret() {
		return masterSecret;
	}

	public void setMasterSecret(String masterSecret) {
		this.masterSecret = masterSecret;
	}
	
	

}
