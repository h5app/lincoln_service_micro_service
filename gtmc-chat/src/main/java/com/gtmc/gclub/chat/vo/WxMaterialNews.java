package com.gtmc.gclub.chat.vo;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gtmc.gclub.common.WxMaterialTypeEnum;

import sun.util.logging.resources.logging;

/**
 * @author kaihua
 *
 * 图文素材参数实体
 */
public class WxMaterialNews{
	private String media_id;
	private WxMaterialNewsEntity content;
	private Date update_time;
	public String getMedia_id() {
		return media_id;
	}
	public void setMedia_id(String media_id) {
		this.media_id = media_id;
	}
	public Date getUpdate_time() {
		return update_time;
	}
	public void setUpdate_time(Date update_time) {
		this.update_time = update_time;
	}
	public WxMaterialNewsEntity getContent() {
		return content;
	}
	public void setContent(WxMaterialNewsEntity content) {
		this.content = content;
	}
	@Override
	public String toString() {
		return "WxMaterialNews [media_id=" + media_id + ", content=" + content + ", update_time=" + update_time + "]";
	}
	/**
	 * 在这里目前只做一个图文消息只有一篇文章的形式，不包含多个。
	 * @param entity
	 * @return
	 */
	public static Map<String,Object> getH5JsonMap(WxMaterialNews entity){
		WxMaterialNewsEntity newsEntity = entity.getContent();
		WxMaterialNewsDetail wxMaterialNewsDetail = newsEntity.getNews_item().get(0);
		Map<String,Object> root = new HashMap<String,Object>();
		root.put("tagName", wxMaterialNewsDetail.getTitle());
		root.put("media_id", entity.getMedia_id());
		Map<String,String> entityDetail = new HashMap<String,String>();
		entityDetail.put("title", wxMaterialNewsDetail.getTitle());
		entityDetail.put("thumb_media_id", wxMaterialNewsDetail.getThumb_media_id());
		entityDetail.put("show_cover_pic", wxMaterialNewsDetail.getShow_cover_pic()==null ? null : wxMaterialNewsDetail.getShow_cover_pic().toString());
		entityDetail.put("author", wxMaterialNewsDetail.getAuthor());
		entityDetail.put("digest", wxMaterialNewsDetail.getDigest());
		entityDetail.put("content", wxMaterialNewsDetail.getContent());
		entityDetail.put("url", wxMaterialNewsDetail.getUrl());
		entityDetail.put("content_source_url", wxMaterialNewsDetail.getContent_source_url());
		entityDetail.put("type", WxMaterialTypeEnum.news.name());
		root.put("content", entityDetail);
		return root;
	}
}
class WxMaterialNewsEntity{
	private List<WxMaterialNewsDetail> news_item;

	public List<WxMaterialNewsDetail> getNews_item() {
		return news_item;
	}

	public void setNews_item(List<WxMaterialNewsDetail> news_item) {
		this.news_item = news_item;
	}

	@Override
	public String toString() {
		return "WxMaterialNewsEntity [news_item=" + news_item + "]";
	}
}
/**
 * @author kaihua
 *
 * 图文素材参数实体
 */
class WxMaterialNewsDetail{
	private String title;// 	图文消息的标题
	private String thumb_media_id;// 	图文消息的封面图片素材id（必须是永久mediaID）
	private Integer show_cover_pic;//	是否显示封面，0为false，即不显示，1为true，即显示
	private String author;// 	作者
	private String digest;// 	图文消息的摘要，仅有单图文消息才有摘要，多图文此处为空
	private String content;//	图文消息的具体内容，支持HTML标签，必须少于2万字符，小于1M，且此处会去除JS
	private String url;// 图文页的URL，或者，当获取的列表是图片素材列表时，该字段是图片的URL
	private String content_source_url;// 图文消息的原文地址，即点击“阅读原文”后的URL
	private String update_time;// 这篇图文消息素材的最后更新时间
	private String name;// 文件名称
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getThumb_media_id() {
		return thumb_media_id;
	}
	public void setThumb_media_id(String thumb_media_id) {
		this.thumb_media_id = thumb_media_id;
	}
	public Integer getShow_cover_pic() {
		return show_cover_pic;
	}
	public void setShow_cover_pic(Integer show_cover_pic) {
		this.show_cover_pic = show_cover_pic;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getDigest() {
		return digest;
	}
	public void setDigest(String digest) {
		this.digest = digest;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getContent_source_url() {
		return content_source_url;
	}
	public void setContent_source_url(String content_source_url) {
		this.content_source_url = content_source_url;
	}
	public String getUpdate_time() {
		return update_time;
	}
	public void setUpdate_time(String update_time) {
		this.update_time = update_time;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "WxMaterialNews [title=" + title + ", thumb_media_id=" + thumb_media_id + ", show_cover_pic="
				+ show_cover_pic + ", author=" + author + ", digest=" + digest + ", content=" + content + ", url=" + url
				+ ", content_source_url=" + content_source_url + ", update_time=" + update_time + ", name=" + name
				+ "]";
	}
}