package com.gtmc.gclub.chat;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;

import com.gtmc.gclub.chat.constants.CommonProperties;

@EnableCircuitBreaker
@EnableHystrix
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
@EnableRabbit
@EnableConfigurationProperties({ CommonProperties.class})
//@EnableAdminServer
public class GclubInterfaceApplication {

	public static void main(String[] args) {
		SpringApplication.run(GclubInterfaceApplication.class, args);
	}
}
