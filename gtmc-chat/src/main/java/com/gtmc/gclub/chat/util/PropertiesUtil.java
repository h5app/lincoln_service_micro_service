package com.gtmc.gclub.chat.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class PropertiesUtil {
	private static PropertiesUtil propsUtil;
	private static PropertiesUtil systemProper;
	private static PropertiesUtil apiAuthProper;
	private static PropertiesUtil idealerProper;
	private static PropertiesUtil rabbitMQProper;
	private static PropertiesUtil chatProper;
	private static PropertiesUtil smsProper;
	private static PropertiesUtil airParkingProper;
	private static PropertiesUtil sensorsAnalyProper;
	private static PropertiesUtil couponLogicProper;
	private static PropertiesUtil carRentalProper;
	private static PropertiesUtil yingShiCloudProper;
	private Properties props = new Properties();
	private String url;

	private PropertiesUtil(String filePath) {
		InputStream in = null;
		url = filePath;
		try {
			in = new BufferedInputStream(getClass().getResourceAsStream(filePath));
			props.load(in);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
				}
			}
		}
	}

	public static PropertiesUtil getInstance() {
		return propsUtil == null ? (propsUtil = new PropertiesUtil("/msg_zh.properties")) : propsUtil;
	}

	public static PropertiesUtil getSysInstance() {
		return systemProper == null ? (systemProper = new PropertiesUtil("/sys_msg_zh.properties")) : systemProper;
	}

	public static PropertiesUtil getApiInstance() {
		return apiAuthProper == null ? (apiAuthProper = new PropertiesUtil("/api_auth.properties")) : apiAuthProper;
	}

	public static PropertiesUtil getIdealerInstance() {
		return idealerProper == null ? (idealerProper = new PropertiesUtil("/idealerConfig.properties"))
				: idealerProper;
	}

	public static PropertiesUtil getRabbitMQInstance() {
		return rabbitMQProper == null ? (rabbitMQProper = new PropertiesUtil("/rabbitMQConfig.properties"))
				: rabbitMQProper;
	}

	public static PropertiesUtil getChatInstance() {
		return chatProper == null ? (chatProper = new PropertiesUtil("/chatConfig.properties")) : chatProper;
	}

	public static PropertiesUtil getSmsInstance() {
		return smsProper == null ? (smsProper = new PropertiesUtil("/smsConfig.properties")) : smsProper;
	}

	public static PropertiesUtil getAirParkingInstance() {
		return airParkingProper == null ? (airParkingProper = new PropertiesUtil("/airParkingConfig.properties"))
				: airParkingProper;
	}

	public static PropertiesUtil getSensorsAnalyInstance() {
		return sensorsAnalyProper == null ? (sensorsAnalyProper = new PropertiesUtil("/sensorsAnaly.properties"))
				: sensorsAnalyProper;
	}

	public static PropertiesUtil getCouponLogicInstance() {
		return couponLogicProper == null ? (couponLogicProper = new PropertiesUtil("/CouponLogic.properties"))
				: couponLogicProper;
	}

	public static PropertiesUtil getCarRentalInstance() {
		return carRentalProper == null ? (carRentalProper = new PropertiesUtil("/carRentalConfig.properties"))
				: carRentalProper;
	}

	public static PropertiesUtil getYingShiCloudInstance() {
		return yingShiCloudProper == null ? (yingShiCloudProper = new PropertiesUtil("/yingshiCloud.properties"))
				: yingShiCloudProper;
	}

	// 根据key读取value
	public String readValue(String key) {
		return props.getProperty(key);
	}

	public void writeProper(String key, String value) {
		OutputStream fos = null;
		try {
			fos = new FileOutputStream(new File(url));
			props.setProperty(key, value);
			// 将此 Properties 表中的属性列表（键和元素对）写入输出流
			props.store(fos, "『comments』Update key：" + key);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			try {
				if (fos != null) {
					fos.close();
				}
			} catch (Exception e) {

			}
		}
	}
}
