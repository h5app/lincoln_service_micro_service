package com.gtmc.gclub.chat.conimp;

/**
 * 为什么要再Controller和service中间加入这么一层：
 * 在开发过程中发现一个问题：controller中的业务如果略微复杂，需要在service中操作多张表的时候，
 * 若service中需要操作的表，用到了两种数据源：mysql、oracle，由于service中是一个事务完成的，
 * 就会造成一个数据源的链接还没有关闭，另外一个数据源事务过程已经结束，此时就会出现DB connection没有释放,
 * 甚至造成锁表。
 * 这一层就是把controller需要完成的业务，拆分成事务单元，然后交给service去处理这些事务单元。
 * @author kaihua
 *
 */
public interface BaseControllerImpl {

}
