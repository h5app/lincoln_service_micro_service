package com.gtmc.gclub.chat.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.gtmc.gclub.chat.annotation.MySQLDb;
import com.gtmc.gclub.chat.dto.ChatClassifyQueryDto;
import com.gtmc.gclub.chat.model.TmClassifyContent;
import com.gtmc.gclub.chat.vo.ClassifyAllContentVo;
import com.gtmc.gclub.chat.vo.ClassifyContentPageVo;

import tk.mybatis.mapper.common.Mapper;

@MySQLDb
public interface ExTmClassifyContentMapper extends Mapper<TmClassifyContent> {
	int insertBaseAll(TmClassifyContent tm);

	int insertBatchs(List<TmClassifyContent> list);

	List<ClassifyContentPageVo> getAppChatContent(com.gtmc.gclub.chat.dto.ChatClassifyQueryDto dto);

	List<ClassifyContentPageVo> getAppChatContentExport(ChatClassifyQueryDto dto);

	List<ClassifyContentPageVo> getWxChatContent(ChatClassifyQueryDto dto);
	
	List<ClassifyAllContentVo> getAllClassifyContentByDealerCode(@Param("dealerCode")String dealerCode);

}
