package com.gtmc.gclub.chat.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
 // 本表已废弃，替代表为：TR_USER_ENTERPRISE
@Table(name = "WX_CHAT_USER")
public class WxChatUser {
	/**
	 * 客服编号
	 */
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;

	/**
	 * 客服在微信系统的id
	 */
	@Column(name = "weixin_code")
	private String weixinCode;

	/**
	 * 客服在微信端名称
	 */
	@Column(name = "weixin_name")
	private String weixinName;

	/**
	 * 头像地址
	 */
	@Column(name = "icons_photo")
	private String iconsPhoto;

	/**
	 * 所属销售店code
	 */
	@Column(name = "dealer_code")
	private String dealerCode;

	/**
	 * 所属销售店名称
	 */
	@Column(name = "dealer_name")
	private String dealerName;

	/**
	 * 客服种类 0, 销售 1 售后
	 */
	@Column(name = "dealer_cs_type")
	private Byte dealerCsType;

	/**
	 * 在imessage注册账号
	 */
	@Column(name = "imessage_code")
	private String imessageCode;
	
	/**
	 * 在林肯后台对应的UserId
	 */
	@Column(name = "LK_USER_ID")
	private String lkUserId;

	/**
	 * 获取客服编号
	 *
	 * @return id - 客服编号
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 设置客服编号
	 *
	 * @param id
	 *            客服编号
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 获取客服在微信系统的id
	 *
	 * @return weixin_code - 客服在微信系统的id
	 */
	public String getWeixinCode() {
		return weixinCode;
	}

	/**
	 * 设置客服在微信系统的id
	 *
	 * @param weixinCode
	 *            客服在微信系统的id
	 */
	public void setWeixinCode(String weixinCode) {
		this.weixinCode = weixinCode;
	}

	/**
	 * 获取客服在微信端名称
	 *
	 * @return weixin_name - 客服在微信端名称
	 */
	public String getWeixinName() {
		return weixinName;
	}

	/**
	 * 设置客服在微信端名称
	 *
	 * @param weixinName
	 *            客服在微信端名称
	 */
	public void setWeixinName(String weixinName) {
		this.weixinName = weixinName;
	}

	/**
	 * 获取头像地址
	 *
	 * @return icons_photo - 头像地址
	 */
	public String getIconsPhoto() {
		return iconsPhoto;
	}

	/**
	 * 设置头像地址
	 *
	 * @param iconsPhoto
	 *            头像地址
	 */
	public void setIconsPhoto(String iconsPhoto) {
		this.iconsPhoto = iconsPhoto;
	}

	/**
	 * 获取所属销售店code
	 *
	 * @return dealer_code - 所属销售店code
	 */
	public String getDealerCode() {
		return dealerCode;
	}

	/**
	 * 设置所属销售店code
	 *
	 * @param dealerCode
	 *            所属销售店code
	 */
	public void setDealerCode(String dealerCode) {
		this.dealerCode = dealerCode;
	}

	/**
	 * 获取所属销售店名称
	 *
	 * @return dealer_name - 所属销售店名称
	 */
	public String getDealerName() {
		return dealerName;
	}

	/**
	 * 设置所属销售店名称
	 *
	 * @param dealerName
	 *            所属销售店名称
	 */
	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}

	/**
	 * 获取客服种类 0, 销售 1 售后
	 *
	 * @return dealer_cs_type - 客服种类 0, 销售 1 售后
	 */
	public Byte getDealerCsType() {
		return dealerCsType;
	}

	/**
	 * 设置客服种类 0, 销售 1 售后
	 *
	 * @param dealerCsType
	 *            客服种类 0, 销售 1 售后
	 */
	public void setDealerCsType(Byte dealerCsType) {
		this.dealerCsType = dealerCsType;
	}

	/**
	 * 获取在imessage注册账号
	 *
	 * @return imessage_code - 在imessage注册账号
	 */
	public String getImessageCode() {
		return imessageCode;
	}

	/**
	 * 设置在imessage注册账号
	 *
	 * @param imessageCode
	 *            在imessage注册账号
	 */
	public void setImessageCode(String imessageCode) {
		this.imessageCode = imessageCode;
	}
	/**
	 * 在林肯后台对应的UserId
	 */
	public String getLkUserId() {
		return lkUserId;
	}
	/**
	 * 在林肯后台对应的UserId
	 */
	public void setLkUserId(String lkUserId) {
		this.lkUserId = lkUserId;
	}

	@Override
	public String toString() {
		return "WxChatUser [id=" + id + ", weixinCode=" + weixinCode + ", weixinName=" + weixinName + ", iconsPhoto="
				+ iconsPhoto + ", dealerCode=" + dealerCode + ", dealerName=" + dealerName + ", dealerCsType="
				+ dealerCsType + ", imessageCode=" + imessageCode + ", lkUserId=" + lkUserId + "]";
	}

}