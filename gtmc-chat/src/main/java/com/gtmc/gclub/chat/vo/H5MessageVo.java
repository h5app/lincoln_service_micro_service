package com.gtmc.gclub.chat.vo;

import com.gtmc.gclub.chat.model.TmPotentialUser;
import com.gtmc.gclub.common.WxMaterialTypeEnum;

/**
 * 给H5显示的，所有的消息、常见内容、消息记录等
 * @author kaihua
 *
 */
public class H5MessageVo {
	public static final String MESSAGE_FROM_H5 = "h5";
	public static final String MESSAGE_FROM_WX = "wx";
	private TmPotentialUser fromUser;
	private String content;
	private Long lastTime;
	private WxMaterialTypeEnum type;
	private String title;
	private String url;
	private String mediaId;
	private String messageSource;// 标识消息是从h5转发过来，还是用户发送过来
	public TmPotentialUser getFromUser() {
		return fromUser;
	}
	public void setFromUser(TmPotentialUser fromUser) {
		this.fromUser = fromUser;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public WxMaterialTypeEnum getType() {
		return type;
	}
	public void setType(WxMaterialTypeEnum type) {
		this.type = type;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getMediaId() {
		return mediaId;
	}
	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}
	public String getMessageSource() {
		return messageSource;
	}
	public void setMessageSource(String messageSource) {
		this.messageSource = messageSource;
	}
	public Long getLastTime() {
		return lastTime;
	}
	public void setLastTime(Long lastTime) {
		this.lastTime = lastTime;
	}
	@Override
	public String toString() {
		return "H5MessageVo [fromUser=" + fromUser + ", content=" + content + ", lastTime=" + lastTime + ", type="
				+ type + ", title=" + title + ", url=" + url + ", mediaId=" + mediaId + ", messageSource="
				+ messageSource + "]";
	}
}
