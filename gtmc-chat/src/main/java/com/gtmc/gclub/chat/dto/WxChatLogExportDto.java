package com.gtmc.gclub.chat.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.gtmc.gclub.common.WxMaterialTypeEnum;

public class WxChatLogExportDto {
	/**
	 * 客服编号
	 */
	private Integer wxUserId;
	private String wxUserName;
	
	private String dealerCode;
	private String dealerName;

	/**
	 * app用户账号
	 */
	private String ownerCode;
	private String ownerName;
	
	private String ownerModel;
	private String ownerModelName;

	/**
	 * 聊天内容
	 */
	private String content;
	private String contentType;

	private Date searchStartDate;
	private Date searchEndDate;

	/**
	 * 获取客服编号
	 *
	 * @return wx_user_id - 客服编号
	 */
	public Integer getWxUserId() {
		return wxUserId;
	}

	/**
	 * 设置客服编号
	 *
	 * @param wxUserId
	 *            客服编号
	 */
	public void setWxUserId(Integer wxUserId) {
		this.wxUserId = wxUserId;
	}

	/**
	 * 获取app用户账号
	 *
	 * @return owner_code - app用户账号
	 */
	public String getOwnerCode() {
		return ownerCode;
	}

	/**
	 * 设置app用户账号
	 *
	 * @param ownerCode
	 *            app用户账号
	 */
	public void setOwnerCode(String ownerCode) {
		this.ownerCode = ownerCode;
	}

	/**
	 * 获取聊天内容
	 *
	 * @return content - 聊天内容
	 */
	public String getContent() {
		return content;
	}

	/**
	 * 设置聊天内容
	 *
	 * @param content
	 *            聊天内容
	 */
	public void setContent(String content) {
		this.content = content;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public void setContentType(WxMaterialTypeEnum contentType) {
		this.contentType = contentType.name();
	}

	public String getWxUserName() {
		return wxUserName;
	}

	public void setWxUserName(String wxUserName) {
		this.wxUserName = wxUserName;
	}

	public String getDealerCode() {
		return dealerCode;
	}

	public void setDealerCode(String dealerCode) {
		this.dealerCode = dealerCode;
	}

	public String getDealerName() {
		return dealerName;
	}

	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getOwnerModel() {
		return ownerModel;
	}

	public void setOwnerModel(String ownerModel) {
		this.ownerModel = ownerModel;
	}

	public String getOwnerModelName() {
		return ownerModelName;
	}

	public void setOwnerModelName(String ownerModelName) {
		this.ownerModelName = ownerModelName;
	}

	public Date getSearchStartDate() {
		return searchStartDate;
	}

	public void setSearchStartDate(Date searchStartDate) {
		this.searchStartDate = searchStartDate;
	}

	public Date getSearchEndDate() {
		return searchEndDate;
	}

	public void setSearchEndDate(Date searchEndDate) {
		this.searchEndDate = searchEndDate;
	}

	@Override
	public String toString() {
		return "WxChatLogExportDto [wxUserId=" + wxUserId + ", wxUserName=" + wxUserName + ", dealerCode=" + dealerCode
				+ ", dealerName=" + dealerName + ", ownerCode=" + ownerCode + ", ownerName=" + ownerName
				+ ", ownerModel=" + ownerModel + ", ownerModelName=" + ownerModelName + ", content=" + content
				+ ", contentType=" + contentType + ", searchStartDate=" + searchStartDate + ", searchEndDate="
				+ searchEndDate + "]";
	}


}