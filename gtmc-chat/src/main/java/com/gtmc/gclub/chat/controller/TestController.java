package com.gtmc.gclub.chat.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gtmc.gclub.chat.bean.Result;
import com.gtmc.gclub.chat.constants.CommonProperties;
import com.gtmc.gclub.chat.model.TrUserEnterprise;
import com.gtmc.gclub.chat.model.WxToken;
import com.gtmc.gclub.chat.service.WxService;
import com.gtmc.gclub.chat.service.WxUserService;
import com.gtmc.gclub.chat.util.MD5Util;
import com.gtmc.gclub.chat.vo.WxMaterialBaseVo;
import com.gtmc.gclub.common.WxMaterialTypeEnum;
import com.gtmc.gclub.common.WxMediaFileSourceType;
import com.gtmc.gclub.common.WxUtil;
import com.gtmc.gclub.wx.dto.JmessAuth;
import com.gtmc.gclub.wx.dto.QyUserInfo;
import com.gtmc.gclub.wx.dto.WxJsApiAuth;

@Controller
@RequestMapping("/wx/user")
public class TestController extends BaseController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private WxService wxService;

	@Autowired
	private WxUserService wxUserService;

	@Autowired
	private CommonProperties commonProperties;

	@RequestMapping(value = "/getmsg")
	public Map<String, String> testVoid() {

		Map<String, String> returnMap = new HashMap<String, String>();
		returnMap.put("123", "hello");
		returnMap.put("343", "hello222");

		return returnMap;
	}

	@RequestMapping(value = "/jsSdktest")
	public String test1(@RequestParam(value = "code", required = true) String code,
			@RequestParam(value = "state", required = true) String state, Model model) {
		logger.info("微信登录接口");
		// 获取token
		WxToken token = wxService.getWxToken();
		// 获取微信用户信息
		QyUserInfo userInfo = WxUtil.getUserInfo(token.getAccessToken(), code);
		// 查询用户是否存在
		TrUserEnterprise user = wxUserService.getFirstWxChatUser(token.getAccessToken(), userInfo.getUserId());

		model.addAttribute("userInfo", user);
		return "wx/wxchat/chat_list";
	}

	/**
	 * 调试使用
	 * 
	 * @param userId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/wx_chat_reload", method = RequestMethod.GET)
	public String wxChat(@RequestParam(value = "wxCode", required = true) String wxCode, Model model) {
		logger.info("刷新chatList页面");
		TrUserEnterprise user = wxUserService.getFirstWxChatUser1(null, wxCode);
		model.addAttribute("userInfo", user);
		return "wx/wxchat/chat_list";
	}

	@RequestMapping(value = "/wx_chat_customer", method = RequestMethod.POST)
	@ResponseBody
	public Result<List<Map<String, Object>>> getCustomer(
			@RequestParam(value = "userId", required = true) Integer userId) {
		// 获取客户列表
		return new Result<List<Map<String, Object>>>(1, wxUserService.getWxChatList(userId,null), null);
	}

	/**
	 * 跳转聊天页面
	 * 
	 * @param ownerCode
	 * @param userId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/wx_chat", method = RequestMethod.GET)
	public String wxChat(@RequestParam(value = "ownerCode", required = true) String ownerCode,
			@RequestParam(value = "userId", required = true) String userId,
			@RequestParam(value = "wxCode", required = true) String wxCode, Model model) {
		logger.info("进入chat页面:wxChat");
		model.addAttribute("userId", userId);
		model.addAttribute("wxCode", wxCode);
		model.addAttribute("ownerCode", ownerCode);
		return "wx/wxchat/chat";
	}

	/**
	 * 查询聊天 用户-客户信息
	 * 
	 * @param ownerCode
	 * @param userId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/wx_chat_userInfo", method = RequestMethod.POST)
	@ResponseBody
	public Result<Map<String, Object>> wxChatUserInfo(
			@RequestParam(value = "ownerCode", required = true) String ownerCode,
			@RequestParam(value = "userId", required = true) String userId,
			@RequestParam(value = "url", required = true) String url, Model model) {
		logger.info("获取聊天用户信息:wxChatUserInfo");
		Map<String, Object> map = wxUserService.getUserInfo(ownerCode, userId);
		try {
			JmessAuth auth = wxUserService.getJmessAuth();
			WxJsApiAuth jsAuth = wxService.getWxJsApiAuth(url);
			map.put("auth", auth);
			map.put("jsAuth", jsAuth);
		} catch (Exception e) {
			logger.error("查询auth出错" + e.getMessage());
		}
		return new Result<Map<String, Object>>(1, map, null);
	}

	/**
	 * 查询聊天对话信息
	 * 
	 * @param ownerCode
	 * @param userId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/wx_chat_talk", method = RequestMethod.POST)
	@ResponseBody
	public Result<List<Map<String, Object>>> wxChatTalk(
			@RequestParam(value = "pageCurrent", required = true) Integer pageCurrent,
			@RequestParam(value = "pageSize", required = true) Integer pageSize,
			@RequestParam(value = "ownerCode", required = true) String ownerCode,
			@RequestParam(value = "userId", required = true) String userId,
			@RequestParam(value = "chatId", required = false) Integer chatId) {
		logger.info("获取聊天详情:wxChatTalk");
		// List<Map<String, Object>> list = wxUserService.getWxChatTalk(userId,
		// ownerCode, pageCurrent, pageSize, chatId);
		return new Result(1, null, null);
	}

	/**
	 * 发送信息接口
	 * 
	 * @param ownerCode
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "/wx_sendmessage", method = RequestMethod.POST)
	@ResponseBody
	public Result<Map> sendMessage(@RequestParam(value = "ownerCode", required = true) String ownerCode,
			@RequestParam(value = "userId", required = true) String userId,
			@RequestParam(value = "content", required = true) String content) {
		logger.info("发送信息接口:sendMessage");
		Map<String, Object> map = wxUserService.getJmessage(userId, ownerCode);
		map.put("content", content);
		map.put("contentType", WxMaterialTypeEnum.video);
		return wxUserService.sendMessage(map);
	}

	@RequestMapping(value = "/wx_sendimage", method = RequestMethod.POST)
	@ResponseBody
	public Result<Map> sendImage(@RequestParam(value = "ownerCode", required = true) String ownerCode,
			@RequestParam(value = "userId", required = true) String userId,
			@RequestParam(value = "serId", required = true) String serId) throws Exception {
		logger.info("发送图片信息接口:sendImage");
		System.out.println(ownerCode + "," + userId + "," + serId);
		String fileUrl = "@" + commonProperties.appkey + wxService.getFileUrl(serId,WxMaterialTypeEnum.image,WxMediaFileSourceType.temp).getLocalUrl();
		Map<String, Object> map = wxUserService.getJmessage(userId, ownerCode);
		map.put("content", fileUrl);
		map.put("contentType", WxMaterialTypeEnum.image);
		return wxUserService.sendMessage(map);
	}
	
	@RequestMapping(value = "/testAllMaterial", method = RequestMethod.POST)
	@ResponseBody
	public List<WxMaterialBaseVo> testAllMaterial() throws Exception {
		logger.debug("开始获取视频素材");
		return wxService.getAllMaterial(WxMaterialTypeEnum.video);
	}
	
	
	public static void main(String[] args) {
		Long currentTime = new Date().getTime();
		System.out.println("timestamp:"+currentTime);
		System.out.println("signature:"+MD5Util.MD5("appkey=2da93a099ef98d054aeb585a&timestamp="+currentTime+"&random_str=404&key=93a7cdc961dc2cbd2c0b0ab4"));
	}
}
