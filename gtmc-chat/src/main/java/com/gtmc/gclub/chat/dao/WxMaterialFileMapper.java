package com.gtmc.gclub.chat.dao;

import com.gtmc.gclub.chat.annotation.MySQLDb;
import com.gtmc.gclub.chat.model.WxMaterialFile;

import tk.mybatis.mapper.common.Mapper;

@MySQLDb
public interface WxMaterialFileMapper extends Mapper<WxMaterialFile> {

}