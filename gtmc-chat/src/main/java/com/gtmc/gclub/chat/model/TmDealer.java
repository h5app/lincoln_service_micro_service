package com.gtmc.gclub.chat.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "TM_DEALER")
public class TmDealer {
	// 店代码 主数据、接口字段：代理商编码
    @Column(name = "DEALER_CODE")
    @Id
    private String dealerCode;
    // 店名称 主数据、接口字段
    @Column(name = "DEALER_NAME")
    private String dealerName;
    // 店简称 主数据、接口字段
    @Column(name = "DEALER_SHORTNAME")
    private String dealerShortname;
    // 店类型 主数据、接口字段：1 4S店、2 特许店、3 综合店
    @Column(name = "DEALER_TYPE")
    private Short dealerType;
    // 省份ID 主数据
    @Column(name = "PROVINCE_ID")
    private String provinceId;
    // 城市ID 主数据
    @Column(name = "CITY_ID")
    private String cityId;
    // 所属区域 接口字段：所属的市，应该同CITY_ID
    @Column(name = "REGION_CODE")
    private String regionCode;

//    @Column(name = "BELONG_SALES_COOPERATIVE")
//    private String belongSalesCooperative;

//    @Column(name = "BELONG_SERVICE_COOPERATIVE")
//    private String belongServiceCooperative;
    // 详细地址 主数据、接口字段
    @Column(name = "DETAIL_ADDRESS")
    private String detailAddress;
    // 经度 接口字段：待收集
    @Column(name = "LONGITUDE")
    private BigDecimal longitude;
    // 纬度 接口字段：待收集
    @Column(name = "LATITUDE")
    private BigDecimal latitude;
    // 救援电话 接口字段
    @Column(name = "SOS_TEL")
    private String sosTel;
    // 销售热线 接口字段：销售电话
    @Column(name = "SALES_HOTLINE")
    private String salesHotline;
    // 服务热线 接口字段：售后电话中存在其它字符
    @Column(name = "SERVICE_HOTLINE")
    private String serviceHotline;
    // 星级评价  接口字段 
    @Column(name = "STAR_RATING")
    private BigDecimal starRating;
    // 代理商图片 接口字段：待收集
    @Column(name = "PICTURE_URL")
    private String pictureUrl;
    // 删除标识 1未删除 -0已删除
    @Column(name = "DELETE_FLAG")
    private Short deleteFlag;
    // 增量时间戳
    @Column(name = "INCREMENTAL_TIME")
    private Date incrementalTime;

//    @Column(name = "DRIVINGTEST_STARTTIME")
//    private String drivingtestStarttime;

//    @Column(name = "DRIVINGTEST_ENDTIME")
//    private String drivingtestEndtime;

//    @Column(name = "GSC_FLAG")
//    private Short gscFlag;

    @Column(name = "GSC_DEALER_FLAG")
    private Short gscDealerFlag;

//    @Column(name = "WEICHAT_CS_FLAG")
//    private Short weichatCsFlag;

//    @Column(name = "WEICHAT_CS_REMARK")
//    private String weichatCsRemark;

//    @Column(name = "APP_CLUB_FLAG")
//    private Short appClubFlag;
    // ASC状态 主数据 10011001:新建 10011002:签约 10011003:启用 10011004:停用
    @Column(name = "ASC_STATUS")
    private String ascStatus;
    // 是否入网标识 主数据
    @Column(name = "ONLINE_STATUS")
    private String onlineStatus;
    // 创建时间
    @Column(name = "CREATE_DATE")
    private Date createDate;
    // 更新时间
    @Column(name = "UPDATE_DATE")
    private Date updateDate;
    // 创建人
    @Column(name = "CREATE_BY")
    private BigDecimal createBy;
    // 最后更新人
    @Column(name = "UPDATE_BY")
    private BigDecimal updateBy;
    
    @Column(name = "URL")
    private String url;
    
    @Column(name = "DEALERLNG")
    private String dealerlng;
    
    @Column(name = "DEALERLAT")
    private String dealerlat;
    // 是否收费10011001-收费,10011002-不收费
    @Column(name = "IS_CHARGE")
    private BigDecimal isCharge;
    // 4S店联系人
    @Column(name = "CONTACTS_MAN")
    private String contactsMan;
    // 联系电话
    @Column(name = "CONTACTS_TEL")
    private String contactsTel;
    // 总经理邮箱
    @Column(name = "MANAGER_EMAIL")
    private String managerEmail;
    // 所属集团
    @Column(name = "BELONG_GROUP")
    private BigDecimal belongGroup;
    // 所属代理商
    @Column(name = "BELONG_AGENT")
    private BigDecimal belongAgent;
    // 所属客服
    @Column(name = "CUSTOMER_CARE")
    private BigDecimal customerAare;
    // 是否开通微信高级端口
    @Column(name = "WECHAT_SENIOR")
    private BigDecimal wechatSenior;
    // 4S店首字母
    @Column(name = "FIRST_WORD")
    private String firstWord;
    // 是否设置为样板店
    @Column(name = "IS_SAMPLEPLATE")
    private BigDecimal isSampleplate;
    // 是否开启模板消息
    @Column(name = "TEMPLET_MSG")
    private BigDecimal templetMsg;
    // 所在行业
    @Column(name = "PROFESSION")
    private BigDecimal profession;
    // 是否连锁店
    @Column(name = "CHAIN_STORE")
    private BigDecimal chainStore;
    // LOGG图片的URL
    @Column(name = "LOGS_URL")
    private String logsUrl;
    // 经销商授权方appid
    @Column(name = "AUTHORIZER_APPID")
    private String authorizerAppid;
    // DMS经销商代码
    @Column(name = "DMS_DEALER_CODE")
    private String dmsDealerCode;
    // 图片创建日期
    @Column(name = "IMG_CREATE_DATE")
    private Date imgCreateDate;
    //  品牌表主键
    @Column(name = "TM_BRAND_ID")
    private BigDecimal tmBrandId;
    // 县
    @Column(name = "TOWN_ID")
    private BigDecimal townId;
    // 版本号
    @Column(name = "DICTIONARY_CODE")
    private BigDecimal dictionaryCode;
    // 是否测试帐号
    @Column(name = "IS_TEST_ACCOUNT")
    private BigDecimal isTestAccount;
    // 微信公众号管理表ID
    @Column(name = "TM_WX_PUBLIC_ACCOUNT_ID")
    private BigDecimal tmWxPublicAccountId;
    // 经销商对应微信二维码rul
    @Column(name = "WX_QRCODE_URL")
    private String wxQrcodeUrl;
    // 经销商对应微信二维码ticket
    @Column(name = "WX_QRCODE_TICKET")
    private String wxQrcodeTicket;
    // dcms的经销商主键
    @Column(name = "DEALER_ID")
    private BigDecimal dealerId;
    // 取送车服务里程
    @Column(name = "GSC_MAX_MILEAGE")
    private BigDecimal gscMaxMileage;

	public String getDealerCode() {
		return dealerCode;
	}

	public void setDealerCode(String dealerCode) {
		this.dealerCode = dealerCode;
	}

	public String getDealerName() {
		return dealerName;
	}

	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}

	public String getDealerShortname() {
		return dealerShortname;
	}

	public void setDealerShortname(String dealerShortname) {
		this.dealerShortname = dealerShortname;
	}

	public Short getDealerType() {
		return dealerType;
	}

	public void setDealerType(Short dealerType) {
		this.dealerType = dealerType;
	}

	public String getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(String provinceId) {
		this.provinceId = provinceId;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getRegionCode() {
		return regionCode;
	}

	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}

	public String getDetailAddress() {
		return detailAddress;
	}

	public void setDetailAddress(String detailAddress) {
		this.detailAddress = detailAddress;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public String getSosTel() {
		return sosTel;
	}

	public void setSosTel(String sosTel) {
		this.sosTel = sosTel;
	}

	public String getSalesHotline() {
		return salesHotline;
	}

	public void setSalesHotline(String salesHotline) {
		this.salesHotline = salesHotline;
	}

	public String getServiceHotline() {
		return serviceHotline;
	}

	public void setServiceHotline(String serviceHotline) {
		this.serviceHotline = serviceHotline;
	}

	public BigDecimal getStarRating() {
		return starRating;
	}

	public void setStarRating(BigDecimal starRating) {
		this.starRating = starRating;
	}

	public String getPictureUrl() {
		return pictureUrl;
	}

	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}

	public Short getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(Short deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public Date getIncrementalTime() {
		return incrementalTime;
	}

	public void setIncrementalTime(Date incrementalTime) {
		this.incrementalTime = incrementalTime;
	}

	public Short getGscDealerFlag() {
		return gscDealerFlag;
	}

	public void setGscDealerFlag(Short gscDealerFlag) {
		this.gscDealerFlag = gscDealerFlag;
	}

	public String getAscStatus() {
		return ascStatus;
	}

	public void setAscStatus(String ascStatus) {
		this.ascStatus = ascStatus;
	}

	public String getOnlineStatus() {
		return onlineStatus;
	}

	public void setOnlineStatus(String onlineStatus) {
		this.onlineStatus = onlineStatus;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public BigDecimal getCreateBy() {
		return createBy;
	}

	public void setCreateBy(BigDecimal createBy) {
		this.createBy = createBy;
	}

	public BigDecimal getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(BigDecimal updateBy) {
		this.updateBy = updateBy;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDealerlng() {
		return dealerlng;
	}

	public void setDealerlng(String dealerlng) {
		this.dealerlng = dealerlng;
	}

	public String getDealerlat() {
		return dealerlat;
	}

	public void setDealerlat(String dealerlat) {
		this.dealerlat = dealerlat;
	}

	public BigDecimal getIsCharge() {
		return isCharge;
	}

	public void setIsCharge(BigDecimal isCharge) {
		this.isCharge = isCharge;
	}

	public String getContactsMan() {
		return contactsMan;
	}

	public void setContactsMan(String contactsMan) {
		this.contactsMan = contactsMan;
	}

	public String getContactsTel() {
		return contactsTel;
	}

	public void setContactsTel(String contactsTel) {
		this.contactsTel = contactsTel;
	}

	public String getManagerEmail() {
		return managerEmail;
	}

	public void setManagerEmail(String managerEmail) {
		this.managerEmail = managerEmail;
	}

	public BigDecimal getBelongGroup() {
		return belongGroup;
	}

	public void setBelongGroup(BigDecimal belongGroup) {
		this.belongGroup = belongGroup;
	}

	public BigDecimal getBelongAgent() {
		return belongAgent;
	}

	public void setBelongAgent(BigDecimal belongAgent) {
		this.belongAgent = belongAgent;
	}

	public BigDecimal getCustomerAare() {
		return customerAare;
	}

	public void setCustomerAare(BigDecimal customerAare) {
		this.customerAare = customerAare;
	}

	public BigDecimal getWechatSenior() {
		return wechatSenior;
	}

	public void setWechatSenior(BigDecimal wechatSenior) {
		this.wechatSenior = wechatSenior;
	}

	public String getFirstWord() {
		return firstWord;
	}

	public void setFirstWord(String firstWord) {
		this.firstWord = firstWord;
	}

	public BigDecimal getIsSampleplate() {
		return isSampleplate;
	}

	public void setIsSampleplate(BigDecimal isSampleplate) {
		this.isSampleplate = isSampleplate;
	}

	public BigDecimal getTempletMsg() {
		return templetMsg;
	}

	public void setTempletMsg(BigDecimal templetMsg) {
		this.templetMsg = templetMsg;
	}

	public BigDecimal getProfession() {
		return profession;
	}

	public void setProfession(BigDecimal profession) {
		this.profession = profession;
	}

	public BigDecimal getChainStore() {
		return chainStore;
	}

	public void setChainStore(BigDecimal chainStore) {
		this.chainStore = chainStore;
	}

	public String getLogsUrl() {
		return logsUrl;
	}

	public void setLogsUrl(String logsUrl) {
		this.logsUrl = logsUrl;
	}

	public String getAuthorizerAppid() {
		return authorizerAppid;
	}

	public void setAuthorizerAppid(String authorizerAppid) {
		this.authorizerAppid = authorizerAppid;
	}

	public String getDmsDealerCode() {
		return dmsDealerCode;
	}

	public void setDmsDealerCode(String dmsDealerCode) {
		this.dmsDealerCode = dmsDealerCode;
	}

	public Date getImgCreateDate() {
		return imgCreateDate;
	}

	public void setImgCreateDate(Date imgCreateDate) {
		this.imgCreateDate = imgCreateDate;
	}

	public BigDecimal getTmBrandId() {
		return tmBrandId;
	}

	public void setTmBrandId(BigDecimal tmBrandId) {
		this.tmBrandId = tmBrandId;
	}

	public BigDecimal getTownId() {
		return townId;
	}

	public void setTownId(BigDecimal townId) {
		this.townId = townId;
	}

	public BigDecimal getDictionaryCode() {
		return dictionaryCode;
	}

	public void setDictionaryCode(BigDecimal dictionaryCode) {
		this.dictionaryCode = dictionaryCode;
	}

	public BigDecimal getIsTestAccount() {
		return isTestAccount;
	}

	public void setIsTestAccount(BigDecimal isTestAccount) {
		this.isTestAccount = isTestAccount;
	}

	public BigDecimal getTmWxPublicAccountId() {
		return tmWxPublicAccountId;
	}

	public void setTmWxPublicAccountId(BigDecimal tmWxPublicAccountId) {
		this.tmWxPublicAccountId = tmWxPublicAccountId;
	}

	public String getWxQrcodeUrl() {
		return wxQrcodeUrl;
	}

	public void setWxQrcodeUrl(String wxQrcodeUrl) {
		this.wxQrcodeUrl = wxQrcodeUrl;
	}

	public String getWxQrcodeTicket() {
		return wxQrcodeTicket;
	}

	public void setWxQrcodeTicket(String wxQrcodeTicket) {
		this.wxQrcodeTicket = wxQrcodeTicket;
	}

	public BigDecimal getDealerId() {
		return dealerId;
	}

	public void setDealerId(BigDecimal dealerId) {
		this.dealerId = dealerId;
	}

	public BigDecimal getGscMaxMileage() {
		return gscMaxMileage;
	}

	public void setGscMaxMileage(BigDecimal gscMaxMileage) {
		this.gscMaxMileage = gscMaxMileage;
	}

	@Override
	public String toString() {
		return "TmDealer [dealerCode=" + dealerCode + ", dealerName=" + dealerName + ", dealerShortname="
				+ dealerShortname + ", dealerType=" + dealerType + ", provinceId=" + provinceId + ", cityId=" + cityId
				+ ", regionCode=" + regionCode + ", detailAddress=" + detailAddress + ", longitude=" + longitude
				+ ", latitude=" + latitude + ", sosTel=" + sosTel + ", salesHotline=" + salesHotline
				+ ", serviceHotline=" + serviceHotline + ", starRating=" + starRating + ", pictureUrl=" + pictureUrl
				+ ", deleteFlag=" + deleteFlag + ", incrementalTime=" + incrementalTime + ", gscDealerFlag="
				+ gscDealerFlag + ", ascStatus=" + ascStatus + ", onlineStatus=" + onlineStatus + ", createDate="
				+ createDate + ", updateDate=" + updateDate + ", createBy=" + createBy + ", updateBy=" + updateBy
				+ ", url=" + url + ", dealerlng=" + dealerlng + ", dealerlat=" + dealerlat + ", isCharge=" + isCharge
				+ ", contactsMan=" + contactsMan + ", contactsTel=" + contactsTel + ", managerEmail=" + managerEmail
				+ ", belongGroup=" + belongGroup + ", belongAgent=" + belongAgent + ", customerAare=" + customerAare
				+ ", wechatSenior=" + wechatSenior + ", firstWord=" + firstWord + ", isSampleplate=" + isSampleplate
				+ ", templetMsg=" + templetMsg + ", profession=" + profession + ", chainStore=" + chainStore
				+ ", logsUrl=" + logsUrl + ", authorizerAppid=" + authorizerAppid + ", dmsDealerCode=" + dmsDealerCode
				+ ", imgCreateDate=" + imgCreateDate + ", tmBrandId=" + tmBrandId + ", townId=" + townId
				+ ", dictionaryCode=" + dictionaryCode + ", isTestAccount=" + isTestAccount + ", tmWxPublicAccountId="
				+ tmWxPublicAccountId + ", wxQrcodeUrl=" + wxQrcodeUrl + ", wxQrcodeTicket=" + wxQrcodeTicket
				+ ", dealerId=" + dealerId + ", gscMaxMileage=" + gscMaxMileage + "]";
	}

}