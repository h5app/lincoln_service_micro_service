package com.gtmc.gclub.chat.constants;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "com.yonyou")
public class CommonProperties {
	public String corpid;

	public String corpsecret;

	public Integer tokenId;

	public String appkey;

	public String masterSecret;

	public String pwd;

	public Integer tagidBefore;

	public Integer tagidAfter;

	public Integer ticketId;

	public Integer activeNum;

	public Integer agentId;

	public Integer depId;
	
	public Integer chatConnectTimeOut;
	
	public Integer chatLoginTimeOut;

	public Integer getDepId() {
		return depId;
	}

	public void setDepId(Integer depId) {
		this.depId = depId;
	}

	public Integer getAgentId() {
		return agentId;
	}

	public void setAgentId(Integer agentId) {
		this.agentId = agentId;
	}

	public Integer getActiveNum() {
		return activeNum;
	}

	public void setActiveNum(Integer activeNum) {
		this.activeNum = activeNum;
	}

	public Integer getTicketId() {
		return ticketId;
	}

	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}

	public Integer getTagidBefore() {
		return tagidBefore;
	}

	public void setTagidBefore(Integer tagidBefore) {
		this.tagidBefore = tagidBefore;
	}

	public Integer getTagidAfter() {
		return tagidAfter;
	}

	public void setTagidAfter(Integer tagidAfter) {
		this.tagidAfter = tagidAfter;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getAppkey() {
		return appkey;
	}

	public void setAppkey(String appkey) {
		this.appkey = appkey;
	}

	public String getMasterSecret() {
		return masterSecret;
	}

	public void setMasterSecret(String masterSecret) {
		this.masterSecret = masterSecret;
	}

	public Integer getTokenId() {
		return tokenId;
	}

	public void setTokenId(Integer tokenId) {
		this.tokenId = tokenId;
	}

	public String getCorpid() {
		return corpid;
	}

	public void setCorpid(String corpid) {
		this.corpid = corpid;
	}

	public String getCorpsecret() {
		return corpsecret;
	}

	public void setCorpsecret(String corpsecret) {
		this.corpsecret = corpsecret;
	}

	public Integer getChatConnectTimeOut() {
		return chatConnectTimeOut;
	}

	public void setChatConnectTimeOut(Integer chatConnectTimeOut) {
		this.chatConnectTimeOut = chatConnectTimeOut;
	}

	public Integer getChatLoginTimeOut() {
		return chatLoginTimeOut;
	}

	public void setChatLoginTimeOut(Integer chatLoginTimeOut) {
		this.chatLoginTimeOut = chatLoginTimeOut;
	}

}
