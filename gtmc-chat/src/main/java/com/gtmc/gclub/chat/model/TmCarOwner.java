package com.gtmc.gclub.chat.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "TM_CAR_OWNER") 
public class TmCarOwner {
	@Id
    @Column(name = "USER_ID")
    private Integer userId;

    @Column(name = "USER_NAME")
    private String userName;

    @Column(name = "BIRTHDAY")
    private Date birthday;

    @Column(name = "SEX")
    private Short sex;

    @Column(name = "PHONE")
    private String phone;

    @Column(name = "WX_CODE")
    private String wxCode;

//    @Column(name = "PASSWORD")
//    private String password;

    @Column(name = "NICKNAME")
    private String nickname;

    @Column(name = "IDENTITY_CARD")
    private String identityCard;

    @Column(name = "USER_TYPE")
    private Short userType;

    @Column(name = "ICONS_PHOTO")
    private String iconsPhoto;

    @Column(name = "DRIVER_LICENSE_EXPIRED")
    private Date driverLicenseExpired;

//    @Column(name = "RECOMMENDED_NAME")
//    private String recommendedName;

    @Column(name = "ADDRESS")
    private String address;

    @Column(name = "REGISTER_DATE")
    private Date registerDate;

    @Column(name = "STATUS")
    private Short status;

//    @Column(name = "APIREGISTER")
//    private BigDecimal apiregister;

    @Column(name = "COUNT")
    private Short count;

    @Column(name = "LOG_TIME")
    private Date logTime;
    
    @Column(name = "HINT_STAT")
    private Short hintStat;
    
    @Column(name = "CREATE_DATE")
    private Date createDate;
    
    @Column(name = "CREATE_BY")
    private BigDecimal createBy;
    
    @Column(name = "UPDATE_DATE")
    private Date updateDate;
    
    @Column(name = "UPDATE_BY")
    private BigDecimal updateBy;

//    @Column(name = "GSC_SERVICE_FLAG")
//    private Short gscServiceFlag;

//    @Column(name = "PROVINCE_NAME")
//    private String provinceName;

//    @Column(name = "CITY_NAME")
//    private String cityName;
    
    @Column(name = "CITY")
    private String city;

//    @Column(name = "FROM_APPLICATION")
//    private Short fromApplication;

//    @Column(name = "APP_ACTIVE_FLAG")
//    private Short appActiveFlag;

//    @Column(name = "CLUB_DEAL_FLAG")
//    private Short clubDealFlag;
//
//    @Column(name = "ICLUB_ACTIVE_FLAG")
//    private Short iclubActiveFlag;
//
    @Column(name = "JMESSAGE_CODE")
    private String jmessageCode;
    
    @Column(name = "PROVINCE")
    private String province;
    
    @Column(name = "RECOMMENDED_NAME")
    private String recommendedName;
    
    @Column(name = "MARITAL_STATUS")
    private BigDecimal maritalStatus;
    
    @Column(name = "MARRIAGE")
    private BigDecimal marriage;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Short getSex() {
		return sex;
	}

	public void setSex(Short sex) {
		this.sex = sex;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getWxCode() {
		return wxCode;
	}

	public void setWxCode(String wxCode) {
		this.wxCode = wxCode;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getIdentityCard() {
		return identityCard;
	}

	public void setIdentityCard(String identityCard) {
		this.identityCard = identityCard;
	}

	public Short getUserType() {
		return userType;
	}

	public void setUserType(Short userType) {
		this.userType = userType;
	}

	public String getIconsPhoto() {
		return iconsPhoto;
	}

	public void setIconsPhoto(String iconsPhoto) {
		this.iconsPhoto = iconsPhoto;
	}

	public Date getDriverLicenseExpired() {
		return driverLicenseExpired;
	}

	public void setDriverLicenseExpired(Date driverLicenseExpired) {
		this.driverLicenseExpired = driverLicenseExpired;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public Short getStatus() {
		return status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}

	public Short getCount() {
		return count;
	}

	public void setCount(Short count) {
		this.count = count;
	}

	public Date getLogTime() {
		return logTime;
	}

	public void setLogTime(Date logTime) {
		this.logTime = logTime;
	}

	public Short getHintStat() {
		return hintStat;
	}

	public void setHintStat(Short hintStat) {
		this.hintStat = hintStat;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public BigDecimal getCreateBy() {
		return createBy;
	}

	public void setCreateBy(BigDecimal createBy) {
		this.createBy = createBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public BigDecimal getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(BigDecimal updateBy) {
		this.updateBy = updateBy;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getRecommendedName() {
		return recommendedName;
	}

	public void setRecommendedName(String recommendedName) {
		this.recommendedName = recommendedName;
	}

	public BigDecimal getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(BigDecimal maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public BigDecimal getMarriage() {
		return marriage;
	}

	public void setMarriage(BigDecimal marriage) {
		this.marriage = marriage;
	}

	public String getJmessageCode() {
		return jmessageCode;
	}

	public void setJmessageCode(String jmessageCode) {
		this.jmessageCode = jmessageCode;
	}

	@Override
	public String toString() {
		return "TmCarOwner [userId=" + userId + ", userName=" + userName + ", birthday=" + birthday + ", sex=" + sex
				+ ", phone=" + phone + ", wxCode=" + wxCode + ", nickname=" + nickname + ", identityCard="
				+ identityCard + ", userType=" + userType + ", iconsPhoto=" + iconsPhoto + ", driverLicenseExpired="
				+ driverLicenseExpired + ", address=" + address + ", registerDate=" + registerDate + ", status="
				+ status + ", count=" + count + ", logTime=" + logTime + ", hintStat=" + hintStat + ", createDate="
				+ createDate + ", createBy=" + createBy + ", updateDate=" + updateDate + ", updateBy=" + updateBy
				+ ", city=" + city + ", jmessageCode=" + jmessageCode + ", province=" + province + ", recommendedName="
				+ recommendedName + ", maritalStatus=" + maritalStatus + ", marriage=" + marriage + "]";
	}

}