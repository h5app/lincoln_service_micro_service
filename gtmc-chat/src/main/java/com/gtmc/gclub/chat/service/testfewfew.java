package com.gtmc.gclub.chat.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

import com.gtmc.gclub.chat.model.TmClassifyTag;
import com.gtmc.gclub.chat.vo.ChatCommonContent;
import com.gtmc.gclub.chat.vo.ClassifyAllContentVo;

public class testfewfew {

	private Map<String,List<TmClassifyTag>> allTagsMap= new HashMap<String,List<TmClassifyTag>>();
	private Map<String,ClassifyAllContentVo> allContentMap= new HashMap<String,ClassifyAllContentVo>();
	private Map<String,ClassifyAllContentVo> tagContentsMap= new HashMap<String,ClassifyAllContentVo>();
	// 递归函数
	private List<ChatCommonContent> loopTagTree(String pid) {
		List<ChatCommonContent> res = new ArrayList<ChatCommonContent>();
		// 获取给定pid下的所有子节点
		List<TmClassifyTag> currentNodes = allTagsMap.get(pid);
		// 取出了此节点就把此节点下的数据清空，防止出现递归死循环
		allTagsMap.put(pid, null);
		if(CollectionUtils.isEmpty(currentNodes)) {
			return res;
		}
		// 组装所有子节点的数据
		for(TmClassifyTag tag : currentNodes) {
			ChatCommonContent currentNode = new ChatCommonContent();
			currentNode.setId(tag.getId().toString());
			currentNode.setContent(allContentMap.get(tag.getId().toString()));
			List<ChatCommonContent> childs = loopTagTree(tag.getId().toString());
			currentNode.setChilds(childs);
			res.add(currentNode);
		}
		return res;
	}
	private void test() {
		// 添加测试数据
		TmClassifyTag t1 = new TmClassifyTag();
		t1.setPid(0);
		t1.setId(100);
		TmClassifyTag t2 = new TmClassifyTag();
		t2.setPid(0);
		t2.setId(101);
		
		TmClassifyTag t3 = new TmClassifyTag();
		t3.setPid(100);
		t3.setId(102);
		TmClassifyTag t4 = new TmClassifyTag();
		t4.setPid(101);
		t4.setId(103);
		List<TmClassifyTag> l1 = new ArrayList<TmClassifyTag>();
		l1.add(t1);
		l1.add(t2);
		// 0：100、101
		allTagsMap.put("0", l1);
		List<TmClassifyTag> l2 = new ArrayList<TmClassifyTag>();
//		l2.add(t3);
		l2.add(t2);
		// 100：102     101
		allTagsMap.put(t1.getId().toString(), l2);
		List<TmClassifyTag> l3 = new ArrayList<TmClassifyTag>();
//		l3.add(t4);
		l3.add(t1);
		// 101：103     100
		allTagsMap.put(t2.getId().toString(), l3);
		
		ClassifyAllContentVo c1 = new ClassifyAllContentVo();
		c1.setPid(102);
		c1.setCcontent("这个是102叶子节点的内容");
		ClassifyAllContentVo c2 = new ClassifyAllContentVo();
		c2.setPid(103);
		c2.setCcontent("这个是103叶子节点的内容");
		allContentMap.put("102", c1);
		allContentMap.put("103", c2);
		List<ChatCommonContent> loopTagTree = loopTagTree("0");
		System.out.println(loopTagTree);
	}
	public void test2() {
		List<TmClassifyTag> allClassifyTagByDealerCode = new ArrayList<TmClassifyTag>();
		List<ClassifyAllContentVo> allClassifyContentByDealerCode = new ArrayList<ClassifyAllContentVo>();
		for(int i=0;i<10;i++) {
			TmClassifyTag tag = new TmClassifyTag();
			tag.setId(i);
			tag.setClassifyName("root"+i);
			tag.setPid(i/2);
			ClassifyAllContentVo content = new ClassifyAllContentVo();
			content.setPid(i);
			content.setCcontent("content"+i);
			allClassifyContentByDealerCode.add(content);
		}
		for(TmClassifyTag tag : allClassifyTagByDealerCode) {
			List<TmClassifyTag> childs = allTagsMap.get(tag.getPid()+"");
			if(CollectionUtils.isEmpty(childs)) {
				childs = new ArrayList<TmClassifyTag>();
				allTagsMap.put(tag.getPid().toString(), childs);
			}
			childs.add(tag);
			// 根据前台要求，就算是节点，也要有content，前台根据childs是否为空，判断是否是叶子节点。
//			ClassifyAllContentVo tagContent = new ClassifyAllContentVo();
//			tagContent.setCcontent(tag.getClassifyName());
//			tagContentsMap.put(tag.getId().toString(), tagContent);
		}
		
		for(ClassifyAllContentVo content : allClassifyContentByDealerCode) {
			allContentMap.put(content.getPid().toString(), content);
		}
		List<ChatCommonContent> res = loopTagTree("0");
		System.out.println(res);
	}
	public static void main(String[] args) {
		testfewfew tt = new testfewfew();
		tt.test();
	}
}
