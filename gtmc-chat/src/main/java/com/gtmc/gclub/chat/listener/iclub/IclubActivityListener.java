package com.gtmc.gclub.chat.listener.iclub;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import com.gtmc.gclub.chat.component.MessagerSender;
import com.gtmc.gclub.chat.constants.MessageConstant;
import com.gtmc.gclub.chat.constants.MessageConstant.MESSAGE_TYPE;
import com.gtmc.gclub.chat.message.MessageProtocol;

/**
 * 活动消息的监听
 * 
 * @author BENJAMIN
 */
@Component
public class IclubActivityListener {
	private static Logger logger = LoggerFactory.getLogger(IclubActivityListener.class);
	
	@Autowired
	private MessagerSender messagerSender;
	
	//@RabbitListener(queues=MessageConstant.ICLUB_ACTIVITY_QUEUE,containerFactory="rabbitListenerContainerFactory")
	public void receiveQueueGClub(@Payload MessageProtocol protocol){
		logger.info("start to listen");
        logger.info("@@@@ ICLUB 的 ACTIVITY 队列 接收到 content is "+protocol.toString());
        logger.info("end to listen " );
        logger.info("" );
		//messagerSender.sendActivity2ExchangesGClub(protocol);
        
	}
}
