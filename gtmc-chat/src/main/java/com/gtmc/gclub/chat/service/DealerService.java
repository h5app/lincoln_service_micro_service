package com.gtmc.gclub.chat.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gtmc.gclub.chat.dao.TmDealerMapper;
import com.gtmc.gclub.chat.model.TmCarOwner;
import com.gtmc.gclub.chat.model.TmDealer;

@Service
@Transactional
public class DealerService {
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private TmDealerMapper tmDealerMapper;
	
	// 
	public TmDealer getOne(TmDealer dealer) {
		return tmDealerMapper.selectOne(dealer);
	}
	
	public int updateDealer(TmDealer dealer) {
		int i = tmDealerMapper.updateByPrimaryKeySelective(dealer);
		return i;
	}
	
	public List<TmDealer> queryDealers(TmDealer dealer){
		return tmDealerMapper.select(dealer);
	}
	
}
