package com.gtmc.gclub.chat.dao;

import com.gtmc.gclub.chat.annotation.MySQLDb;
import com.gtmc.gclub.chat.annotation.OracleDb;

import com.gtmc.gclub.chat.model.TmCarOwner;
import tk.mybatis.mapper.common.Mapper;



//@OracleDb
@MySQLDb
public interface TmCarOwnerMapper extends Mapper<TmCarOwner> {
}