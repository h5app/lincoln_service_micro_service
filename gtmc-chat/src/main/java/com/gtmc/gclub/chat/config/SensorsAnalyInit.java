package com.gtmc.gclub.chat.config;

import java.io.IOException;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import com.gtmc.gclub.chat.util.PropertiesUtil;
import com.sensorsdata.analytics.javasdk.SensorsAnalytics;

@Component
public class SensorsAnalyInit {
	final static String SA_SERVER_URL = PropertiesUtil.getSensorsAnalyInstance()
			.readValue("sensorsAnaly.SA_SERVER_URL");
	final static String isDeBuge = PropertiesUtil.getSensorsAnalyInstance().readValue("sensorsAnaly.isDeBuge");
	final static String isLoggingConsumer = PropertiesUtil.getSensorsAnalyInstance()
			.readValue("sensorsAnaly.isLoggingConsumer");

	final static String LoggingUrl = PropertiesUtil.getSensorsAnalyInstance().readValue("sensorsAnaly.LoggingUrl");
	public static SensorsAnalytics sa;

	public void destroy() {

	}

	@PostConstruct
	public void init() {
		// System.err.println(SA_SERVER_URL + "神策
		// 神策就是神策启动的时候走这里了吗##############################" + isDeBuge);
		if (isDeBuge.equals("true")) {
			sa = new SensorsAnalytics(new SensorsAnalytics.DebugConsumer(SA_SERVER_URL, true));
		} else {
			try {
				sa = new SensorsAnalytics(new SensorsAnalytics.ConcurrentLoggingConsumer(LoggingUrl));
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

	}

}
