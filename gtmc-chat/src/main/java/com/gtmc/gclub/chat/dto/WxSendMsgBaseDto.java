package com.gtmc.gclub.chat.dto;

import com.gtmc.gclub.common.WxMaterialTypeEnum;

/**
 * @author kaihu
 * 发送微信消息时必须的通用参数。当前阶段未完成，后续优化使用
 */
public class WxSendMsgBaseDto {
	private String toUserName;// 	开发者微信号
	private String fromUserName;// 	发送方帐号（一个OpenID）
	private WxMaterialTypeEnum type;// 		消息类型
	private String mediaId;// 	图片消息媒体id，可以调用多媒体文件下载接口拉取数据。/语音消息媒体id，可以调用多媒体文件下载接口拉取数据。
	private String content;
	public String getToUserName() {
		return toUserName;
	}
	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}
	public String getFromUserName() {
		return fromUserName;
	}
	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}
	public WxMaterialTypeEnum getType() {
		return type;
	}
	public void setType(WxMaterialTypeEnum type) {
		this.type = type;
	}
	public String getMediaId() {
		return mediaId;
	}
	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	@Override
	public String toString() {
		return "WxSendMsgBaseDto [toUserName=" + toUserName + ", fromUserName=" + fromUserName + ", type=" + type
				+ ", mediaId=" + mediaId + ", content=" + content + "]";
	}
}
