package com.gtmc.gclub.chat.vo;

import java.util.List;

public class ChangeChatVo {
	private String department;
	private List<TrUserEnterpriseVo> list;
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public List<TrUserEnterpriseVo> getList() {
		return list;
	}
	public void setList(List<TrUserEnterpriseVo> list) {
		this.list = list;
	}
	@Override
	public String toString() {
		return "ChangeChatVo [department=" + department + ", list=" + list + "]";
	}
	
}
