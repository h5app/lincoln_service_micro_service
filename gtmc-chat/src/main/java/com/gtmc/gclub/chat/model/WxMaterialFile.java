package com.gtmc.gclub.chat.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.gtmc.gclub.common.WxMaterialTypeEnum;

/**
 * @author kaihua
 *	保存微信永久素材，作为常见内容中的媒体内容出现
 */
@Table(name = "WX_MATERIAL_FILE")
public class WxMaterialFile {
	
	public static final String APPID_GZ = "GZ";
	public static final String APPID_QY = "QY";
	public static final String STATUS_USEFUL = "1000001";
	public static final String STATUS_NOUSE = "1000002";
	public static final Integer IS_SHOW_PIC_FALSE = 0;
	public static final Integer IS_SHOW_PIC_TRUE = 1;
	
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;
	/**
	 * 微信媒体ID
	 */
	@Column(name = "MEDIA_ID")
	private String mediaId;

	/**
	 * 媒体类型
	 */
	@Column(name = "TYPE")
	private String type;

	/**
	 * 媒体文件微信url
	 */
	@Column(name = "WX_URL")
	private String wxUrl;

	/**
	 * 本地可以访问的fireStore的Url
	 */
	@Column(name = "LOCAL_URL")
	private String localUrl;

	/**
	 * 媒体文件描述
	 */
	@Column(name = "DESCRIPTION")
	private String description;

	/**
	 * 会话编号
	 */
	@Column(name = "TITLE")
	private String title;


	/**
	 * 系统来源（公众号/企业号）
	 */
	@Column(name = "APPID")
	private String appid;
	
	/**
	 * 父媒体ID（适用于新闻）
	 */
	@Column(name = "PMEDIA_ID")
	private String pmediaId;
	/**
	 * 状态 1000001 有效 1000002无效
	 */
	@Column(name = "STATUS")
	private String status;
	
	/**
	 * 创建日期
	 */
	@Column(name = "CREATE_DATE")
	private Date createDate;
	/**
	 * 更新日期
	 */
	@Column(name = "UPDATE_DATE")
	private Date updateDate;
	
	/**
	 * 	图文消息的封面图片素材id（必须是永久mediaID）
	 */
	@Column(name = "THUMB_MEDIA_ID")
	private String thumb_media_id;
	/**
	 * 是否显示封面，0为false，即不显示，1为true，即显示
	 */
	@Column(name = "SHOW_COVER_PIC")
	private Integer show_cover_pic;
	/**
	 * 作者
	 */
	@Column(name = "AUTHOR")
	private String author;
	/**
	 * 图文消息的摘要，仅有单图文消息才有摘要，多图文此处为空
	 */
	@Column(name = "DIGEST")
	private String digest;
	/**
	 * 图文消息的具体内容，支持HTML标签，必须少于2万字符，小于1M，且此处会去除JS
	 */
	@Column(name = "CONTENT")
	private String content;
	/**
	 * 	图文消息的原文地址，即点击“阅读原文”后的URL
	 */
	@Column(name = "CONTENT_SOURCE_URL")
	private String content_source_url;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public void setType(WxMaterialTypeEnum type) {
		this.type = type.name();
	}

	public String getWxUrl() {
		return wxUrl;
	}

	public void setWxUrl(String wxUrl) {
		this.wxUrl = wxUrl;
	}

	public String getLocalUrl() {
		return localUrl;
	}

	public void setLocalUrl(String localUrl) {
		this.localUrl = localUrl;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getPmediaId() {
		return pmediaId;
	}

	public void setPmediaId(String pmediaId) {
		this.pmediaId = pmediaId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getThumb_media_id() {
		return thumb_media_id;
	}

	public void setThumb_media_id(String thumb_media_id) {
		this.thumb_media_id = thumb_media_id;
	}

	public Integer getShow_cover_pic() {
		return show_cover_pic;
	}

	public void setShow_cover_pic(Integer show_cover_pic) {
		this.show_cover_pic = show_cover_pic;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getDigest() {
		return digest;
	}

	public void setDigest(String digest) {
		this.digest = digest;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getContent_source_url() {
		return content_source_url;
	}

	public void setContent_source_url(String content_source_url) {
		this.content_source_url = content_source_url;
	}

	@Override
	public String toString() {
		return "WxMaterialFile [id=" + id + ", mediaId=" + mediaId + ", type=" + type + ", wxUrl=" + wxUrl
				+ ", localUrl=" + localUrl + ", description=" + description + ", title=" + title + ", appid=" + appid
				+ ", pmediaId=" + pmediaId + ", status=" + status + ", createDate=" + createDate + ", updateDate="
				+ updateDate + ", thumb_media_id=" + thumb_media_id + ", show_cover_pic="
				+ show_cover_pic + ", author=" + author + ", digest=" + digest + ", content=" + (content==null ? null : content.length())
				+ ", content_source_url=" + content_source_url + "]";
	}

}