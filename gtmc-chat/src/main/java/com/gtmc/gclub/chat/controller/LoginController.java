package com.gtmc.gclub.chat.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping
public class LoginController extends BaseController{

	@RequestMapping(value="/login",method = RequestMethod.POST)
	public String login(){
		return redirect("/admin/index");
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String getLogin(HttpSession session) {
		return "/admin/login.html";
	}
}
