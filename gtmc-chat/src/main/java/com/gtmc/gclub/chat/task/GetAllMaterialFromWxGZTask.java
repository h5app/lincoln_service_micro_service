package com.gtmc.gclub.chat.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gtmc.gclub.chat.service.WxService;
import com.gtmc.gclub.common.WxMaterialTypeEnum;

/**
 * @author kaihua
 * 定时从微信获取所有的永久素材，更新到数据库中
 */
public class GetAllMaterialFromWxGZTask extends Thread{
	
	private static Logger logger = LoggerFactory.getLogger(GetAllMaterialFromWxGZTask.class);
	
	private WxService wxService;
	
	private WxMaterialTypeEnum type;
	
	public GetAllMaterialFromWxGZTask(WxService wxService,WxMaterialTypeEnum type) {
		this.wxService = wxService;
		this.type = type;
	}
	
	public void run() {
		synchronized (GetAllMaterialFromWxGZTask.class) {
			logger.debug("GetAllMaterialFromWxGZTask同步微信永久素材到本地DB、fileStore开始");
			wxService.syncWxMaterialFileToDbThreadMethod(type);
			logger.debug("GetAllMaterialFromWxGZTask同步微信永久素材到本地DB、fileStore结束");
		}
	}
	
}
