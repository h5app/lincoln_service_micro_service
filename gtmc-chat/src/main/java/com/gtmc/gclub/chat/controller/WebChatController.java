package com.gtmc.gclub.chat.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.gtmc.gclub.chat.bean.Result;
import com.gtmc.gclub.chat.dao.WxUserOwnerMapper;
import com.gtmc.gclub.chat.dto.ChatParam;
import com.gtmc.gclub.chat.model.TtChatLog;
import com.gtmc.gclub.chat.model.WxUserOwner;
import com.gtmc.gclub.chat.service.ChatLogService;
import com.gtmc.gclub.chat.util.EmojiUtil;

import io.swagger.annotations.ApiOperation;

@Controller
@RequestMapping(value = "/chat/web")
public class WebChatController extends BaseController {
	
	@Autowired
	private ChatLogService chatLogService; 
	
	/**
	 * 查询当天聊天的客户列表
	 * @param param
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value="/query/customerList",method=RequestMethod.POST)
	@ResponseBody
	@ApiOperation(value="查询当天聊天的客户列表",notes="查询当天聊天的客户列表")
	public Result<PageInfo<ChatParam>> querySessionList(@RequestBody ChatParam param) throws Exception{
		PageInfo<ChatParam> page = chatLogService.queryCustomerList(param); 
//		for(ChatParam temp:page.getList()) {
//			temp.setUserName(temp.getUserName()==null ? null : EmojiUtil.emojiConverterToAlias(temp.getUserName()));
//		}
		return new Result<PageInfo<ChatParam>>(1,page,null);
	}
	
	/**
	 * 获取同选中客户聊天的客服会话的信息
	 * @param param
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value="/query/chatCustomerList",method=RequestMethod.POST)
	@ResponseBody
	@ApiOperation(value="获取同选中客户聊天的客服会话的信息",notes="")
	public Result<PageInfo<ChatParam>> queryChatCustomerList(@RequestBody ChatParam param) throws Exception{
		PageInfo<ChatParam> page = chatLogService.queryChatCustomerList(param); 
//		for(ChatParam temp:page.getList()) {
//			temp.setUserName(temp.getUserName()==null ? null : EmojiUtil.emojiConverterToAlias(temp.getUserName()));
//		}
		return new Result<PageInfo<ChatParam>>(1,page,null);
	}
	
	/**
	 * 导出选中用户的历史聊天记录
	 * @param param
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value="/export/chatCustomerHistoryList",method=RequestMethod.POST)
	@ResponseBody
	@ApiOperation(value="导出选中用户的历史聊天记录",notes="Map中数据格式为：FILE_URL、FILE_NAME")
	public Result<List<Map<String,Object>>> chatCustomerHistoryFile(@RequestBody ChatParam param) throws Exception{
		List<Map<String,Object>> page = chatLogService.chatCustomerHistoryFile(param);
		return new Result<List<Map<String,Object>>>(1,page,null);
	}
	
}
