package com.gtmc.gclub.chat.component;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gtmc.gclub.chat.constants.MessageConstant;
import com.gtmc.gclub.chat.message.MessageProtocol;

@Component
public class MessagerSender {
	
	@Autowired
	private RabbitTemplate rabbitTemplate;
	
	
	/**
	 * 发布活动到队列
	 * 
	 * @param msg
	 */
	public void sendActivity2ExchangesGClub(MessageProtocol msg){
		this.rabbitTemplate.convertAndSend(MessageConstant.GTMC_EXCHANGES_TOPIC_GCLUB,MessageConstant.MESSAGE_TYPE.ACTIVITY.toString(), msg);
	}
	
	/**
	 * 发布用户信息到队列
	 * 
	 * @param msg
	 */
	public void sendUSER2ExchangesGClub(MessageProtocol msg){
		this.rabbitTemplate.convertAndSend(MessageConstant.GTMC_EXCHANGES_TOPIC_GCLUB, MessageConstant.MESSAGE_TYPE.USER.toString(), msg);
	}
	
	public void sendActivity2ExchangesWCHAT(MessageProtocol msg){
		this.rabbitTemplate.convertAndSend(MessageConstant.W_ACTIVITY_QUEUE, msg);
	}
	
	
	public void sendTest(MessageProtocol msg){
		this.rabbitTemplate.convertAndSend("WECHAT.TEST", msg);
	}

}
