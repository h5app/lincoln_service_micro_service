package com.gtmc.gclub.chat.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gtmc.gclub.chat.dao.TmCarOwnerMapper;
import com.gtmc.gclub.chat.dao.TmPotentialUserMapper;
import com.gtmc.gclub.chat.model.TmCarOwner;
import com.gtmc.gclub.chat.model.TmPotentialUser;

@Service
@Transactional
public class OwnerService {
	
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private TmCarOwnerMapper tmCarOwnerMapper;
	
	@Autowired
	private TmPotentialUserMapper tmPotentialUserMapper;
	
	public TmCarOwner getOne(TmCarOwner owner) {
		return tmCarOwnerMapper.selectOne(owner);
	}
	
	public TmPotentialUser getOne(TmPotentialUser owner) {
		return tmPotentialUserMapper.selectOne(owner);
	}
	
	public int updateOwner(TmCarOwner owner) {
		int i = tmCarOwnerMapper.updateByPrimaryKeySelective(owner);
		return i;
	}

	public List<TmPotentialUser> queryOwnerByIds(List<String> resIds) {
		return tmPotentialUserMapper.queryOwnerByIds(resIds);
	}

}
