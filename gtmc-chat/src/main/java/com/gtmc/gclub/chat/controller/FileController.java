package com.gtmc.gclub.chat.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.gtmc.gclub.chat.bean.Result;

/**
 * 文件处理
 * 
 * @author qianjs163@163.com
 *
 */
@Controller
@RequestMapping(value = "/file")
public class FileController {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	@ResponseBody
	public Result<String> handleFileUpload(@RequestParam("file") MultipartFile file) {
		logger.info("文件上传开始");
		if (!file.isEmpty()) {

		} else {
			return new Result<String>(0, null, "文件上传失败，文件为空");
		}
		return null;
	}
}
