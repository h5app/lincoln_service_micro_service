package com.gtmc.gclub.chat.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import com.gtmc.gclub.common.WxMaterialTypeEnum;

/**
 * @author kaihua
 *
 *	聊天记录完整表：包含只能回复的聊天记录
 */
@Table(name = "TT_CHAT_LOG") 
public class TtChatLog {
	@Id
    @Column(name = "ID")
	private Integer id;
	
	/**
	 * 潜客ID
	 */
	@Column(name = "POTENTIAL_USER_ID")
	private String potentialUserId;
	/**
	 * 聊天内容
	 */
	@Column(name = "CONTENT")
	private String content;
	/**
	 * 聊天内容消息类型WxMaterialTypeEnum
	 */
	@Column(name = "CONTENT_TYPE")
	private String contentType;
	/**
	 * 消息发送人类型（1：用户；2：智能回复；3：客服）
	 */
	@Column(name = "SEND_TYPE")
	private Integer sendType;
	/**
	 * 客服ID
	 */
	@Column(name = "SERVICE_ID")
	private Integer serviceId;
	/**
	 * 聊天记录时间
	 */
	@Column(name = "CREATE_DATE")
	private Date createDate;
	/**
	 * 创建人ID
	 */
	@Column(name = "CREATE_BY")
	private Integer createBy;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPotentialUserId() {
		return potentialUserId;
	}
	public void setPotentialUserId(String potentialUserId) {
		this.potentialUserId = potentialUserId;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public void setContentType(WxMaterialTypeEnum contentType) {
		this.contentType = contentType.name();
	}
	public Integer getSendType() {
		return sendType;
	}
	public void setSendType(Integer sendType) {
		this.sendType = sendType;
	}
	public Integer getServiceId() {
		return serviceId;
	}
	public void setServiceId(Integer serviceId) {
		this.serviceId = serviceId;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Integer getCreateBy() {
		return createBy;
	}
	public void setCreateBy(Integer createBy) {
		this.createBy = createBy;
	}
	@Override
	public String toString() {
		return "TtChatLog [id=" + id + ", potentialUserId=" + potentialUserId + ", content=" + content
				+ ", contentType=" + contentType + ", sendType=" + sendType + ", serviceId=" + serviceId
				+ ", createDate=" + createDate + ", createBy=" + createBy + "]";
	}
}
