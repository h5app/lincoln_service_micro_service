package com.gtmc.gclub.chat.config;

import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.gtmc.gclub.error.GateWayErrorController;

@Configuration
@AutoConfigureBefore(WebMvcAutoConfiguration.class)
public class ErrorConfig {

	@Bean
	public GateWayErrorController basicErrorController(ErrorAttributes errorAttributes) {
		return new GateWayErrorController(errorAttributes);
	}
}
