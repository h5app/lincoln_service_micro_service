package com.gtmc.gclub.chat.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gtmc.gclub.chat.bean.BizException;
import com.gtmc.gclub.chat.dao.ExTmChatDialogueMapper;
import com.gtmc.gclub.chat.dao.ExTmClassifyContentMapper;
import com.gtmc.gclub.chat.dao.ExTmClassifyTagMapper;
import com.gtmc.gclub.chat.dto.BaseQueryDto;
import com.gtmc.gclub.chat.dto.ChatClassifyContentDto;
import com.gtmc.gclub.chat.dto.ChatClassifyQueryDto;
import com.gtmc.gclub.chat.dto.ChatDialogueDto;
import com.gtmc.gclub.chat.model.TmChatDialogue;
import com.gtmc.gclub.chat.model.TmChatDialogueExample;
import com.gtmc.gclub.chat.model.TmClassifyContent;
import com.gtmc.gclub.chat.model.TmClassifyTag;
import com.gtmc.gclub.chat.model.TmClassifyTagExample;
import com.gtmc.gclub.chat.util.CommonUtils;
import com.gtmc.gclub.chat.vo.ChatCommonContent;
import com.gtmc.gclub.chat.vo.ClassifyAllContentVo;
import com.gtmc.gclub.chat.vo.ClassifyContentPageVo;
import com.gtmc.gclub.chat.vo.ClassifyTagFirstVo;
import com.gtmc.gclub.chat.vo.ClassifyTagSecondVo;
import com.gtmc.gclub.chat.vo.TmChatDialoguePageVo;
import com.gtmc.gclub.chat.vo.TmClassifyContentVo;
import com.gtmc.gclub.common.WxMaterialTypeEnum;

@Service
public class AppApiService {

	@Autowired
	private ExTmChatDialogueMapper exTmChatDialogueMapper;

	@Autowired
	private ExTmClassifyContentMapper exTmClassifyContentMapper;

	@Autowired
	private ExTmClassifyTagMapper exTmClassifyTagMapper;

	@Transactional
	public Integer addBatchAppChatDialogue(ChatDialogueDto dto) {
		TmChatDialogue tm = new TmChatDialogue();
		tm.setContent(dto.getDataMessage().get(0));
		tm.setCreateDate(dto.getCreateDate());
		tm.setDealerCode(dto.getDealerCode());
		int i = exTmChatDialogueMapper.insertSelective(tm);
		return i;
	}

	@Transactional
	public Integer addBatchAppChatDialogues(ChatDialogueDto dto) {
		int i = exTmChatDialogueMapper.insertBatchs(dto);
		return i;
	}

	@Transactional
	public Integer addAppChatContent(TmClassifyContent dto) {
		int i = exTmClassifyContentMapper.insertSelective(dto);
		return i;
	}

	@Transactional
	public int addBatchAppChatContent(List<ChatClassifyContentDto> dto, String dealerCode) {
		TmClassifyTagExample ex = new TmClassifyTagExample();
		ex.createCriteria().andDealerCodeEqualTo(dealerCode).andPidEqualTo(0);
		List<TmClassifyTag> list = exTmClassifyTagMapper.selectByExample(ex);
		// if (list.size() >= 4) {
		// throw new BizException("一级分类已有四个,导入失败");
		// }
		List<ChatClassifyContentDto> insert = new ArrayList<ChatClassifyContentDto>();
		List<ChatClassifyContentDto> update = new ArrayList<ChatClassifyContentDto>();
		ChatClassifyContentDto dto1 = null;
		TmClassifyTag tm1 = null;
		for (int i = 0; i < dto.size(); i++) {
			dto1 = dto.get(i);
			boolean flag = true;
			for (int j = 0; j < list.size(); j++) {
				tm1 = list.get(j);
				if (dto1.getClassifyCode().equals(tm1.getClassifyCode())) {
					flag = false;
					// 更新
					dto1.setId(tm1.getId());
					dto1.setUpdateTime(new Date());
					update.add(dto1);
					break;
				}
			}
			if (!flag) {
				continue;
			}
			// 插入
			insert.add(dto1);
		}
		// if ((list.size() + insert.size()) > 4) {
		// throw new BizException("一级分类超过四个,导入失败");
		// }
		TmClassifyTag tm = null;
		ChatClassifyContentDto dt1 = null;
		for (int i = 0; i < insert.size(); i++) {
			tm = new TmClassifyTag();
			dt1 = insert.get(i);
			tm.setClassifyCode(dt1.getClassifyCode());
			tm.setClassifyName(dt1.getClassifyName());
			tm.setCreateDate(new Date());
			tm.setDealerCode(dealerCode);
			tm.setPid(0);
			tm.setUpdateTime(new Date());
			Integer id = exTmClassifyTagMapper.insertBase(tm);
			// 批量插入
			List<ChatClassifyContentDto> l = dt1.getList();
			// List<TmClassifyTag> in1 = new ArrayList<TmClassifyTag>();
			// List<TmClassifyContent> in2 = new ArrayList<TmClassifyContent>();
			TmClassifyTag tm2 = null;
			ChatClassifyContentDto dt2 = null;
			for (int j = 0; j < l.size(); j++) {
				tm2 = new TmClassifyTag();
				dt2 = l.get(j);
				tm2.setClassifyCode(dt2.getClassifyCode());
				tm2.setClassifyName(dt2.getClassifyName());
				tm2.setCreateDate(new Date());
				tm2.setDealerCode(dealerCode);
				tm2.setPid(tm.getId());
				tm2.setUpdateTime(new Date());
				Integer id2 = exTmClassifyTagMapper.insertBase(tm2);
				List<String> c = dt2.getContent();
				List<TmClassifyContent> ts = new ArrayList<TmClassifyContent>();
				for (int k = 0; k < c.size(); k++) {
					TmClassifyContent t = new TmClassifyContent();
					t.setContent(c.get(k));
					t.setPid(tm2.getId());
					t.setCreateDate(new Date());
					ts.add(t);
				}
				exTmClassifyContentMapper.insertBatchs(ts);
			}

		}
		TmClassifyTag tm3 = null;
		for (int i = 0; i < update.size(); i++) {
			// 批量插入
			ChatClassifyContentDto tag = update.get(i);
			List<ChatClassifyContentDto> l = update.get(i).getList();
			// List<TmClassifyContent> in = new ArrayList<TmClassifyContent>();
			for (int j = 0; j < l.size(); j++) {
				tm3 = new TmClassifyTag();
				tm3.setClassifyCode(l.get(j).getClassifyCode());
				tm3.setDealerCode(dealerCode);
				tm3.setPid(tag.getId());
				List<Map<String, Object>> ids = exTmClassifyTagMapper.getTagCodeId(tm3);
				tm3.setClassifyName(l.get(j).getClassifyName());
				tm3.setCreateDate(new Date());
				tm3.setUpdateTime(new Date());
				if (ids.size() > 0) {
					// 二级标签已存在
					long id = (Long) ids.get(0).get("id");
					List<String> c = l.get(j).getContent();
					List<TmClassifyContent> ts = new ArrayList<TmClassifyContent>();
					for (int k = 0; k < c.size(); k++) {
						TmClassifyContent t = new TmClassifyContent();
						t.setContent(c.get(k));
						t.setPid((int) id);
						t.setCreateDate(new Date());
						ts.add(t);
					}
					exTmClassifyContentMapper.insertBatchs(ts);
				} else {
					// 新增
					Integer id2 = exTmClassifyTagMapper.insertBase(tm3);
					List<String> c = l.get(j).getContent();
					List<TmClassifyContent> ts = new ArrayList<TmClassifyContent>();
					for (int k = 0; k < c.size(); k++) {
						TmClassifyContent t = new TmClassifyContent();
						t.setContent(c.get(k));
						t.setPid(tm3.getId());
						t.setCreateDate(new Date());
						ts.add(t);
					}
					exTmClassifyContentMapper.insertBatchs(ts);
				}
			}
			// exTmClassifyContentMapper.insertBatchs(in);
		}
		return 1;
	}

	@Transactional
	public int updateAppChatContent(ClassifyContentPageVo dto) {
		TmClassifyContent tm = new TmClassifyContent();
		tm.setContent(dto.getContent());
		tm.setId(dto.getId());
		tm.setUpdateTime(new Date());
		int k = exTmClassifyContentMapper.updateByPrimaryKeySelective(tm);
		return k;
	}

	@Transactional(readOnly = true)
	public List<TmClassifyContentVo> getAppChatTag(String dealerCode) {
		List<TmClassifyContentVo> list = exTmClassifyTagMapper.getAppChatTag(dealerCode);
		return list;
	}

	@Transactional(readOnly = true)
	public PageInfo<TmChatDialoguePageVo> getAppChatDialogue(BaseQueryDto dto) {
		PageHelper.startPage(dto.getPageNum(), dto.getPageSize(), true, false);
		List<TmChatDialoguePageVo> list = exTmChatDialogueMapper.getAppChatDialogue(dto.getDealerCode());
		PageInfo<TmChatDialoguePageVo> page = new PageInfo<TmChatDialoguePageVo>(list);
		return page;
	}

	@Transactional(readOnly = true)
	public PageInfo<ClassifyContentPageVo> getAppChatContent(com.gtmc.gclub.chat.dto.ChatClassifyQueryDto dto) {
		PageHelper.startPage(dto.getPageNum(), dto.getPageSize(), true, false);
		List<ClassifyContentPageVo> list = exTmClassifyContentMapper.getAppChatContent(dto);
		PageInfo<ClassifyContentPageVo> page = new PageInfo<ClassifyContentPageVo>(list);
		return page;
	}

	@Transactional
	public int updateAppChatDialogue(TmChatDialogue tm) {
		tm.setUpdateDate(new Date());
		int i = exTmChatDialogueMapper.updateByPrimaryKeySelective(tm);
		return i;
	}

	@Transactional
	public int deleteAppChatDialogue(Integer id) {
		int i = exTmChatDialogueMapper.deleteByPrimaryKey(id);
		return i;
	}

	@Transactional
	public int updateAppChatTag(ClassifyContentPageVo dto) {
		TmClassifyTag tm = new TmClassifyTag();
		tm.setDealerCode(dto.getDealerCode());
		if (dto.getId2() == null) {
			// 一级标签
			tm.setPid(dto.getId1());
			int count = exTmClassifyTagMapper.selectCount(tm);
			if (count > 0) {
				throw new BizException("子目录含有内容,无法更新");
			}
			// TmClassifyTagExample ex = new TmClassifyTagExample();
			// ex.createCriteria().andClassifyCodeEqualTo(dto.getClassifyCodeFirst())
			// .andDealerCodeEqualTo(dto.getDealerCode()).andPidEqualTo(0);
			// List<TmClassifyTag> list =
			// exTmClassifyTagMapper.selectByExample(ex);
			// TmClassifyTag tm2 = list.get(0);
			// if (tm2.getId() != dto.getId1()) {
			// throw new BizException("编号'" + dto.getClassifyCodeFirst() +
			// "'已存在");
			// }
			TmClassifyTag tag = new TmClassifyTag();
			tag.setDealerCode(dto.getDealerCode());
			tag.setClassifyCode(dto.getClassifyCodeFirst());
			List<Map<String, Object>> k = exTmClassifyTagMapper.getTagCodeId(tag);
			if (k.size() > 0) {
				Map<String, Object> map = k.get(0);
				long id = (Long) map.get("id");
				if ((int) id != dto.getId1()) {
					throw new BizException("编号'" + dto.getClassifyCodeFirst() + "'已存在");
				}
			}
			tm.setPid(null);
			tm.setId(dto.getId1());
			tm.setClassifyCode(dto.getClassifyCodeFirst());
			tm.setClassifyName(dto.getClassifyNameFirst());
			tm.setUpdateTime(new Date());
		} else {
			// 二级
			TmClassifyContent tc = new TmClassifyContent();
			tc.setPid(dto.getId2());
			// tm.setClassifyCode(dto.getClassifyCodeSecond());
			int count = exTmClassifyContentMapper.selectCount(tc);
			if (count > 0) {
				throw new BizException("子目录含有内容,无法更新");
			}
			// TmClassifyTagExample ex = new TmClassifyTagExample();
			// ex.createCriteria().andClassifyCodeEqualTo(dto.getClassifyCodeFirst())
			// .andDealerCodeEqualTo(dto.getDealerCode()).andPidEqualTo(dto.getId1());
			// List<TmClassifyTag> list =
			// exTmClassifyTagMapper.selectByExample(ex);
			// TmClassifyTag tm2 = list.get(0);
			// if (tm2.getId() != dto.getId2()) {
			// throw new BizException("编号'" + dto.getClassifyCodeFirst() +
			// "'已存在");
			// }
			TmClassifyTag tag = new TmClassifyTag();
			tag.setDealerCode(dto.getDealerCode());
			tag.setClassifyCode(dto.getClassifyCodeSecond());
			List<Map<String, Object>> k = exTmClassifyTagMapper.getTagCodeId(tag);
			if (k.size() > 0) {
				Map<String, Object> map = k.get(0);
				long id = (Long) map.get("id");
				if ((int) id != dto.getId2()) {
					throw new BizException("编号'" + dto.getClassifyCodeSecond() + "'已存在");
				}
			}
			tm.setPid(null);
			tm.setId(dto.getId2());
			tm.setClassifyCode(dto.getClassifyCodeSecond());
			tm.setClassifyName(dto.getClassifyNameSecond());
			tm.setUpdateTime(new Date());
		}
		int k = exTmClassifyTagMapper.updateByPrimaryKeySelective(tm);
		return k;
	}

	@Transactional(readOnly = true)
	public List<TmChatDialogue> getWxChatDialogue(String dealerCode) {
		TmChatDialogueExample ex = new TmChatDialogueExample();
		ex.createCriteria().andDealerCodeEqualTo(dealerCode);
		List<TmChatDialogue> list = exTmChatDialogueMapper.selectByExample(ex);
		return list;

	}

	@Transactional
	public int updateDialogueTimes(Integer id) {
		int i = exTmChatDialogueMapper.updateDialogueTimes(id);
		return i;
	}

	@Transactional(readOnly = true)
	public List<ClassifyContentPageVo> getWxChatContent(String dealerCode) {
		ChatClassifyQueryDto dto = new ChatClassifyQueryDto();
		dto.setDealerCode(dealerCode);
		List<ClassifyContentPageVo> list = exTmClassifyContentMapper.getAppChatContent(dto);
		return list;
	}

	@Transactional(readOnly = true)
	public List<ChatCommonContent> getWxChatContent2(String dealerCode) throws Exception {
		ChatClassifyQueryDto dto = new ChatClassifyQueryDto();
		dto.setDealerCode(dealerCode);
//		List<ClassifyContentPageVo> list = exTmClassifyContentMapper.getWxChatContent(dto);
//		return list;
		// 重构：使用递归做成无限树
		// 直接拿出所有数据
		List<TmClassifyTag> allClassifyTagByDealerCode = exTmClassifyTagMapper.getAllClassifyTagByDealerCode(dealerCode);
		List<ClassifyAllContentVo> allClassifyContentByDealerCode = exTmClassifyContentMapper.getAllClassifyContentByDealerCode(dealerCode);
		// 整理所有的tag
		for(TmClassifyTag tag : allClassifyTagByDealerCode) {
			List<TmClassifyTag> childs = allTagsMap.get(tag.getPid()+"");
			if(CollectionUtils.isEmpty(childs)) {
				childs = new ArrayList<TmClassifyTag>();
				allTagsMap.put(tag.getPid().toString(), childs);
			}
			childs.add(tag);
			// 根据前台要求，就算是节点，也要有content，前台根据childs是否为空，判断是否是叶子节点。
			ClassifyAllContentVo tagContent = new ClassifyAllContentVo();
			tagContent.setCcontent(tag.getClassifyName());
			tagContentsMap.put(tag.getId().toString(), tagContent);
		}
		// 把内容也加进去
		for(ClassifyAllContentVo contentTag:allClassifyContentByDealerCode) {
			List<TmClassifyTag> childs = allTagsMap.get(contentTag.getPid()+"");
			if(CollectionUtils.isEmpty(childs)) {
				childs = new ArrayList<TmClassifyTag>();
				allTagsMap.put(contentTag.getPid().toString(), childs);
			}
			TmClassifyTag tag = new TmClassifyTag();
			tag.setPid(contentTag.getPid());
			tag.setId(contentTag.getId());
			tag.setClassifyName(contentTag.getCcontent());
			childs.add(tag);
			tagContentsMap.put(contentTag.getId().toString(), contentTag);
		}
		
		for(ClassifyAllContentVo content : allClassifyContentByDealerCode) {
			// 如果是news，则设置des为html代码内容。未完成，此处仅为临时处理，以后所有的图文都由客服自己处理！！！
			if(WxMaterialTypeEnum.news.name().equals(content.getType()) ||
					WxMaterialTypeEnum.mpnews.name().equals(content.getType())) {
				content.setDescription(CommonUtils.getUrlToContent(content.getLocalUrl()));
				content.setLocalUrl(null);
			}
			allContentMap.put(content.getId().toString(), content);
		}
		List<ChatCommonContent> res = loopTagTree("0");
		return res;
	}
	private Map<String,List<TmClassifyTag>> allTagsMap= new HashMap<String,List<TmClassifyTag>>();
	private Map<String,ClassifyAllContentVo> allContentMap= new HashMap<String,ClassifyAllContentVo>();
	private Map<String,ClassifyAllContentVo> tagContentsMap= new HashMap<String,ClassifyAllContentVo>();
	// 递归函数
	private List<ChatCommonContent> loopTagTree(String pid) {
		List<ChatCommonContent> res = new ArrayList<ChatCommonContent>();
		// 获取给定pid下的所有子节点
		List<TmClassifyTag> currentNodes = allTagsMap.get(pid);
		// 取出了此节点就把此节点下的数据清空，防止出现递归死循环
		allTagsMap.put(pid, null);
		if(CollectionUtils.isEmpty(currentNodes)) {
			return res;
		}
		// 组装所有子节点的数据
		for(TmClassifyTag tag : currentNodes) {
			ChatCommonContent currentNode = new ChatCommonContent();
			currentNode.setId(tag.getId().toString());
			// 如果是树节点取得树枝节点的值，如果是叶子节点，取得叶子节点的值
			if(allContentMap.get(tag.getId().toString())==null) {
				currentNode.setContent(tagContentsMap.get(tag.getId().toString()));
			}else {
				currentNode.setContent(allContentMap.get(tag.getId().toString()));
			}
			List<ChatCommonContent> childs = loopTagTree(tag.getId().toString());
			currentNode.setChilds(childs);
			res.add(currentNode);
		}
		return res;
	}

	@Transactional
	public int deleteAppChatContent(ClassifyContentPageVo dto) {

		TmClassifyContent tm = new TmClassifyContent();
		tm.setId(dto.getId());
		int k = exTmClassifyContentMapper.delete(tm);
		return k;
		// tm.setPid(dto.getId1());
		// tm.setClassifyCode(dto.getClassifyCodeSecond());
		// List<TmClassifyContent> list = exTmClassifyContentMapper.select(tm);
		// tm.setPid(null);
		// tm.setClassifyCode(null);
		// if (list.size() > 1) {
		// // 直接删除
		// tm.setId(dto.getId());
		// k = exTmClassifyContentMapper.delete(tm);
		// } else {
		// TmClassifyContent t = list.get(0);
		// String content = t.getContent();
		// if ("".equals(content)) {
		// //
		// throw new BizException("已删除");
		// } else {
		// //
		// tm.setId(dto.getId());
		// tm.setContent("");
		// tm.setUpdateTime(new Date());
		// k = exTmClassifyContentMapper.updateByPrimaryKeySelective(tm);
		// }
		// }
		// return k;
	}

	@Transactional
	public int deleteAppChatTag(ClassifyContentPageVo dto) {
		TmClassifyTag tm = new TmClassifyTag();
		if (dto.getId2() == null) {
			// 一级标签
			tm.setPid(dto.getId1());
			int count = exTmClassifyTagMapper.selectCount(tm);
			if (count > 0) {
				throw new BizException("子目录含有内容,无法删除");
			}
			tm.setPid(null);
			tm.setId(dto.getId1());

		} else {
			// 二级
			// tm.setClassifyCode(dto.getClassifyCodeSecond());
			// int count = exTmClassifyContentMapper.selectCount(tm);
			// if (count > 1) {
			// throw new BizException("子目录含有内容,无法删除");
			// }
			// tm.setPid(null);
			// tm.setClassifyCode(null);
			// tm.setId(dto.getId());
			TmClassifyContent tc = new TmClassifyContent();
			tc.setPid(dto.getId2());
			int css = exTmClassifyContentMapper.delete(tc);
			tm.setId(dto.getId2());
		}
		int k = exTmClassifyTagMapper.delete(tm);
		return k;
	}

	@Transactional(readOnly = true)
	public List<ClassifyContentPageVo> getAppChatContentExport(com.gtmc.gclub.chat.dto.ChatClassifyQueryDto dto) {
		List<ClassifyContentPageVo> list = exTmClassifyContentMapper.getAppChatContentExport(dto);
		return list;
	}

	@Transactional(readOnly = true)
	public List<TmChatDialoguePageVo> getDialogueExport(String dealerCode) {
		List<TmChatDialoguePageVo> list = exTmChatDialogueMapper.getAppChatDialogueExport(dealerCode);
		return list;
	}

	@Transactional(readOnly = true)
	public PageInfo<ClassifyContentPageVo> getAppChatClassify(com.gtmc.gclub.chat.dto.ChatClassifyQueryDto dto,
			String classify) {
		if ("1".equals(classify)) {
			// 一级
			PageHelper.startPage(dto.getPageNum(), dto.getPageSize(), true, false);
			List<ClassifyContentPageVo> list = exTmClassifyTagMapper.getAppChatTagFirst(dto);
			PageInfo<ClassifyContentPageVo> page = new PageInfo<ClassifyContentPageVo>(list);
			//page.setTotal(list.size());
			return page;
		} else if ("2".equals(classify)) {
			// 二级
			PageHelper.startPage(dto.getPageNum(), dto.getPageSize(), true, false);
			List<ClassifyContentPageVo> list = exTmClassifyTagMapper.getAppChatTagSecond(dto);
			PageInfo<ClassifyContentPageVo> page = new PageInfo<ClassifyContentPageVo>(list);
			return page;
		} else {
			throw new BizException("classify is error");
		}

	}

	@Transactional(readOnly = true)
	public List<ClassifyTagSecondVo> getClassifySecondExport(com.gtmc.gclub.chat.dto.ChatClassifyQueryDto dealerCode,
			String classify) {
		if ("2".equals(classify)) {
			// 二级
			List<ClassifyTagSecondVo> list = exTmClassifyTagMapper.getAppChatTagSecondExport(dealerCode);
			return list;
		} else {
			throw new BizException("classify is error");
		}
	}

	@Transactional(readOnly = true)
	public List<ClassifyTagFirstVo> getClassifyFirstExport(com.gtmc.gclub.chat.dto.ChatClassifyQueryDto dealerCode,
			String classify) {
		if ("1".equals(classify)) {
			// 一级
			List<ClassifyTagFirstVo> list = exTmClassifyTagMapper.getAppChatTagFirstExport(dealerCode);
			return list;
		} else {
			throw new BizException("classify is error");
		}
	}

	@Transactional
	public Integer addAppChatTag(TmClassifyTag dto) {
		// TmClassifyTagExample ex = new TmClassifyTagExample();
		if ((int) dto.getPid() == 0) {
			// 插入一级
			// ex.createCriteria().andDealerCodeEqualTo(dto.getDealerCode()).andPidEqualTo(dto.getPid());
			// List<TmClassifyTag> list =
			// exTmClassifyTagMapper.selectByExample(ex);
			TmClassifyTag tag = new TmClassifyTag();
			tag.setDealerCode(dto.getDealerCode());
			tag.setPid(0);
			int c = exTmClassifyTagMapper.getTagCodeCount(tag);
			// if (c >= 4) {
			// throw new BizException("一级分类已有四个");
			// }
			// for (int i = 0; i < list.size(); i++) {
			// String code = list.get(i).getClassifyCode();
			// if (code.equals(dto.getClassifyCode())) {
			// throw new BizException("编号'" + code + "'已存在");
			// }
			// }
			tag.setPid(null);
			tag.setClassifyCode(dto.getClassifyCode());
			int k = exTmClassifyTagMapper.getTagCodeCount(tag);
			if (k > 0) {
				throw new BizException("编号'" + dto.getClassifyCode() + "'已存在");
			}
		} else {
			// 插入二级
			// ex.createCriteria().andDealerCodeEqualTo(dto.getDealerCode()).andPidEqualTo(dto.getPid())
			// .andClassifyCodeEqualTo(dto.getClassifyCode());
			// List<TmClassifyTag> list =
			// exTmClassifyTagMapper.selectByExample(ex);
			// if (list.size() > 0) {
			// TmClassifyTag seconds = list.get(0);
			// throw new BizException("编号'" + dto.getClassifyCode() + "'已存在");
			//
			// }
			TmClassifyTag tag = new TmClassifyTag();
			tag.setClassifyCode(dto.getClassifyCode());
			tag.setDealerCode(dto.getDealerCode());
			int k = exTmClassifyTagMapper.getTagCodeCount(tag);
			if (k > 0) {
				throw new BizException("编号'" + dto.getClassifyCode() + "'已存在");
			}

		}
		int i = exTmClassifyTagMapper.insertBase(dto);
		return dto.getId();
	}
}
