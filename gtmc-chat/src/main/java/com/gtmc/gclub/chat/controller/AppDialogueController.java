package com.gtmc.gclub.chat.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.Workbook;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.enmus.ExcelType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.gtmc.gclub.chat.bean.MyException;
import com.gtmc.gclub.chat.bean.Result;
import com.gtmc.gclub.chat.dto.BaseQueryDto;
import com.gtmc.gclub.chat.dto.ChatDialogueDto;
import com.gtmc.gclub.chat.model.TmChatDialogue;
import com.gtmc.gclub.chat.service.AppApiService;
import com.gtmc.gclub.chat.util.DateUtil;
import com.gtmc.gclub.chat.vo.TmChatDialoguePageVo;
import com.infoservice.filestore.FileStore;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/chat/app/dialogue")
public class AppDialogueController {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private AppApiService appApiService;

	@ApiOperation(value = "分页-->获取常用对白", produces = "application/json; charset=utf-8")
	@RequestMapping(value = "/getAppChatDialogue", method = RequestMethod.POST)
	@ResponseBody
	public Result<PageInfo<TmChatDialoguePageVo>> getAppChatDialogue(@RequestBody BaseQueryDto dto) {
		logger.info("getAppChatDialogue==>");
		PageInfo<TmChatDialoguePageVo> page = appApiService.getAppChatDialogue(dto);
		return new Result<PageInfo<TmChatDialoguePageVo>>(1, page, null);
	}

	@ApiOperation(value = "添加常见对白(支持多条)", produces = "application/json; charset=utf-8")
	@RequestMapping(value = "/addBatchAppChatDialogue", method = RequestMethod.POST)
	@ResponseBody
	public Result<Integer> addBatchAppChatDialogue(
			@RequestParam(value = "dealerCode", required = true) String dealerCode, @RequestBody List<String> list) {
		logger.info("addBatchAppChatDialogue==>");
		ChatDialogueDto dto = new ChatDialogueDto();
		dto.setCreateDate(new Date());
		dto.setDealerCode(dealerCode);
		dto.setDataMessage(list);
		if (dto.getDealerCode() == null || "".equals(dto.getDataMessage())) {
			throw new MyException("dealerCode is null");
		}
		int i = appApiService.addBatchAppChatDialogues(dto);
		return new Result<>(1, i, null);
	}

	@ApiOperation(value = "更新常见对白", produces = "application/json; charset=utf-8")
	@RequestMapping(value = "/updateAppChatDialogue", method = RequestMethod.POST)
	@ResponseBody
	public Result<Integer> updateAppChatDialogue(@RequestBody TmChatDialogue tm) {
		int i = appApiService.updateAppChatDialogue(tm);
		return new Result<>(1, i, null);
	}

	@ApiOperation(value = "删除常见对白", produces = "application/json; charset=utf-8")
	@RequestMapping(value = "/deleteAppChatDialogue", method = RequestMethod.POST)
	@ResponseBody
	public Result<Integer> deleteAppChatDialogue(@RequestParam(value = "id", required = true) Integer id) {
		int i = appApiService.deleteAppChatDialogue(id);
		return new Result<>(1, i, null);
	}

	@ApiOperation(value = "导出-->获取常用对白", produces = "application/json; charset=utf-8")
	@RequestMapping(value = "/getDialogueExport", method = RequestMethod.GET)
	@ResponseBody
	public Result<String> getDialogueExport(@RequestParam(value = "dealerCode", required = true) String dealerCode) {
		logger.info("getDialogueExport==>");
		List<TmChatDialoguePageVo> page = appApiService.getDialogueExport(dealerCode);
		ExportParams params = new ExportParams(null, "常见对白");
		params.setType(ExcelType.HSSF);
		Workbook wk = ExcelExportUtil.exportExcel(params, TmChatDialoguePageVo.class, page);
		ByteArrayOutputStream os = null;
		String fileUrl = null;
		try {
			String str = "常见对白" + "-" + DateUtil.format(new Date(), "yyyyMMddHHmmssSSS") + ".xls";
			os = new ByteArrayOutputStream();
			wk.write(os);
			fileUrl = FileStore.getInstance().write("nd02", "fs01", str, os.toByteArray());
		} catch (Exception e) {
			logger.error("error1", e);
			if (os != null) {
				try {
					os.close();
				} catch (IOException e1) {
					logger.error("error2", e1);
				}
			}
			return new Result<String>(0, fileUrl, null);
		}
		return new Result<String>(1, fileUrl, null);
	}
}
