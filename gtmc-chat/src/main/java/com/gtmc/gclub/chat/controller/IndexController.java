package com.gtmc.gclub.chat.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 登录功能
 * 
 */
@Controller
@RequestMapping(value = "/dev")
public class IndexController extends BaseController {

	/**
	 * 进入首页
	 */
	@RequestMapping(value = "/index")
	public String postIndex() {
		return "swagger-ui";
	}

}
