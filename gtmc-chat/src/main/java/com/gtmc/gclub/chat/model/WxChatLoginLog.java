package com.gtmc.gclub.chat.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author kaihua
 * 登录记录
 *
 */
@Table(name = "WX_CHAT_LOGIN_LOG")
public class WxChatLoginLog {
	public static final Integer APP_PC = 1;
	public static final Integer APP_QY = 2;
	/**
	 * 编号
	 */
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;

	/**
	 * 客服编号
	 */
	@Column(name = "WX_USER_ID")
	private Integer wxUserId;

	/**
	 * 登录时间
	 */
	@Column(name = "LOGIN_DATE")
	private Date loginDate;
	/**
	 * 更新时间
	 */
	@Column(name = "UPDATE_DATE")
	private Date updateDate;
	/**
	 * 下线时间
	 */
	@Column(name = "LOGOUT_DATE")
	private Date logoutDate;

	/**
	 * 登录标志：1.PC，2.企业号
	 */
	@Column(name = "APP_TYPE")
	private Integer appType;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getWxUserId() {
		return wxUserId;
	}

	public void setWxUserId(Integer wxUserId) {
		this.wxUserId = wxUserId;
	}

	public Date getLoginDate() {
		return loginDate;
	}

	public void setLoginDate(Date loginDate) {
		this.loginDate = loginDate;
	}

	public Date getLogoutDate() {
		return logoutDate;
	}

	public void setLogoutDate(Date logoutDate) {
		this.logoutDate = logoutDate;
	}

	public Integer getAppType() {
		return appType;
	}

	public void setAppType(Integer appType) {
		this.appType = appType;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	@Override
	public String toString() {
		return "WxChatLoginLog [id=" + id + ", wxUserId=" + wxUserId + ", loginDate=" + loginDate + ", updateDate="
				+ updateDate + ", logoutDate=" + logoutDate + ", appType=" + appType + "]";
	}


}