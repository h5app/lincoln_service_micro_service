package com.gtmc.gclub.chat.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.gtmc.gclub.chat.annotation.MySQLDb;
import com.gtmc.gclub.chat.model.WxChatLoginLog;

import tk.mybatis.mapper.common.Mapper;

@MySQLDb
public interface WxChatLoginLogMapper extends Mapper<WxChatLoginLog> {

	/**
	 * 获取最后操作时间的对象
	 * @param wxUserId
	 * @param appType
	 * @return
	 */
	public WxChatLoginLog selectLast(@Param("wxUserId") Integer wxUserId, @Param("appType")Integer appType);

	/**
	 * 更新最后操作时间
	 * @param wxUserId
	 * @param appType
	 */
	public void updateTime(@Param("wxUserId") Integer wxUserId, @Param("appType")Integer appType);

	/**
	 * 下线
	 * @param wxUserId
	 * @param appType
	 */
	public void loginOut(@Param("wxUserId") Integer wxUserId);

	/**
	 * @return 获取所有操作超时的客服
	 */
	public List<WxChatLoginLog> getAllTimeOutChat(@Param("timeOutSecond") Integer timeOutSecond);

}