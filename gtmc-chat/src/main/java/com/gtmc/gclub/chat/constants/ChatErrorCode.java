package com.gtmc.gclub.chat.constants;

public class ChatErrorCode {
	public static final Integer SUCCESS = 200;
	/**
	 * 客服忙碌，请等待，正在排队中...
	 */
	public static final Integer CHAT_BESY_WAIT = 20001 ; 
	/**
	 * 客服忙碌，请稍后再试。（队列已满）
	 */
	public static final Integer CHAT_BESY_NEXT = 20002 ; 
	/**
	 * 目前没有在线状态的客服
	 */
	public static final Integer CHAT_NO_ONLINE = 20003 ;
	/**
	 * 目前没有在线状态的客服
	 */
	public static final Integer CHAT_OFFLINE = 20006 ;
	/**
	 * 目前没有空闲的在线客服
	 */
	public static final Integer CHAT_NO_LEISUREST = 20004 ; 
	/**
	 * 媒体消息文件超过2MB
	 */
	public static final Integer MEDIA_SO_BIG = 20005 ; 
	/**
	 * 消息发送失败
	 */
	public static final Integer MESSAGE_SEND_FAIL = 30001 ; 
	/**
	 * no app user
	 */
	public static final Integer NO_APP_USER = 10022 ; 
	/**
	 * no wx user
	 */
	public static final Integer NO_WX_USER = 10021 ; 
}
