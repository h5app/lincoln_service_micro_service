package com.gtmc.gclub.chat.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.gtmc.gclub.chat.annotation.MySQLDb;
import com.gtmc.gclub.chat.model.WxUserOwner;

import tk.mybatis.mapper.common.Mapper;

@MySQLDb
public interface WxUserOwnerMapper extends Mapper<WxUserOwner> {

	public List<Map<String, Object>> getWxChatList(@Param("userId") Integer userId,@Param("status") Integer status);

	public int updateBy(WxUserOwner wu);
	
	public List<Map<String, Object>> getWxChatOwerList(Map<String, Object> map);
	
	public List<Map<String, Object>> getWxChatOwerListUnite(Map<String, Object> map);
	
	/**
	 * 选取客服：
	 * 筛选ownerCode对应的客服中，指定时间内lastStartTime-lastEndTime对应人数最少的客服
	 * 补充条件：一个客服最多服务20人
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> getLeisureService(Map<String, Object> map);
	
	/**
	 * 根据客服list获取所有有效的指定客服在指定时间范围的对应人数关系表
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> getChatStatusByChatList(@Param("chatList") List<String> chatList,@Param("status") Integer status,
			@Param("firstStartTime") Date firstStartTime,@Param("firstEndTime") Date firstEndTime,
			@Param("lastStartTime") Date lastStartTime,@Param("lastEndTime") Date lastEndTime);

	/**
	 * 超过指定时长的聊天链接，结束掉.单位秒
	 */
//	public void updateStatusByTimeOut(Integer timeOutSecond);
	/**
	 * 查询出所有最后发送消息超出指定时间的聊天链接
	 * @return
	 */
	public List<WxUserOwner> selectChatUserByTimeOut(Integer timeOut);

	/**
	 * 断开客服和用户之间的链接
	 * @param chatId	客服ID
	 * @param ownerCode	用户ID
	 */
	public void deleteChatOwnerRel(@Param("chatId") Integer chatId, @Param("ownerCode") String ownerCode);
	
}