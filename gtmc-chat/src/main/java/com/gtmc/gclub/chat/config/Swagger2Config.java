package com.gtmc.gclub.chat.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class Swagger2Config {

	@Bean
	public Docket docket() {// 定义组
		return new Docket(DocumentationType.SWAGGER_2).groupName("api").select() // 选择那些路径和api会生成document
				.apis(RequestHandlerSelectors.basePackage("com.gtmc.gclub.chat.controller")) // 拦截的包
				// .paths(regex("/api/.*"))// 拦截的接口路径
				// .paths(regex("(/chat/|/api/).*"))
				.build() // 创建
				.apiInfo(apiInfo()); // 配置说明
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()//
				.title("在线咨询API")// 标题
				.description("用于在线咨询的API")// 描述
				.termsOfServiceUrl("http://www.baidu.com")//
				.contact(new Contact("benjamin", "www.baidu.com", "liuxudong@yonyou.com"))// 联系
				// .license("Apache License Version 2.0")// 开源协议
				// .licenseUrl("https://github.com/springfox/springfox/blob/master/LICENSE")//
				// 地址
				.version("1.0")// 版本
				.build();
	}

}
