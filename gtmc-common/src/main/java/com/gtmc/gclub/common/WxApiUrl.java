package com.gtmc.gclub.common;

public class WxApiUrl {
	// 微信调用接口凭证 access_token
	public static final String WX_ACCESS_TOKEN = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=ID&corpsecret=SECRECT";
	// 根据code获取成员信息
	public static final String WX_GETUSERINFO = "https://qyapi.weixin.qq.com/cgi-bin/user/getuserinfo?access_token=ACCESS_TOKEN&code=CODE";
	// 获取成员详细信息
	public static final String WX_USER_GET = "https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token=ACCESS_TOKEN&userid=USERID";

	// 根据部门id 获取部门信息
	public static final String WX_DEPARTMENT_LIST = "https://qyapi.weixin.qq.com/cgi-bin/department/list?access_token=ACCESS_TOKEN&id=ID";
	// 根据标签id 获取成员列表
	public static final String WX_TAG_LIST = "https://qyapi.weixin.qq.com/cgi-bin/tag/get?access_token=ACCESS_TOKEN&tagid=TAGID";
	// 获取jsApi的ticket
	public static final String WX_JS_TICKET = "https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket?access_token=ACCESS_TOKE";

	// 推送消息
	public static final String WX_SEND_MSG = "https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=ACCESS_TOKE";
	// 获取多媒体文件
//	public static final String WX_MEDIA = "https://qyapi.weixin.qq.com/cgi-bin/media/get?access_token=ACCESS_TOKEN&media_id=MEDIA_ID";

	// 获取部门成员
	public static final String WX_SIMPLELIST = "https://qyapi.weixin.qq.com/cgi-bin/user/list?access_token=ACCESS_TOKEN&department_id=DEPARTMENT_ID&fetch_child=FETCH_CHILD&status=STATUS";
	
	// 添加永久图文素材
	public static final String WX_ADD_PERMANENT_NEWSSOURCE = "https://api.weixin.qq.com/cgi-bin/material/add_news?access_token=ACCESS_TOKEN";
	// 添加永久图文素材中的图片
	public static final String WX_ADD_PERMANENT_NEWPIC = "https://api.weixin.qq.com/cgi-bin/media/uploadimg?access_token=ACCESS_TOKEN";
	// 添加永久素材(媒体文件类型，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb）)
	public static final String WX_ADD_PERMANENT_SOURCE = "https://api.weixin.qq.com/cgi-bin/material/add_material?access_token=ACCESS_TOKEN&type=TYPE";
	/**
	 * 获取永久素材列表地址
	 */
	public static final String WX_GET_PERMANENT_SOURCE = "https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token=ACCESS_TOKEN";
	/**
	 * 获取单个永久素材
	 */
	public static final String WX_GET_PERMANENT_ONE = "https://api.weixin.qq.com/cgi-bin/material/get_material?access_token=ACCESS_TOKEN";
	/**
	 * 添加临时素材
	 */
	public static final String WX_ADD_TEMP_SOURCE = "https://api.weixin.qq.com/cgi-bin/media/upload?access_token=ACCESS_TOKEN&type=TYPE";
	
	/**
	 * 从公众号获取多媒体文件(临时素材，video需要使用http请求)
	 */
	public static final String WX_MEDIA = "https://api.weixin.qq.com/cgi-bin/media/get?access_token=ACCESS_TOKEN&media_id=MEDIA_ID";
}
