package com.gtmc.gclub.wx.dto;

/**
 * 微信通用接口凭证 -按钮
 * 
 * @author liufeng
 * @date 2013-08-08
 */
public class AccessToken {
	private String token;
	// 获取到的凭证
	private String access_token;
	// 凭证有效时间，单位：秒
	private int expires_in;

	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public int getExpires_in() {
		return expires_in;
	}

	public void setExpires_in(int expires_in) {
		this.expires_in = expires_in;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Override
	public String toString() {
		return "AccessToken [token=" + token + ", access_token=" + access_token + ", expires_in=" + expires_in + "]";
	}

}
