package com.gtmc.gclub.wx.dto;

import java.util.List;

public class SingUserInfo {
	private String userid;
	private String name;
	private List<Integer> department;

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Integer> getDepartment() {
		return department;
	}

	public void setDepartment(List<Integer> department) {
		this.department = department;
	}

}
