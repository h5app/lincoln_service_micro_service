package com.gtmc.gclub.wx.dto;

import java.util.List;

public class DepUser {
	private Integer errcode;
	private String errmsg;
	private List<WxUserInfo> userList;

	public Integer getErrcode() {
		return errcode;
	}

	public void setErrcode(Integer errcode) {
		this.errcode = errcode;
	}

	public String getErrmsg() {
		return errmsg;
	}

	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}

	public List<WxUserInfo> getUserList() {
		return userList;
	}

	public void setUserList(List<WxUserInfo> userList) {
		this.userList = userList;
	}

}
