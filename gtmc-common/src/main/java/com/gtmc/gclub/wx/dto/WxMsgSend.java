package com.gtmc.gclub.wx.dto;

public class WxMsgSend {
	/*
	   "errcode": 0,
	   "errmsg": "ok",
	   "invaliduser": "UserID1",
	   "invalidparty":"PartyID1",
	   "invalidtag":"TagID1"
	 */
	private Integer errcode;
	private String errmsg;
	private String invaliduser;
	private String invalidparty;
	private String invalidtag;
	
	public Integer getErrcode() {
		return errcode;
	}
	public void setErrcode(Integer errcode) {
		this.errcode = errcode;
	}
	public String getErrmsg() {
		return errmsg;
	}
	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}
	public String getInvaliduser() {
		return invaliduser;
	}
	public void setInvaliduser(String invaliduser) {
		this.invaliduser = invaliduser;
	}
	public String getInvalidparty() {
		return invalidparty;
	}
	public void setInvalidparty(String invalidparty) {
		this.invalidparty = invalidparty;
	}
	public String getInvalidtag() {
		return invalidtag;
	}
	public void setInvalidtag(String invalidtag) {
		this.invalidtag = invalidtag;
	}
	
	
	
	
}
