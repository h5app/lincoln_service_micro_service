package com.gtmc.gclub.wx.dto;

import java.util.List;

public class TagUser {
	private String userid;
	private String name;
	private List department;

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List getDepartment() {
		return department;
	}

	public void setDepartment(List department) {
		this.department = department;
	}

}
