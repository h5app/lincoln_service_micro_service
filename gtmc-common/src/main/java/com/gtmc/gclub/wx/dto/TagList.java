package com.gtmc.gclub.wx.dto;

import java.util.List;

public class TagList {
	private Integer errcode;

	private String errmsg;
	private List partylist;

	private List<TagUser> userlist;

	public Integer getErrcode() {
		return errcode;
	}

	public void setErrcode(Integer errcode) {
		this.errcode = errcode;
	}

	public String getErrmsg() {
		return errmsg;
	}

	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}

	public List getPartylist() {
		return partylist;
	}

	public void setPartylist(List partylist) {
		this.partylist = partylist;
	}

	public List<TagUser> getUserlist() {
		return userlist;
	}

	public void setUserlist(List<TagUser> userlist) {
		this.userlist = userlist;
	}

}
