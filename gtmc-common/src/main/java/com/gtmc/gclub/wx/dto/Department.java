package com.gtmc.gclub.wx.dto;

public class Department {
	public Integer id;

	public String name;

	public Integer parentid;

	public Integer order;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getParentid() {
		return parentid;
	}

	public void setParentid(Integer parentid) {
		this.parentid = parentid;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	@Override
	public String toString() {
		return "Department [id=" + id + ", name=" + name + ", parentid=" + parentid + ", order=" + order + "]";
	}

}
