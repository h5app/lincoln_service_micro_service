package com.gtmc.gclub.chat.vo;

import org.jeecgframework.poi.excel.annotation.Excel;

public class ClassifyTagFirstVo {
	private Integer id;

	private Integer id1;

	private Integer id2;
	@Excel(name = "编号", orderNum = "1")
	private Integer num;

	@Excel(name = "一级分类编号", orderNum = "2")
	private String classifyCodeFirst;
	@Excel(name = "一级分类名称", orderNum = "3")
	private String classifyNameFirst;

	private String classifyCodeSecond;

	private String classifyNameSecond;
	private String content;

	private String dealerCode;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId1() {
		return id1;
	}

	public void setId1(Integer id1) {
		this.id1 = id1;
	}

	public Integer getId2() {
		return id2;
	}

	public void setId2(Integer id2) {
		this.id2 = id2;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public String getClassifyCodeFirst() {
		return classifyCodeFirst;
	}

	public void setClassifyCodeFirst(String classifyCodeFirst) {
		this.classifyCodeFirst = classifyCodeFirst;
	}

	public String getClassifyNameFirst() {
		return classifyNameFirst;
	}

	public void setClassifyNameFirst(String classifyNameFirst) {
		this.classifyNameFirst = classifyNameFirst;
	}

	public String getClassifyCodeSecond() {
		return classifyCodeSecond;
	}

	public void setClassifyCodeSecond(String classifyCodeSecond) {
		this.classifyCodeSecond = classifyCodeSecond;
	}

	public String getClassifyNameSecond() {
		return classifyNameSecond;
	}

	public void setClassifyNameSecond(String classifyNameSecond) {
		this.classifyNameSecond = classifyNameSecond;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getDealerCode() {
		return dealerCode;
	}

	public void setDealerCode(String dealerCode) {
		this.dealerCode = dealerCode;
	}

}
