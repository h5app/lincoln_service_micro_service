package com.gtmc.gclub.chat.entity;

public class Result<T> {
	private Integer returnFlag;

	public Integer getReturnFlag() {
		return returnFlag;
	}

	public void setReturnFlag(Integer returnFlag) {
		this.returnFlag = returnFlag;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	private T data;

	private String errorMsg;

	public Result(Integer returnFlag, String errorMsg) {
		super();
		this.returnFlag = returnFlag;
		this.errorMsg = errorMsg;
	}

	public Result(Integer returnFlag, T data, String errorMsg) {
		super();
		this.returnFlag = returnFlag;
		this.data = data;
		this.errorMsg = errorMsg;
	}

	public Result() {
		super();
	}

	@Override
	public String toString() {
		return "Result [ returnFlag=" + returnFlag + ", errorMsg=" + errorMsg + ", data=" + data + "]";
	}
}
