package com.gtmc.gclub.chat.vo;

import org.jeecgframework.poi.excel.annotation.Excel;

public class ClassifyContentPageVo {

	private Integer id;

	private Integer id1;

	private Integer id2;
	@Excel(name = "编号", orderNum = "1")
	private Integer num;

	@Excel(name = "一级分类编号", orderNum = "2")
	private String classifyCodeFirst;
	@Excel(name = "一级分类名称", orderNum = "3")
	private String classifyNameFirst;
	@Excel(name = "二级分类编号", orderNum = "4")
	private String classifyCodeSecond;
	@Excel(name = "二级分类名称", orderNum = "5")
	private String classifyNameSecond;
	@Excel(name = "内容", orderNum = "6")
	private String content;
	private String contentType;

	private String dealerCode;

	public Integer getId2() {
		return id2;
	}

	public void setId2(Integer id2) {
		this.id2 = id2;
	}

	public String getDealerCode() {
		return dealerCode;
	}

	public void setDealerCode(String dealerCode) {
		this.dealerCode = dealerCode;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId1() {
		return id1;
	}

	public void setId1(Integer id1) {
		this.id1 = id1;
	}

	public String getClassifyCodeFirst() {
		return classifyCodeFirst;
	}

	public void setClassifyCodeFirst(String classifyCodeFirst) {
		this.classifyCodeFirst = classifyCodeFirst;
	}

	public String getClassifyNameFirst() {
		return classifyNameFirst;
	}

	public void setClassifyNameFirst(String classifyNameFirst) {
		this.classifyNameFirst = classifyNameFirst;
	}

	public String getClassifyCodeSecond() {
		return classifyCodeSecond;
	}

	public void setClassifyCodeSecond(String classifyCodeSecond) {
		this.classifyCodeSecond = classifyCodeSecond;
	}

	public String getClassifyNameSecond() {
		return classifyNameSecond;
	}

	public void setClassifyNameSecond(String classifyNameSecond) {
		this.classifyNameSecond = classifyNameSecond;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	@Override
	public String toString() {
		return "ClassifyContentPageVo [id=" + id + ", id1=" + id1 + ", id2=" + id2 + ", num=" + num
				+ ", classifyCodeFirst=" + classifyCodeFirst + ", classifyNameFirst=" + classifyNameFirst
				+ ", classifyCodeSecond=" + classifyCodeSecond + ", classifyNameSecond=" + classifyNameSecond
				+ ", content=" + content + ", contentType=" + contentType + ", dealerCode=" + dealerCode + "]";
	}

}
