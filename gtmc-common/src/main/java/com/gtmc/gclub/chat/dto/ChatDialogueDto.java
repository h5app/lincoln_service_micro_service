package com.gtmc.gclub.chat.dto;

import java.util.Date;
import java.util.List;

public class ChatDialogueDto {
	private String dealerCode;

	public List<String> dataMessage;

	private Date createDate;

	public String getDealerCode() {
		return dealerCode;
	}

	public void setDealerCode(String dealerCode) {
		this.dealerCode = dealerCode;
	}

	public List<String> getDataMessage() {
		return dataMessage;
	}

	public void setDataMessage(List<String> dataMessage) {
		this.dataMessage = dataMessage;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

}
