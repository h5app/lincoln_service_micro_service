package com.gtmc.gclub.chat.dto;

import java.util.Date;
import java.util.List;

public class ChatClassifyContentDto {

	private Integer id;

	private String classifyCode;

	private String classifyName;

	private List<String> content;

	private Integer pid;

	private Date createDate;

	private Date updateTime;

	private List<ChatClassifyContentDto> list;

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<ChatClassifyContentDto> getList() {
		return list;
	}

	public void setList(List<ChatClassifyContentDto> list) {
		this.list = list;
	}

	public String getClassifyCode() {
		return classifyCode;
	}

	public void setClassifyCode(String classifyCode) {
		this.classifyCode = classifyCode;
	}

	public String getClassifyName() {
		return classifyName;
	}

	public void setClassifyName(String classifyName) {
		this.classifyName = classifyName;
	}

	public List<String> getContent() {
		return content;
	}

	public void setContent(List<String> content) {
		this.content = content;
	}

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

}
