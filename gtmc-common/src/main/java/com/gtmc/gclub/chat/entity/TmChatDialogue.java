//package com.gtmc.gclub.chat.entity;
//
//import java.util.Date;
//
//public class TmChatDialogue {
//
//	private Integer id;
//
//	private String dealerCode;
//
//	/**
//	 * 内容
//	 */
//	private String content;
//
//	private Integer useNum;
//
//	private Date createDate;
//
//	/**
//	 * 更新时间
//	 */
//
//	private Date updateDate;
//
//	/**
//	 * @return id
//	 */
//	public Integer getId() {
//		return id;
//	}
//
//	/**
//	 * @param id
//	 */
//	public void setId(Integer id) {
//		this.id = id;
//	}
//
//	/**
//	 * 获取经销商code
//	 *
//	 * @return dealer_code - 经销商code
//	 */
//	public String getDealerCode() {
//		return dealerCode;
//	}
//
//	/**
//	 * 设置经销商code
//	 *
//	 * @param dealerCode
//	 *            经销商code
//	 */
//	public void setDealerCode(String dealerCode) {
//		this.dealerCode = dealerCode;
//	}
//
//	/**
//	 * 获取内容
//	 *
//	 * @return content - 内容
//	 */
//	public String getContent() {
//		return content;
//	}
//
//	/**
//	 * 设置内容
//	 *
//	 * @param content
//	 *            内容
//	 */
//	public void setContent(String content) {
//		this.content = content;
//	}
//
//	/**
//	 * 获取使用次数
//	 *
//	 * @return use_num - 使用次数
//	 */
//	public Integer getUseNum() {
//		return useNum;
//	}
//
//	/**
//	 * 设置使用次数
//	 *
//	 * @param useNum
//	 *            使用次数
//	 */
//	public void setUseNum(Integer useNum) {
//		this.useNum = useNum;
//	}
//
//	/**
//	 * 获取创建时间
//	 *
//	 * @return create_date - 创建时间
//	 */
//	public Date getCreateDate() {
//		return createDate;
//	}
//
//	/**
//	 * 设置创建时间
//	 *
//	 * @param createDate
//	 *            创建时间
//	 */
//	public void setCreateDate(Date createDate) {
//		this.createDate = createDate;
//	}
//
//	/**
//	 * 获取更新时间
//	 *
//	 * @return update_date - 更新时间
//	 */
//	public Date getUpdateDate() {
//		return updateDate;
//	}
//
//	/**
//	 * 设置更新时间
//	 *
//	 * @param updateDate
//	 *            更新时间
//	 */
//	public void setUpdateDate(Date updateDate) {
//		this.updateDate = updateDate;
//	}
//}