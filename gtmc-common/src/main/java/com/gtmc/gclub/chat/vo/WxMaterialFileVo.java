package com.gtmc.gclub.chat.vo;

import java.util.Arrays;

public class WxMaterialFileVo {
	private String title ;
	private String description;
	private String downloadUrl;
	private String localUrl;
	private byte[] data;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public byte[] getData() {
		return data;
	}
	public void setData(byte[] data) {
		this.data = data;
	}
	public String getDownloadUrl() {
		return downloadUrl;
	}
	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}
	public String getLocalUrl() {
		return localUrl;
	}
	public void setLocalUrl(String localUrl) {
		this.localUrl = localUrl;
	}
	@Override
	public String toString() {
		return "WxMaterialFileVo [title=" + title + ", description=" + description + ", downloadUrl=" + downloadUrl
				+ ", localUrl=" + localUrl + ", data=" + data.length + "]";
	}
}
