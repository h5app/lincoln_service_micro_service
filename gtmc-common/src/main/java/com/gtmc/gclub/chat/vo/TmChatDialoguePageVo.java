package com.gtmc.gclub.chat.vo;

import java.util.Date;

import org.jeecgframework.poi.excel.annotation.Excel;

public class TmChatDialoguePageVo {
	@Excel(name = "编号", orderNum = "1")
	private Integer num;

	private Integer id;

	private String dealerCode;
	@Excel(name = "对白", orderNum = "2")
	private String content;
	// @Excel(name = "使用批次", orderNum = "3")
	private Integer useNum;

	private Date createDate;
	private Date updateDate;

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDealerCode() {
		return dealerCode;
	}

	public void setDealerCode(String dealerCode) {
		this.dealerCode = dealerCode;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getUseNum() {
		return useNum;
	}

	public void setUseNum(Integer useNum) {
		this.useNum = useNum;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

}
