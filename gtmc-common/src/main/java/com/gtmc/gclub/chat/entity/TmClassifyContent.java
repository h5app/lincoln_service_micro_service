//package com.gtmc.gclub.chat.entity;
//
//import java.util.Date;
//
//public class TmClassifyContent {
//
//	private Integer id;
//
//	private String dealerCode;
//
//	private String classifyCode;
//
//	private String classifyName;
//
//	/**
//	 * 名称
//	 */
//	private String content;
//
//	/**
//	 * 上级节点id
//	 */
//	private Integer pid;
//
//	/**
//	 * 创建时间
//	 */
//
//	private Date createDate;
//
//	private Date updateTime;
//
//	/**
//	 * @return id
//	 */
//	public Integer getId() {
//		return id;
//	}
//
//	/**
//	 * @param id
//	 */
//	public void setId(Integer id) {
//		this.id = id;
//	}
//
//	/**
//	 * 获取经销商code
//	 *
//	 * @return dealer_code - 经销商code
//	 */
//	public String getDealerCode() {
//		return dealerCode;
//	}
//
//	/**
//	 * 设置经销商code
//	 *
//	 * @param dealerCode
//	 *            经销商code
//	 */
//	public void setDealerCode(String dealerCode) {
//		this.dealerCode = dealerCode;
//	}
//
//	/**
//	 * 获取分类code
//	 *
//	 * @return classify_code - 分类code
//	 */
//	public String getClassifyCode() {
//		return classifyCode;
//	}
//
//	/**
//	 * 设置分类code
//	 *
//	 * @param classifyCode
//	 *            分类code
//	 */
//	public void setClassifyCode(String classifyCode) {
//		this.classifyCode = classifyCode;
//	}
//
//	/**
//	 * 获取分类name
//	 *
//	 * @return classify_name - 分类name
//	 */
//	public String getClassifyName() {
//		return classifyName;
//	}
//
//	/**
//	 * 设置分类name
//	 *
//	 * @param classifyName
//	 *            分类name
//	 */
//	public void setClassifyName(String classifyName) {
//		this.classifyName = classifyName;
//	}
//
//	/**
//	 * 获取名称
//	 *
//	 * @return content - 名称
//	 */
//	public String getContent() {
//		return content;
//	}
//
//	/**
//	 * 设置名称
//	 *
//	 * @param content
//	 *            名称
//	 */
//	public void setContent(String content) {
//		this.content = content;
//	}
//
//	/**
//	 * 获取上级节点id
//	 *
//	 * @return pid - 上级节点id
//	 */
//	public Integer getPid() {
//		return pid;
//	}
//
//	/**
//	 * 设置上级节点id
//	 *
//	 * @param pid
//	 *            上级节点id
//	 */
//	public void setPid(Integer pid) {
//		this.pid = pid;
//	}
//
//	/**
//	 * 获取创建时间
//	 *
//	 * @return create_date - 创建时间
//	 */
//	public Date getCreateDate() {
//		return createDate;
//	}
//
//	/**
//	 * 设置创建时间
//	 *
//	 * @param createDate
//	 *            创建时间
//	 */
//	public void setCreateDate(Date createDate) {
//		this.createDate = createDate;
//	}
//
//	/**
//	 * 获取更新时间
//	 *
//	 * @return update_time - 更新时间
//	 */
//	public Date getUpdateTime() {
//		return updateTime;
//	}
//
//	/**
//	 * 设置更新时间
//	 *
//	 * @param updateTime
//	 *            更新时间
//	 */
//	public void setUpdateTime(Date updateTime) {
//		this.updateTime = updateTime;
//	}
//}